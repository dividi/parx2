#! /bin/bash


php artisan migrate --seed
chown -R www-data:www-data public
chown -R www-data:www-data storage/logs/
/usr/sbin/apache2ctl -DFOREGROUND
