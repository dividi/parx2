<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/test', function (Request $request) {
    return $request;
});*/




// protected routes
Route::group( ['middleware' => ['auth:sanctum']], function () {

    Route::get('/test_auth', ['uses' => 'RestController@testAuth']);

    Route::post('/rest_host', ['uses' => 'RestController@addHost']);
    Route::post('/rest_version', ['uses' => 'RestController@updateVersion']);
    Route::post('/rest_hostupdate', ['uses' => 'RestController@updateHost']);
    Route::post('/hosts/update-last-profile-name', ['uses' => 'RestController@updateHostLastProfile']);
    Route::post('/rest_deltask', ['uses' => 'RestController@delTask']);
    //Route::post('/REST_saveShortcuts', ['uses' => 'RestController@RESTsaveShortcuts']);   // c# client
    Route::post('/save_shortcuts', ['uses' => 'RestController@saveShortcuts']);
    Route::get('/REST_exportUsers', ['uses' => 'RestController@getAllUsers']);  // users is a protected data, cannot be in public routes

});


// public routes
Route::get('/rest', ['uses' => 'RestController@rest']);
Route::get('/rest_hostid/{hostName}', ['uses' => 'RestController@getHostId']);
Route::get('/rest_profiles/{hostName}/{userLogin}', ['uses' => 'RestController@getProfiles']);
Route::get('/rest_profile/{profileId}', ['uses' => 'RestController@getProfile']);
Route::get('/rest_options/{profileid}', ['uses' => 'RestController@getParameters']);
Route::get('/profile_desktop_shortcuts/{profileid}', ['uses' => 'RestController@getShortcuts']);
Route::get('/profile_taskbar_shortcuts/{profileid}', ['uses' => 'RestController@getTaskbarShortcuts']);
Route::get('/rest_shares/{profileid}', ['uses' => 'RestController@getShares']);
Route::get('/rest_printers/{profileid}', ['uses' => 'RestController@getPrinters']);
Route::get('/rest_tasks/{hostid}', ['uses' => 'RestController@getTasks']);
Route::get('/tasks_by_hostname/{hostname}', ['uses' => 'RestController@getTasksByHostname']);
Route::get('/rest_option/{profileId}/{parameterName}', ['uses' => 'RestController@getParameter']);
Route::get('/REST_fullProfiles/{hostName}/{userLogin}', ['uses' => 'RestController@getFullProfiles']);
Route::get('/REST_fullProfile/{id}', ['uses' => 'RestController@getFullProfile']);
Route::get('/REST_exportHosts', ['uses' => 'RestController@getAllHosts']);
Route::get('/all_profile_ids', ['uses' => 'RestController@getAllProfileIds']);
Route::get('/rest_file_assoc/{profileId}', ['uses' => 'RestController@getFileAssoc']);

