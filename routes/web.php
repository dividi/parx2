<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// HOME PAGE

Route::get('/', function() { return view('welcome'); })->middleware('auth');
Route::get('/', ['as' => 'home', 'uses' => 'HomeController@stats'])->middleware('auth');



// LOGIN / LOGOUT
//Route::get('register_api', ['uses' => 'AuthController@registerAPI'])->middleware('auth');     // route to create a parxshell account + token (in a seed now)
Route::get('logins', ['as' => 'auth.index', 'uses' => 'AuthController@index'])->middleware('auth');
Route::get('logins/delete/{id}', ['as' => 'auth.delete', 'uses' => 'AuthController@destroyConfirmation'])->middleware('auth');
Route::delete('logins/{id}', ['as' => 'auth.destroy', 'uses' => 'AuthController@destroy'])->middleware('auth');
Route::get('logins/create', ['as' => 'auth.create', 'uses' => 'AuthController@create'])->middleware('auth');
Route::post('logins/create', ['as' => 'auth.store', 'uses' => 'AuthController@store'])->middleware('auth');


// USERGROUPS

Route::get('usergroups/{id}/editProfiles', ['as' => 'usergroups.editProfiles', 'uses' => 'UserGroupController@editProfiles'])->middleware('auth');
Route::patch('usergroups/{id}/editProfiles', ['as' => 'usergroups.updateProfiles', 'uses' => 'UserGroupController@updateProfiles'])->middleware('auth');
Route::get('usergroups/massDelete', ['as' => 'usergroups.massDelete', 'uses' => 'UserGroupController@massDelete'])->middleware('auth');
Route::delete('usergroups/massDelete', ['as' => 'usergroups.massDestroy', 'uses' => 'UserGroupController@massDestroy'])->middleware('auth');
Route::get('usergroups/create', ['as' => 'usergroups.create', 'uses' => 'UserGroupController@create'])->middleware('auth');
Route::post('usergroups', ['as' => 'usergroups.store', 'uses' => 'UserGroupController@store'])->middleware('auth');
Route::get('usergroups', ['as' => 'usergroups.index', 'uses' => 'UserGroupController@index'])->middleware('auth');
Route::get('usergroups/{id}', ['as' => 'usergroups.show', 'uses' => 'UserGroupController@show'])->middleware('auth');
Route::get('usergroups/delete/{id}', ['as' => 'usergroups.delete', 'uses' => 'UserGroupController@destroyConfirmation'])->middleware('auth');
Route::delete('usergroups/{id}', ['as' => 'usergroups.destroy', 'uses' => 'UserGroupController@destroy'])->middleware('auth');
Route::get('usergroups/{id}/edit', ['as' => 'usergroups.edit', 'uses' => 'UserGroupController@edit'])->middleware('auth');
Route::patch('usergroups/{id}', ['as' => 'usergroups.update', 'uses' => 'UserGroupController@update'])->middleware('auth');






// USERS
Route::get('users/export', ['as' => 'users.export', 'uses' => 'UserController@exportUsers'])->middleware('auth');
Route::get('users/{id}/showImport', ['as' => 'users.showImport', 'uses' => 'UserController@showImport'])->middleware('auth');
Route::patch('users/{id}/import', ['as' => 'users.import', 'uses' => 'UserController@import'])->middleware('auth');
Route::post('users/showMassImport', ['as' => 'users.showMassImport', 'uses' => 'UserController@showMassImport'])->middleware('auth');
Route::patch('users/massImport', ['as' => 'users.massImport', 'uses' => 'UserController@massImport'])->middleware('auth');
Route::post('users/massEditGroups', ['as' => 'users.massEditGroups', 'uses' => 'UserController@massEditGroups'])->middleware('auth');
Route::patch('users/massEditGroups', ['as' => 'users.massUpdateGroups', 'uses' => 'UserController@massUpdateGroups'])->middleware('auth');
Route::post('users/massEditProfiles', ['as' => 'users.massEditProfiles', 'uses' => 'UserController@massEditProfiles'])->middleware('auth');
Route::patch('users/massEditProfiles', ['as' => 'users.massUpdateProfiles', 'uses' => 'UserController@massUpdateProfiles'])->middleware('auth');
Route::get('users/importCSVCreate', ['as' => 'users.importCSVCreate', 'uses' => 'UserController@importCSVCreate'])->middleware('auth');
Route::post('users/importCSVStore', ['as' => 'users.importCSVStore', 'uses' => 'UserController@importCSVStore'])->middleware('auth');
Route::get('users/importLDAPCreate', ['as' => 'users.importLDAPCreate', 'uses' => 'UserController@importLDAPCreate'])->middleware('auth');
Route::post('users/importLDAPStore', ['as' => 'users.importLDAPStore', 'uses' => 'UserController@importLDAPStore'])->middleware('auth');
Route::get('users/massDelete', ['as' => 'users.massDelete', 'uses' => 'UserController@massDelete'])->middleware('auth');
Route::delete('users/massDelete', ['as' => 'users.massDestroy', 'uses' => 'UserController@massDestroy'])->middleware('auth');
Route::get('users/create', ['as' => 'users.create', 'uses' => 'UserController@create'])->middleware('auth');
Route::post('users', ['as' => 'users.store', 'uses' => 'UserController@store'])->middleware('auth');
Route::get('users', ['as' => 'users.index', 'uses' => 'UserController@index'])->middleware('auth');
Route::get('users/{id}', ['as' => 'users.show', 'uses' => 'UserController@show'])->middleware('auth');
Route::get('users/delete/{id}', ['as' => 'users.delete', 'uses' => 'UserController@destroyConfirmation'])->middleware('auth');
Route::delete('users/{id}', ['as' => 'users.destroy', 'uses' => 'UserController@destroy'])->middleware('auth');
Route::get('users/{id}/edit', ['as' => 'users.edit', 'uses' => 'UserController@edit'])->middleware('auth');
Route::patch('users/{id}', ['as' => 'users.update', 'uses' => 'UserController@update'])->middleware('auth');







// CLIENT UPDATER
Route::get('tasks/uploadClient', ['as' => 'uploadClient', 'uses' => 'TaskController@uploadClient'])->middleware('auth');
Route::post('tasks/uploadClient', ['as' => 'storeClient', 'uses' => 'TaskController@storeClient'])->middleware('auth');


// PACKAGES
Route::get('tasks/uploadPackage', ['as' => 'uploadPackage', 'uses' => 'TaskController@uploadPackage'])->middleware('auth');
Route::post('tasks/uploadPackage', ['as' => 'storePackage', 'uses' => 'TaskController@storePackage'])->middleware('auth');




// HOSTS
Route::get('hosts/export', ['as' => 'hosts.export', 'uses' => 'HostController@exportHosts'])->middleware('auth');
Route::get('hosts/importCreate', ['as' => 'hosts.importCreate', 'uses' => 'HostController@importCreate'])->middleware('auth');
Route::post('hosts/importStore', ['as' => 'hosts.importStore', 'uses' => 'HostController@importStore'])->middleware('auth');
Route::post('hosts/massConfig', ['as' => 'hosts.massConfig', 'uses' => 'HostController@massConfig'])->middleware('auth');
Route::patch('hosts/massConfig', ['as' => 'hosts.massCreateConfig', 'uses' => 'HostController@massCreateConfig'])->middleware('auth');
Route::post('hosts/massEditGroups', ['as' => 'hosts.massEditGroups', 'uses' => 'HostController@massEditGroups'])->middleware('auth');
Route::patch('hosts/massEditGroups', ['as' => 'hosts.massUpdateGroups', 'uses' => 'HostController@massUpdateGroups'])->middleware('auth');
Route::get('hosts/importLDAPCreate', ['as' => 'hosts.importLDAPCreate', 'uses' => 'HostController@importLDAPCreate'])->middleware('auth');
Route::post('hosts/importLDAPStore', ['as' => 'hosts.importLDAPStore', 'uses' => 'HostController@importLDAPStore'])->middleware('auth');
Route::post('hosts/massEditProfiles', ['as' => 'hosts.massEditProfiles', 'uses' => 'HostController@massEditProfiles'])->middleware('auth');
Route::patch('hosts/massEditProfiles', ['as' => 'hosts.massUpdateProfiles', 'uses' => 'HostController@massUpdateProfiles'])->middleware('auth');
Route::get('hosts/massDelete', ['as' => 'hosts.massDelete', 'uses' => 'HostController@massDelete'])->middleware('auth');
Route::delete('hosts/massDelete', ['as' => 'hosts.massDestroy', 'uses' => 'HostController@massDestroy'])->middleware('auth');
Route::get('hosts/create', ['as' => 'hosts.create', 'uses' => 'HostController@create'])->middleware('auth');
Route::post('hosts', ['as' => 'hosts.store', 'uses' => 'HostController@store'])->middleware('auth');
Route::get('hosts', ['as' => 'hosts.index', 'uses' => 'HostController@index'])->middleware('auth');
Route::get('hosts/{id}', ['as' => 'hosts.show', 'uses' => 'HostController@show'])->middleware('auth');
Route::get('hosts/delete/{id}', ['as' => 'hosts.delete', 'uses' => 'HostController@destroyConfirmation'])->middleware('auth');
Route::delete('hosts/{id}', ['as' => 'hosts.destroy', 'uses' => 'HostController@destroy'])->middleware('auth');
Route::get('hosts/{id}/edit', ['as' => 'hosts.edit', 'uses' => 'HostController@edit'])->middleware('auth');
Route::patch('hosts/{id}', ['as' => 'hosts.update', 'uses' => 'HostController@update'])->middleware('auth');




// HOSTGROUPS

Route::get('hostgroups/{id}/editProfiles', ['as' => 'hostgroups.editProfiles', 'uses' => 'HostGroupController@editProfiles'])->middleware('auth');
Route::patch('hostgroups/{id}/editProfiles', ['as' => 'hostgroups.updateProfiles', 'uses' => 'HostGroupController@updateProfiles'])->middleware('auth');
Route::get('hostgroups/create', ['as' => 'hostgroups.create', 'uses' => 'HostGroupController@create'])->middleware('auth');
Route::post('hostgroups', ['as' => 'hostgroups.store', 'uses' => 'HostGroupController@store'])->middleware('auth');
Route::get('hostgroups', ['as' => 'hostgroups.index', 'uses' => 'HostGroupController@index'])->middleware('auth');
Route::get('hostgroups/{id}', ['as' => 'hostgroups.show', 'uses' => 'HostGroupController@show'])->middleware('auth');
Route::get('hostgroups/delete/{id}', ['as' => 'hostgroups.delete', 'uses' => 'HostGroupController@destroyConfirmation'])->middleware('auth');
Route::delete('hostgroups/{id}', ['as' => 'hostgroups.destroy', 'uses' => 'HostGroupController@destroy'])->middleware('auth');
Route::get('hostgroups/{id}/edit', ['as' => 'hostgroups.edit', 'uses' => 'HostGroupController@edit'])->middleware('auth');
Route::patch('hostgroups/{id}', ['as' => 'hostgroups.update', 'uses' => 'HostGroupController@update'])->middleware('auth');





// PROFILES



Route::post('profiles/showMassImport', ['as' => 'profiles.showMassImport', 'uses' => 'ProfileController@showMassImport'])->middleware('auth');
Route::patch('profiles/massImport', ['as' => 'profiles.massImport', 'uses' => 'ProfileController@massImport'])->middleware('auth');
Route::get('profiles/{id}/showImport', ['as' => 'profiles.showImport', 'uses' => 'ProfileController@showImport'])->middleware('auth');
Route::post('profiles/{id}/import', ['as' => 'profiles.import', 'uses' => 'ProfileController@import'])->middleware('auth');
Route::get('profiles/create', ['as' => 'profiles.create', 'uses' => 'ProfileController@create'])->middleware('auth');
Route::post('profiles', ['as' => 'profiles.store', 'uses' => 'ProfileController@store'])->middleware('auth');
Route::get('profiles', ['as' => 'profiles.index', 'uses' => 'ProfileController@index'])->middleware('auth');
Route::get('profiles/delete/{id}', ['as' => 'profiles.delete', 'uses' => 'ProfileController@destroyConfirmation'])->middleware('auth');
Route::delete('profiles/{id}', ['as' => 'profiles.destroy', 'uses' => 'ProfileController@destroy'])->middleware('auth');
Route::get('profiles/{id}/edit', ['as' => 'profiles.edit', 'uses' => 'ProfileController@edit'])->middleware('auth');
Route::patch('profiles/{id}', ['as' => 'profiles.update', 'uses' => 'ProfileController@update'])->middleware('auth');
Route::get('profiles/{id}/uploadWallpaper', ['as' => 'profiles.uploadWallpaper', 'uses' => 'ProfileController@uploadWallpaper'])->middleware('auth');
Route::post('profiles/{id}/uploadWallpaper', ['as' => 'profiles.storeWallpaper', 'uses' => 'ProfileController@storeWallpaper'])->middleware('auth');
Route::post('profiles/massUploadWallpaper', ['as' => 'profiles.massUploadWallpaper', 'uses' => 'ProfileController@massUploadWallpaper'])->middleware('auth');
Route::post('profiles/massStoreWallpaper', ['as' => 'profiles.massStoreWallpaper', 'uses' => 'ProfileController@massStoreWallpaper'])->middleware('auth');
Route::get('profiles/chooseFileAssociation/{id}', ['as' => 'profiles.chooseFileAssociation', 'uses' => 'ProfileController@chooseFileAssociation'])->middleware('auth');
Route::patch('profiles/{id}/storeFileAssociation', ['as' => 'profiles.storeFileAssociation', 'uses' => 'ProfileController@storeFileAssociation'])->middleware('auth');
Route::get('profiles/{id}', ['as' => 'profiles.show', 'uses' => 'ProfileController@show'])->middleware('auth');






// PARAMETERS

Route::post('profiles/parameters/edit', ['as' => 'parameters.massParameters', 'uses' => 'ParameterController@showMassEdit'])->middleware('auth');
Route::post('profiles/parameters/update', ['as' => 'parameters.updateProfilesParameters', 'uses' => 'ParameterController@updateProfilesParameters'])->middleware('auth');
Route::get('profiles/parameters/{id}', ['as' => 'profiles.parameters', 'uses' => 'ParameterController@index'])->middleware('auth');
Route::post('profiles/parameters/{id}', ['as' => 'parameters.store', 'uses' => 'ParameterController@store'])->middleware('auth');




// PRINTERS

Route::get('profiles/{id}/assignedPrinters', ['as' => 'profiles.assignedPrinters', 'uses' => 'PrinterController@assignedPrinters'])->middleware('auth');
Route::get('profiles/{id}/printers', ['as' => 'profiles.assignPrinter', 'uses' => 'PrinterController@assignPrinter'])->middleware('auth');
Route::patch('profiles/{id}/printers', ['as' => 'profiles.updatePrinter', 'uses' => 'PrinterController@updatePrinter'])->middleware('auth');
Route::get('profiles/{id}/unassignPrinter/{printer_id}', ['as' => 'profiles.unassignPrinter', 'uses' => 'PrinterController@unassignPrinter'])->middleware('auth');
Route::delete('profiles/{id}/unassignPrinter/{printer_id}', ['as' => 'profiles.destroyPrinter', 'uses' => 'PrinterController@destroyPrinter'])->middleware('auth');
Route::get('profiles/{id}/defaultPrinter/{printer_id}', ['as' => 'profiles.defaultPrinter', 'uses' => 'PrinterController@defaultPrinter'])->middleware('auth');
//Route::get('profiles/{id}/printers', ['as' => 'profiles.editPrinters', 'uses' => 'PrinterController@editPrinters'])->middleware('auth');
//Route::patch('profiles/{id}/printers', ['as' => 'profiles.updatePrinters', 'uses' => 'PrinterController@updatePrinters'])->middleware('auth');
Route::get('printers/create', ['as' => 'printers.create', 'uses' => 'PrinterController@create'])->middleware('auth');
Route::post('printers', ['as' => 'printers.store', 'uses' => 'PrinterController@store'])->middleware('auth');
Route::get('printers', ['as' => 'printers.index', 'uses' => 'PrinterController@index'])->middleware('auth');
Route::get('printers/delete/{id}', ['as' => 'printers.delete', 'uses' => 'PrinterController@destroyConfirmation'])->middleware('auth');
Route::delete('printers/{id}', ['as' => 'printers.destroy', 'uses' => 'PrinterController@destroy'])->middleware('auth');
Route::get('printers/{id}/edit', ['as' => 'printers.edit', 'uses' => 'PrinterController@edit'])->middleware('auth');
Route::patch('printers/{id}', ['as' => 'printers.update', 'uses' => 'PrinterController@update'])->middleware('auth');
Route::get('printers/{id}/affect', ['as' => 'printers.affected', 'uses' => 'PrinterController@affectPrinter'])->middleware('auth');
Route::post('printers/{id}/affect', ['as' => 'printers.affect', 'uses' => 'PrinterController@affectPrinterUpdate'])->middleware('auth');




// SHARE

Route::get('profiles/{id}/assignedShares', ['as' => 'profiles.assignedShares', 'uses' => 'ShareController@assignedShares'])->middleware('auth');
Route::get('profiles/{id}/shares', ['as' => 'profiles.assignShare', 'uses' => 'ShareController@assignShare'])->middleware('auth');
Route::patch('profiles/{id}/shares', ['as' => 'profiles.updateShare', 'uses' => 'ShareController@updateShare'])->middleware('auth');
Route::get('profiles/{id}/unassignShare/{share_id}', ['as' => 'profiles.unassignShare', 'uses' => 'ShareController@unassignShare'])->middleware('auth');
Route::delete('profiles/{id}/unassignShare/{share_id}', ['as' => 'profiles.destroyShare', 'uses' => 'ShareController@destroyShare'])->middleware('auth');
Route::get('shares/create', ['as' => 'shares.create', 'uses' => 'ShareController@create'])->middleware('auth');
Route::post('shares', ['as' => 'shares.store', 'uses' => 'ShareController@store'])->middleware('auth');
Route::get('shares', ['as' => 'shares.index', 'uses' => 'ShareController@index'])->middleware('auth');
Route::get('shares/delete/{id}', ['as' => 'shares.delete', 'uses' => 'ShareController@destroyConfirmation'])->middleware('auth');
Route::delete('shares/{id}', ['as' => 'shares.destroy', 'uses' => 'ShareController@destroy'])->middleware('auth');
Route::get('shares/{id}/edit', ['as' => 'shares.edit', 'uses' => 'ShareController@edit'])->middleware('auth');
Route::patch('shares/{id}', ['as' => 'shares.update', 'uses' => 'ShareController@update'])->middleware('auth');





// SHORTCUTS

Route::get('profiles/{id}/shortcuts/assign', ['as' => 'shortcuts.assignShortcuts', 'uses' => 'ShortcutController@assignShortcuts'])->middleware('auth');
Route::post('profiles/{id}/shortcuts/assign', ['as' => 'shortcuts.updateShortcuts', 'uses' => 'ShortcutController@updateShortcuts'])->middleware('auth');
Route::get('profiles/{id}/shortcuts/create', ['as' => 'shortcuts.create', 'uses' => 'ShortcutController@create'])->middleware('auth');
Route::post('profiles/{id}/shortcuts', ['as' => 'shortcuts.store', 'uses' => 'ShortcutController@store'])->middleware('auth');
Route::get('profiles/{id}/shortcuts/{shortcut_id}/delete', ['as' => 'shortcuts.delete', 'uses' => 'ShortcutController@destroyConfirmation'])->middleware('auth');
Route::delete('profiles/{id}/shortcuts/{shortcut_id}/delete', ['as' => 'shortcuts.destroy', 'uses' => 'ShortcutController@destroy'])->middleware('auth');
Route::get('profiles/{id}/shortcuts/{shortcut_id}/edit', ['as' => 'shortcuts.edit', 'uses' => 'ShortcutController@edit'])->middleware('auth');
Route::patch('profiles/{id}/shortcuts/{shortcut_id}/edit', ['as' => 'shortcuts.update', 'uses' => 'ShortcutController@update'])->middleware('auth');
Route::get('profiles/{id}/shortcuts', ['as' => 'shortcuts.index', 'uses' => 'ShortcutController@index'])->middleware('auth');




// SHORTCUTS MANAGEMENT

Route::get('shortcuts', ['as' => 'shortcutsMgt.index', 'uses' => 'ShortcutController@massIndex'])->middleware('auth');
Route::get('shortcuts/massCreate', ['as' => 'shortcutsMgt.massCreate', 'uses' => 'ShortcutController@massCreate'])->middleware('auth');
Route::post('shortcuts/massStore', ['as' => 'shortcutsMgt.massStore', 'uses' => 'ShortcutController@massStore'])->middleware('auth');
Route::get('shortcuts/{shortcut_id}/duplicate', ['as' => 'shortcutsMgt.duplicate', 'uses' => 'ShortcutController@duplicate'])->middleware('auth');
Route::post('shortcuts/{shortcut_id}/duplicate', ['as' => 'shortcutsMgt.duplicateStore', 'uses' => 'ShortcutController@duplicateStore'])->middleware('auth');
Route::get('shortcuts/massDelete', ['as' => 'shortcutsMgt.massDelete', 'uses' => 'ShortcutController@massDelete'])->middleware('auth');
Route::delete('shortcuts/massDelete', ['as' => 'shortcutsMgt.massDestroy', 'uses' => 'ShortcutController@massDestroy'])->middleware('auth');





// TASKS

Route::get('tasks/assignHosts', ['as' => 'tasks.assignHosts', 'uses' => 'TaskController@assignHosts'])->middleware('auth');
Route::post('tasks/assignHosts', ['as' => 'tasks.updateHosts', 'uses' => 'TaskController@updateHosts'])->middleware('auth');
Route::get('tasks/assignHostGroup', ['as' => 'tasks.assignHostGroup', 'uses' => 'TaskController@assignHostGroup'])->middleware('auth');
Route::post('tasks/assignHostGroup', ['as' => 'tasks.updateHostGroup', 'uses' => 'TaskController@updateHostGroup'])->middleware('auth');
Route::get('tasks/{task_id}/host/{host_id}/unassignHost/{couple_id}', ['as' => 'tasks.unassignHost', 'uses' => 'TaskController@unassignHostConfirmation'])->middleware('auth');
Route::delete('tasks/unassignHost/{couple_id}', ['as' => 'tasks.destroyHost', 'uses' => 'TaskController@unassignHost'])->middleware('auth');
Route::get('tasks/active', ['as' => 'tasks.active', 'uses' => 'TaskController@activeTasks'])->middleware('auth');
Route::get('tasks/create', ['as' => 'tasks.create', 'uses' => 'TaskController@create'])->middleware('auth');
Route::post('tasks', ['as' => 'tasks.store', 'uses' => 'TaskController@store'])->middleware('auth');
Route::get('tasks', ['as' => 'tasks.index', 'uses' => 'TaskController@index'])->middleware('auth');
Route::get('tasks/delete/{id}', ['as' => 'tasks.delete', 'uses' => 'TaskController@destroyConfirmation'])->middleware('auth');
Route::get('tasks/massDelete', ['as' => 'tasks.massDelete', 'uses' => 'TaskController@massDelete'])->middleware('auth');
Route::delete('tasks/massDelete', ['as' => 'tasks.massDestroy', 'uses' => 'TaskController@massDestroy'])->middleware('auth');
Route::get('tasks/active/toggleHidden/{show}', ['as' => 'tasks.toggleHiddenTasks', 'uses' => 'TaskController@toggleHiddenTasks'])->middleware('auth');
Route::delete('tasks/{id}', ['as' => 'tasks.destroy', 'uses' => 'TaskController@destroy'])->middleware('auth');
Route::get('tasks/{id}/edit', ['as' => 'tasks.edit', 'uses' => 'TaskController@edit'])->middleware('auth');
Route::patch('tasks/{id}', ['as' => 'tasks.update', 'uses' => 'TaskController@update'])->middleware('auth');





// AUTH
Auth::routes(['register' => false]);