<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class TokenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // delete
        DB::table('Login')->where("name", "parxshell")->delete();
        DB::table('personal_access_tokens')->where("name", "parxshell-token")->delete();

        DB::table('Login')->where("name", "pfsic")->delete();
        DB::table('personal_access_tokens')->where("name", "pfsic-token")->delete();

	    // add parxshell user
        DB::table('Login')->insert(
            array (
                'id' => 8,
                'name' => 'parxshell',
                'email' => 'parxshell@parx.org',
                'email_verified_at' => NULL,
                'password' => '$2y$10$wIi/vFmhz980UOySSzSVyeVFkYfYNfAv8QopPhN3miCbt8gU/093q',
                'remember_token' => NULL,
                'created_at' => '2022-06-07 08:45:56',
                'updated_at' => '2022-06-07 08:45:56',
            )
        );

        // add parxshell token (1|rKdOtRmmdsq14VeRESgaaKR5fkytWf4teIjSm6uI)
        DB::table('personal_access_tokens')->insert(
            array (
                'id' => 1,
                'tokenable_type' => 'App\\Login',
                'tokenable_id' => 8,
                'name' => 'parxshell-token',
                'token' => '3726dda792528de2c5dd7f2d2a38a5c5bff5657f9266b386349449887f93eb9d',
                'abilities' => '["*"]',
                'last_used_at' => NULL,
                'created_at' => '2022-06-08 10:46:43',
                'updated_at' => '2022-06-08 10:46:43',
            )
        );
    


	    // add pfsic user (1prcXvIleNPpus6)
        DB::table('Login')->insert(
            array (
                'id' => 9,
                'name' => 'pfsic',
                'email' => 'pfsic@parx.org',
                'email_verified_at' => NULL,
                'password' => '$2y$10$.0S8xrF8REFYvALjq0KDCOcrOioVHz.BCRtr7uhIJRcXHkW0cTPvS',
                'remember_token' => NULL,
                'created_at' => '2023-03-29 08:42:53',
                'updated_at' => '2023-03-29 08:42:53',
            )
        );

        // add pfsic token (2|pgSwjd0sU9dTll1EVx9EJfD5W3Zq3DbqBTZBIW9y)
        DB::table('personal_access_tokens')->insert(
            array (
                'id' => 2,
                'tokenable_type' => 'App\\Login',
                'tokenable_id' => 9,
                'name' => 'pfsic-token',
                'token' => '3afe60aaaf675f4bc7df81511f38b90649ea35b085fefb451c4718315ddf1e2c',
                'abilities' => '["*"]',
                'last_used_at' => NULL,
                'created_at' => '2023-03-29 08:42:53',
                'updated_at' => '2023-03-29 08:42:53',
            )
        );
    




    }

    
}
