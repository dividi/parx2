<?php


// Don't forget to put "$this->call('CreateAllTablesSeeder');" in app/database/seeders/DatabaseSeeder.php
// To run it

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CreateAllTablesSeeder extends Seeder {

	public function run() {

	    // Create a default user in DB
		if (!DB::table('Login')->where("name", "admin")->exists() ){
			DB::table('Login')->insert(['name' => 'admin', 'email' => 'admin@parx.org', 'password' => Hash::make('ParxPass')]);
		}

	    // disable foreign key constraint check
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');

	    // Seed the Parameter table
	  	DB::table('Parameter')->truncate();
  		DB::table('Parameter')->insert(["id" => "1",	"name" => 'proxyUrlIE',			"defaultValue" => '', 	"description" => "adresse et port pour le proxy IE (url:port)"]);
		DB::table('Parameter')->insert(["id" => "2",	"name" => 'proxyUrlFF',			"defaultValue" => '', 	"description" => "adresse et port pour le proxy Firefox (url:port)"]);
		DB::table('Parameter')->insert(["id" => "3",	"name" => 'hiddenDrives',		"defaultValue" => '', 	"description" => "liste des lecteurs masqués (C,D,E,...)"]);
		DB::table('Parameter')->insert(["id" => "4",	"name" => 'hideControlPanel',	"defaultValue" => '0', 	"description" => "masque le panneau de configuration (1 pour masquer ou 0)"]);
		DB::table('Parameter')->insert(["id" => "5",	"name" => 'hideRunMenu',		"defaultValue" => '0', 	"description" => "montre ou masque le bouton d’exécution (1 pour masquer ou 0)"]);
		DB::table('Parameter')->insert(["id" => "6",	"name" => 'disableRegistry',	"defaultValue" => '0', 	"description" => "désactive l'accès à la base de registre (1 pour désactiver le registre ou 0)"]);
		DB::table('Parameter')->insert(["id" => "7",	"name" => 'bindHomeFolder',		"defaultValue" => '', 	"description" => "lecteur à associer au dossier mes documents (par exemple : U)"]);
		DB::table('Parameter')->insert(["id" => "8",	"name" => 'disallowRun',		"defaultValue" => '', 	"description" => "liste des applications bloquées (par ex : calc.exe, notepad.exe, ...)"]);
		DB::table('Parameter')->insert(["id" => "9",	"name" => 'changeHomePageIE',	"defaultValue" => '', 	"description" => "url de la page de démarrage de IE (url)"]);
		DB::table('Parameter')->insert(["id" => "10",	"name" => 'changeHomePageFF',	"defaultValue" => '', 	"description" => "url de la page de démarrage de Firefox (url)"]);
		DB::table('Parameter')->insert(["id" => "11",	"name" => 'lockTimer',			"defaultValue" => '50',	"description" => "le délai entre 2 recherches de tâche (en secondes). mettre 0 pour ne pas utiliser."]);
		DB::table('Parameter')->insert(["id" => "12",	"name" => 'monitorSleep',		"defaultValue" => '10',	"description" => "Mise en veille du moniteur (en minutes)"]);
		DB::table('Parameter')->insert(["id" => "13",	"name" => 'systemSleep',		"defaultValue" => '20',	"description" => "Mise en veille du système (en minutes)"]);
		DB::table('Parameter')->insert(["id" => "16",	"name" => 'shutdownTime',		"defaultValue" => '', 	"description" => "Heure d'extinction du client (rien pour pas d'arrêt, ou format HH:MM)"]);
		DB::table('Parameter')->insert(["id" => "17",	"name" => 'proxyBypassIE',		"defaultValue" => '', 	"description" => "Ne pas utiliser le proxy pour ces adresses sous IE (adresses séparées par des virgules)"]);
		DB::table('Parameter')->insert(["id" => "18",	"name" => 'proxyBypassFF',		"defaultValue" => '', 	"description" => "Ne pas utiliser le proxy pour ces adresses sous FF (adresses séparées par des virgules)"]);
		DB::table('Parameter')->insert(["id" => "19",	"name" => 'proxyTypeFF',		"defaultValue" => '', 	"description" => "Type de proxy Firefox (0=aucun, 1=manuel, 2=PAC, 4=autodetect, 5=system proxy)"]);
		DB::table('Parameter')->insert(["id" => "22",	"name" => 'proxyScriptIE',		"defaultValue" => '', 	"description" => "Adresse de configuration automatique du proxy IE"]);
		DB::table('Parameter')->insert(["id" => "23",	"name" => 'proxyScriptFF',		"defaultValue" => '', 	"description" => "Adresse de configuration automatique du proxy FF"]);
		DB::table('Parameter')->insert(["id" => "24",	"name" => 'disableDesktop',		"defaultValue" => '0', 	"description" => "Désactive l'écriture sur le bureau"]);
		DB::table('Parameter')->insert(["id" => "25",	"name" => 'disableTaskManager',	"defaultValue" => '0', 	"description" => "Désactive le gestionnaire des tâches"]);



		// Seed the Task table
		DB::table('Task')->whereIn("id", [1,2,3,4])->delete();
		DB::table('Task')->insert(["id" => "1",	"name" => 'shutdown',	"exe" => '------', 	"args" => "", 	"asAdmin" => 0, 	"description" => "Eteint le PC"]);
		DB::table('Task')->insert(["id" => "2",	"name" => 'reboot',		"exe" => '------', 	"args" => "", 	"asAdmin" => 0, 	"description" => "Redémarre le PC"]);
		DB::table('Task')->insert(["id" => "3",	"name" => 'logout',		"exe" => '------', 	"args" => "", 	"asAdmin" => 0, 	"description" => "Déconnecte l'utilisateur"]);
		DB::table('Task')->insert(["id" => "4",	"name" => 'update',		"exe" => '------', 	"args" => "",	"asAdmin" => 0, 	"description" => "Met à jour le client ParxShell"]);


		// Seed the Task table
		DB::table('Config')->whereIn("id", [1,2,3,4])->delete();
		DB::table('Config')->insert(["id" => "1",	"key" => 'ldap_server',	"value" => '']);
		DB::table('Config')->insert(["id" => "2",	"key" => 'ldap_dn',	"value" => '']);
		DB::table('Config')->insert(["id" => "3",	"key" => 'ldap_login',	"value" => '']);
		DB::table('Config')->insert(["id" => "4",	"key" => 'gui_theme',	"value" => '']);


		// Add a default profile
		DB::table('Profile')->where("id", 9999)->delete();
		DB::table('Profile')->insert(["id" => "9999", "name" => 'DEFAULT', "matchType" => 0]);
		



		// enable foreign key constraint check
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

	}

}
