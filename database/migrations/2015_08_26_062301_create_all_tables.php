<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        // Table Config
        Schema::create('Config', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key')->default('');
            $table->string('value')->default('')->nullable();
        });

        // Table Profile
        Schema::create('Profile', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('');
            $table->timestamp('revision')->nullable();
            $table->json('fileAssoc')->nullable();
            $table->smallInteger('matchType');
        });



        // Table Host
        Schema::create('Host', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->default('');
            $table->string('os', 255)->default('')->nullable();
            $table->string('version', 255)->default('')->nullable();
        });



        // Table HostGroup
        Schema::create('HostGroup', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50)->default('');
        });



        // Table Parameter
        Schema::create('Parameter', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->default('');
            $table->string('defaultValue', 100)->default('')->nullable();
            $table->string('description')->default('')->nullable();
        });



        // Table Printer
        Schema::create('Printer', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('');
            $table->text('target')->nullable();
        });



        // Table Share
        Schema::create('Share', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('');
            $table->text('target')->nullable();
        });



        // Table Shortcut
        Schema::create('Shortcut', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->default('');
            $table->text('target')->nullable();
            $table->string('arguments')->default('')->nullable();
            $table->string('workingDir')->default('')->nullable();
            $table->string('description')->default('')->nullable();
            $table->smallInteger('location')->nullable();
            $table->tinyInteger('startup')->nullable();
            $table->integer('profile_id')->unsigned()->index();
            $table->foreign('profile_id')->references('id')->on('Profile')->onDelete('cascade');
        });



        // Table Task
        Schema::create('Task', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->default('');
            $table->text('exe');
            $table->text('args')->nullable();
            $table->smallInteger('asAdmin')->nullable();
            $table->text('description')->nullable();
        });



        // Table User
        Schema::create('User', function (Blueprint $table) {
            $table->increments('id');
            $table->string('login')->default('');
            $table->string('firstName')->default('')->nullable();
            $table->string('lastName')->default('')->nullable();
        });



        // Table UserGroup
        Schema::create('UserGroup', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->default('');
        });




        // Pivots

        // Table pivot Host - Task
        Schema::create('host_task', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('host_id')->unsigned()->index();
            $table->foreign('host_id')->references('id')->on('Host')->onDelete('cascade');
            $table->integer('task_id')->unsigned()->index();
            $table->foreign('task_id')->references('id')->on('Task')->onDelete('cascade');
            $table->smallInteger('recurtion')->nullable();
            $table->text('schedule')->nullable();

        });


        // Table pivot Profile - User
        Schema::create('user_profile', function(Blueprint $table) {
            $table->integer('profile_id')->unsigned()->index();
            $table->foreign('profile_id')->references('id')->on('Profile')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('User')->onDelete('cascade');
        });



        // Table pivot User - UserGroup
        Schema::create('user_usergroup', function(Blueprint $table) {
            $table->integer('usergroup_id')->unsigned()->index();
            $table->foreign('usergroup_id')->references('id')->on('UserGroup')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('User')->onDelete('cascade');
        });



        // Table pivot Profile - Parameter (must be changed to profile_parameter)
        Schema::create('ProfileParameter', function(Blueprint $table) {
            $table->integer('profile_id')->unsigned()->index();
            $table->foreign('profile_id')->references('id')->on('Profile')->onDelete('cascade');
            $table->integer('parameter_id')->unsigned()->index();
            $table->foreign('parameter_id')->references('id')->on('Parameter');
            $table->string('parameterValue')->default('');
        });



        // Table pivot Profile - Share (must be changed to profile_share)
        Schema::create('ProfileShare', function(Blueprint $table) {
            $table->integer('profile_id')->unsigned()->index();
            $table->foreign('profile_id')->references('id')->on('Profile')->onDelete('cascade');
            $table->integer('share_id')->unsigned()->index();
            $table->foreign('share_id')->references('id')->on('Share')->onDelete('cascade');
            $table->string('shareLetter', 1)->default('');
        });



        // Table pivot Host - HostGroup
        Schema::create('host_hostgroup', function(Blueprint $table) {
            $table->integer('host_id')->unsigned()->index();
            $table->foreign('host_id')->references('id')->on('Host')->onDelete('cascade');
            $table->integer('hostgroup_id')->unsigned()->index();
            $table->foreign('hostgroup_id')->references('id')->on('HostGroup')->onDelete('cascade');
        });



        // Table pivot Host - Profile
        Schema::create('host_profile', function(Blueprint $table) {
            $table->integer('host_id')->unsigned()->index();
            $table->foreign('host_id')->references('id')->on('Host')->onDelete('cascade');
            $table->integer('profile_id')->unsigned()->index();
            $table->foreign('profile_id')->references('id')->on('Profile')->onDelete('cascade');
        });



        // Table pivot Printer - Profile
        Schema::create('profile_printer', function(Blueprint $table) {
            $table->integer('printer_id')->unsigned()->index();
            $table->foreign('printer_id')->references('id')->on('Printer')->onDelete('cascade');
            $table->integer('profile_id')->unsigned()->index();
            $table->foreign('profile_id')->references('id')->on('Profile')->onDelete('cascade');
            $table->boolean('default');
        });





    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('host_task');
        Schema::drop('host_hostgroup');
        Schema::drop('host_profile');
        Schema::drop('ProfileParameter');
        Schema::drop('ProfileShare');
        Schema::drop('profile_printer');
        Schema::drop('user_profile');
        Schema::drop('user_usergroup');
        Schema::drop('HostGroup');
        Schema::drop('UserGroup');
        Schema::drop('Shortcut');
        Schema::drop('Parameter');
        Schema::drop('Printer');
        Schema::drop('Profile');
        Schema::drop('Share');
        Schema::drop('Task');
        Schema::drop('User');
        Schema::drop('Host');
        Schema::drop('Config');

    }
}
