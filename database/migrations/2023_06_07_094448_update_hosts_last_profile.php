<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateHostsLastProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Host', function (Blueprint $table) {
            $table->string('last_profile_name', 255)->default('')->nullable();
            $table->timestamp('last_connection')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Host', function (Blueprint $table) {
            $table->dropColumn('last_profile_name');
            $table->dropColumn('last_connection');
        });
    }
}
