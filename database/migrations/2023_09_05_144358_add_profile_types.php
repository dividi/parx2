<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProfileTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Table pivot Profile - User
        Schema::create('usergroup_profile', function(Blueprint $table) {
            $table->integer('profile_id')->unsigned()->index();
            $table->foreign('profile_id')->references('id')->on('Profile')->onDelete('cascade');
            $table->integer('usergroup_id')->unsigned()->index();
            $table->foreign('usergroup_id')->references('id')->on('UserGroup')->onDelete('cascade');
        });

        // Table pivot Profile - User
        Schema::create('hostgroup_profile', function(Blueprint $table) {
            $table->integer('profile_id')->unsigned()->index();
            $table->foreign('profile_id')->references('id')->on('Profile')->onDelete('cascade');
            $table->integer('hostgroup_id')->unsigned()->index();
            $table->foreign('hostgroup_id')->references('id')->on('HostGroup')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usergroup_profile');
        Schema::drop('hostgroup_profile');
    }
}
