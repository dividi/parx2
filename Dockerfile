# DOCKER FILE LOCAL
# to build "docker build -t parx:dev ."


ARG BUILD_ENV=dev
ARG VENDOR_INSTALL="yes"


# PRODUCTION BASE

FROM ubuntu:22.04 as production
ARG DEBIAN_FRONTEND=noninteractive

# install
RUN apt-get update -y && \
	apt-get install -y  --no-install-recommends apache2 php8.1 php8.1-mysql libapache2-mod-php8.1 php8.1-mbstring php8.1-xmlrpc php8.1-curl php8.1-soap php8.1-gd php8.1-ldap php8.1-xml php8.1-cli php8.1-zip locales unzip curl gnupg ca-certificates && \
	apt-get install -y  --no-install-recommends build-essential -V && \
	locale-gen 'fr_FR.UTF-8' 'en_US.UTF-8' && \
	a2enmod rewrite && \
	apt-get clean -y && \
    apt-get autoclean -y && \
    apt-get autoremove -y && \
	rm -rf /var/lib/apt/lists/*





# DEV

FROM production as dev

ENV WORKDIR /var/www/html
ARG DEBIAN_FRONTEND=noninteractive
ARG COMPOSER_ALLOW_SUPERUSER=1


RUN apt-get update -y && \
    apt-get install -y ca-certificates build-essential gnupg curl git unzip php-pcov && \
    mkdir -p /etc/apt/keyrings && \
    curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg && \
    echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_18.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list && \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    apt-get update -y && \
    apt-get install -y nodejs yarn

# composer
RUN curl -Ss https://getcomposer.org/installer | php && \
    mv composer.phar /usr/local/bin/composer

WORKDIR ${WORKDIR}
COPY . $WORKDIR

RUN if [ "$BUILD_ENV" = "production" ]; then \
        composer install --optimize-autoloader --no-dev --no-progress --no-interaction && chown -R www-data:www-data $WORKDIR && \
        yarn install && yarn run production && find . -type d -name node_modules | xargs rm -rf;\
    elif [ "$VENDOR_INSTALL" != "no" ]; then \
        composer install --no-progress --no-interaction && chown -R www-data:www-data $WORKDIR && \
        yarn install; \
    fi

CMD /usr/sbin/apache2ctl -DFOREGROUND

# MAIN

ARG BUILD_ENV
FROM ${BUILD_ENV} as final
LABEL MAINTAINER="David LE VIAVANT"
ENV DEBIAN_FRONTEND=noninteractive
ENV WORKDIR /var/www/html

# set the working directory
WORKDIR ${WORKDIR}

# copy from base image
COPY --from=dev $WORKDIR $WORKDIR

# Apache and php config
COPY a2site.conf /etc/apache2/sites-available/000-default.conf
COPY php.ini /etc/php/8.1/apache2/php.ini

# RUN

# expose a port
EXPOSE 80

CMD ["./web.sh"]
