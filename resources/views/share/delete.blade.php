<center>
	<form method="POST" action={{ URL::route( 'shares.destroy', [$share->id] )  }}>
		
		<input type="hidden" name="_method" value="DELETE">

		{!! csrf_field() !!}

		<div>

			Vous allez supprimer le partage {{ $share->name }}?<br>

		</div>
		
		
		<br />

		<input type='submit' class='btn btn-danger' value='Supprimer'>
		<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>

	</form>
</center>
