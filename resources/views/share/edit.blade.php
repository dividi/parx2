@extends('layout')


@section('navtitle') Modification d'un partage @stop

@section('help') Modification d'un partage. @stop


@section('body')


<div class="form-container">

    {!! Form::model($share, ['method'=>'PATCH', 'route'=>['shares.update', $share->id]]) !!}
	
    <table class="form-table">
        <tr>
            <td>{!! Form::label('name', 'Nom') !!}</td>
            <td>{!! Form::text('name') !!}</td>
        </tr>
        <tr>
            <td>{!! Form::label('target', 'Chemin') !!}</td>
            <td>{!! Form::textarea('target', null, ['size' => '25x3']) !!}</td>
        </tr>

    </table>

    <div class="cardbox">
        {!! Form::submit('Modifier', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>


    {!! Form::close() !!}

</div>

<script>
    $(function() {
        $("input:first").focus();
        $(".chosen").chosen();
    });

</script>

@stop
