{{-- List of assigned shares to a profile --}}



@extends('layout')


@section('navtitle') PARTAGES de {{ $profile->name }}  <li class="title-info"> {{ count($profile->shares) }} </li> @stop

@section('help') Les partages disponibles sur le profile {{ $profile->name}} . @stop


@section('buttons')
	<li> <a href={{ URL::route('profiles.assignShare', array('id'=>$profile->id) ) }} class='btn' title='Régler les partages de {{$profile->name}}'><i class="fa fa-link"></i> Associer</a> </li>
@stop


@section('body')

	<div class="table-responsive">

		<table class="nice">
			<thead>
				<tr>
					<th>Nom</th>
					<th>Chemin</th>
					<th>Lettre</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
				</tr>
			</thead>


			@foreach ($profile->shares as $share)
			
				<tr>
					<td> {{$share->name}} </td>
					<td> {{$share->target}} </td>
					<td> {{$share->pivot->shareLetter}} </td>
					
					<td class="line-button"><a href={{ URL::route('profiles.unassignShare', array('id'=>$profile->id, 'share_id'=>$share->id) ) }} class='modalbox btn' title="Retirer le partage {{$share->name}}"><i class="fa fa-chain-broken"></i></a> </td>
				
				</tr>

			@endforeach

		</table>
	</div>

@stop