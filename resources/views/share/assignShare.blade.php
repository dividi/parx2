@extends('layout')


@section('navtitle') Assigner un partage @stop

@section('help') Assigner un partage. @stop


@section('body')


<div class="form-container">

    {!! Form::open(array('method'=>'PATCH', 'route'=>['profiles.updateShare', $profile->id] )) !!}

    <table class="form-table">

        <tr>
            <td>{!! Form::label('share', 'Partages') !!}</td>
            <td>{!! Form::select('share', $shares, null, array("class" => "chosen")) !!}</td>
        </tr>

        <tr>
            <td>{!! Form::label('shareLetter', 'Lettre') !!}</td>
            <td>{!! Form::select('shareLetter', array('D' => 'D', 'E' => 'E', 'F' => 'F', 'G' => 'G', 'H' => 'H', 'I' => 'I', 'J' => 'J', 'K' => 'K', 'L' => 'L', 'M' => 'M', 'N' => 'N', 'O' => 'O', 'P' => 'P', 'Q' => 'Q', 'R' => 'R', 'S' => 'S', 'T' => 'T', 'U' => 'U', 'V' => 'V', 'W' => 'W', 'X' => 'X', 'Y' => 'Y', 'Z' => 'Z'), null, array("class" => "chosen")) !!}</td>
        </tr>

    </table>

    <div class="cardbox">
        {!! Form::submit('Assigner', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>


    {!! Form::close() !!}

</div>

<script>
    $(function() {
        $("input:first").focus();
        $(".chosen").chosen();
    });

</script>

@stop
