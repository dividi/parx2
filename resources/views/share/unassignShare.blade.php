<center>
	{!! Form::open(array('method'=>'DELETE', 'route'=>['profiles.destroyShare', $profile->id, $share->id] )) !!}

		<div>

			Voulez vous supprimer le partage {{ $share->name}} du profil {{ $profile->name }}?<br>
			
			<span class="small-font">
				Les partages associés ne seront pas supprimés.
			</span>


		</div>
		
		
		<br />
			
		{!! Form::submit('Retirer', ['class'=>'btn btn-danger']) !!}
		<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>

	{!! Form::close() !!}

	</center>


