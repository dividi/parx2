<!DOCTYPE html>
<html>
    <head>
        <title>404 ! </title>

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .logo {
              height:200px;
            }

            .title {
                font-size: 64px;
                margin-bottom: 5px;
            }

            .text {
                font-size: 32px;
                margin-bottom: 40px;
            }

            .subtext {
                font-size: 20px;
                margin-bottom: 40px;
            }

            a:link, a:visited {
              background-color: #B0BEC5;
              color: white;
              padding: 14px 25px;
              text-align: center;
              text-decoration: none;
              display: inline-block;
            }

            a:hover, a:active {
              background-color: #cecece;
              color:black;
            }



        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <img class="logo" src={{ asset('images/dead_link.png')	}}></img>
                <div class="title">404</div>
                <div class="text">Damn, you've found a dead Link</div>
                <div class="subtext"><a href={{ URL::to('/') }}>Home page</a></div>
            </div>
        </div>
    </body>
</html>
