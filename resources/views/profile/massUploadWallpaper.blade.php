@extends('layout')


@section('navtitle') Importer des données dans la sélection de profils @stop

@section('help') Permet d'importer des données d'un profil dans une sélection de profils. @stop


@section('body')

<div class="form-container">



	<div class="cardbox text-danger">
		Attention ! Vous allez modifier le wallpaper de <b>{{ count($selected_profiles) }} profil(s)</b> ({{ $selected_profiles->implode(",") }})
	</div>

	<div class="cardbox">
		Le papier peint doit être au format JPG. <br />
		Le poids du papier peint doit être inférieur à 5Mo. <br />
		Si aucun papier peint n'est sélectionné, le papier peint stocké sera supprimé.
	</div>

	
		{!! Form::open(array('route' => ['profiles.massStoreWallpaper'], 'method'=>'POST', 'files'=>true)) !!}
			{!! Form::label('Ajouter un nouveau wallpaper') !!}
			{!! Form::file('wallpaper') !!}
			{!! Form::hidden('id') !!}
					
			{{-- Ids of selected items --}}
			<input type="hidden" id="ids" name="ids" value={{ $ids }} />

			<br />
		
      		{!! Form::submit('Envoyer', array('class'=>'btn btn-primary')) !!}
      		<button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
			
      	{!! Form::close() !!}

</div>

@stop