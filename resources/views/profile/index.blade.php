@extends('layout')

@section('navtitle') PROFILS 
	<li id="selectedCount" class="title-info hidden-button hide"></li>
	<li class="title-info hidden-button hide" title="sélectionnés">/</li>
	<li class="title-info"> <span title="affichés">{{ count($profiles) }} </span> / <span title="total">{{$total_count}}</li> @stop


@section('help') Liste des profils @stop


@section('buttons')
	<li class="hidden-button hide"> <a href="" class='btn' id="submit_mass_parameters" title='Modifier les paramètres pour les profils sélectionnés'><i class="fa fa-edit"></i> Paramètres </a></li>
	<li class="hidden-button hide"> <a href="" class='btn' id="submit_mass_wallpaper" title='Modifier le papier peint pour les profils sélectionnés'><i class="fa fa-picture-o"></i> Papier Peint </a> </li>
	<li class="hidden-button hide"> <a href="" class='btn' id="submit_mass_import" title='Import des données pour les profils sélectionnés'><i class="fa fa-download"></i> Importer	</a> </li>
	<li> <a href="{{ URL::route('profiles.create' ) }}" title='Ajouter un nouveau profil'><i class="fa fa-plus-circle"></i> Ajouter</a> </li>
@stop


@section('body')



<form id="form_mass_parameters" method="POST" action="{{ route('parameters.massParameters') }}">
    @csrf
	<input type="hidden" name="selection_ids" class="selection_ids" value="">
</form>


<form id="form_mass_import" method="POST" action="{{ route('profiles.showMassImport') }}">
    @csrf
	<input type="hidden" name="selection_ids" class="selection_ids" value="">
</form>


<form id="form_mass_wallpaper" method="POST" action="{{ route('profiles.massUploadWallpaper') }}">
    @csrf
	<input type="hidden" name="selection_ids" class="selection_ids" value="">
</form>

	<div class="table-responsive">


	{!! Form::open( ['url' => route('profiles.index'), 'method' => 'get', 'id' => 'filter_form'] )!!}	

		<table class="nice">
			<thead>
				<tr>
				<th data-sorter="false" data-filter="false"><input type="checkbox" class="checkall"></th>
				<th>Nom</th>
				<th>Test sur</th>
				
				<th><i class="fa fa-desktop" title="Nombre d'hôtes dans le profil"></i> </th>
				<th><i class="fa fa-user" title="Nombre d'utilisateurs dans le profil"></i> </th>
				<th><i class="fa fa-desktop" title="Nombre de groupe d'hôtes dans le profil"></i> </th>
				<th><i class="fa fa-users" title="Nombre de groupes d'utilisateurs dans le profil"></i> </th>
			
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				</tr>

				<tr class="filters">
					<th></th>
					<th>{!! Form::text('filter_name', $request['filter_name'], array('size' => 6, 'class' => 'filter_field') ) !!}</th>
					<th>
						{!! Form::select('filter_match_type', [
						''	=> '',
						'0' => 'H+U',
						'1' => 'U',
						'2' => 'H',
						'3' => 'GH',
						'4' => 'GU',
						'5' => 'GH+GU',

						], $request['filter_match_type'], array('id' => 'match_type', 'class' => 'filter_field')) !!}
					</th>
					
					<!-- <th>{!! Form::text('filter_match_type', $request['filter_match_type'], array('size' => 6) ) !!}</th> -->
					<th>{!! Form::text('filter_host_count', $request['filter_host_count'], array('size' => 3, 'class' => 'filter_field') ) !!}</th>
					<th>{!! Form::text('filter_user_count', $request['filter_user_count'], array('size' => 3, 'class' => 'filter_field') ) !!}</th>
					<th>{!! Form::text('filter_hostgroup_count', $request['filter_hostgroup_count'], array('size' => 3, 'class' => 'filter_field') ) !!}</th>
					<th>{!! Form::text('filter_usergroup_count', $request['filter_usergroup_count'], array('size' => 3, 'class' => 'filter_field') ) !!}</th>					
					
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					
					<th colspan=4>
						{!! Form::Button("<i class='fa fa-search'></i>", array('onclick' => "submit()")) !!}
						{!! Form::Button("<i class='fa fa-times-circle'></i>", array('onclick' => "$('#filter_form').find('input').val('') ; $('#match_type option:eq(0)').prop('selected', true) ; submit() ")) !!}
					</th>
				</tr>

			</thead>

			@foreach ($profiles as $profile)

				<tr>
					<td class="line-button"><input type="checkbox" class="checkline" name={{ $profile->id }} id={{ $profile->id }}></td>
					<td> <a href={{ URL::route('profiles.show', array('id'=>$profile->id) ) }} title="Information de {{$profile->name}}"> {{$profile->name}} </a> </td>

					<td> 
						@if ($profile->matchType  == 0)
							Hôte + Utilisateur
						@elseif ($profile->matchType  == 1)
							Utilisateur
						@elseif ($profile->matchType  == 2)
							Hôte
						@elseif ($profile->matchType  == 3)
							Groupe hôtes
						@elseif ($profile->matchType  == 4)
							Groupe utilisateurs
						@elseif ($profile->matchType  == 5)
							Groupe hôte + Groupe utilisateur
						@endif
					</td>

					<td> @if (count($profile->hosts) > 0) <span class="badge">{{ count($profile->hosts) }}</span> @endif </td>
					<td> @if (count($profile->users) > 0) <span class="badge">{{ count($profile->users) }}</span>  @endif </td>
					<td> @if (count($profile->hostgroups) > 0) <span class="badge">{{ count($profile->hostgroups) }}</span>  @endif </td>
					<td> @if (count($profile->usergroups) > 0) <span class="badge">{{ count($profile->usergroups) }}</span>  @endif </td>

					<td class="line-button"><a href={{ URL::route('profiles.show', array('id'=>$profile->id) ) }} class='btn' title="Information de {{$profile->name}}"><i class="fa fa-info-circle"></i></a></td>
					<td class="line-button"><a href={{ URL::route('profiles.parameters', array('id'=>$profile->id) ) }} class='btn' title='Régler les paramètres de {{$profile->name}}'><i class="fa fa-sliders"></i></a></td>
					<td class="line-button"><a href={{ URL::route('shortcuts.index', array('id'=>$profile->id) ) }} class='btn' title='Régler les raccourcis de {{$profile->name}}'><i class="fa fa-share-square"></i></a></td>					
					<td class="line-button"><a href={{ URL::route('profiles.assignedPrinters', array('id'=>$profile->id) ) }} class='btn' title='Régler les imprimantes de {{$profile->name}}'><i class="fa fa-print"></i></a></td>
					<td class="line-button"><a href={{ URL::route('profiles.assignedShares', array('id'=>$profile->id) ) }} class='btn' title='Régler les partages de {{$profile->name}}'><i class="fa fa-share-alt"></i></a></td>
					<td class="line-button"><a href={{ URL::route('profiles.showImport', array('id'=>$profile->id) ) }} class='btn' title='Importer des données vers {{$profile->name}}'><i class="fa fa-download"></i></a></td>
					<td class="line-button"><a href={{ URL::route('profiles.uploadWallpaper', array('id'=>$profile->id) ) }} class='btn' title='Importer un papier peint pour le profil {{$profile->name}}'><i class="fa fa-picture-o"></i></a></td>
					<td class="line-button"><a href={{ URL::route('profiles.chooseFileAssociation', array('id'=>$profile->id) ) }} class='btn' title='Gérer les associations de fichiers pour le profil {{$profile->name}}'><i class="fa fa-star"></i></a></td>
					<td class="line-button"><a href={{ URL::route('profiles.edit', array('id'=>$profile->id) ) }} class='btn' title='Modifier le profil {{$profile->name}}'><i class="fa fa-pencil"></i></a></td>
					<td class="line-button"><a href={{ URL::route('profiles.delete', array('id'=>$profile->id) ) }} class='modalbox btn' title='Supprimer le profil {{$profile->name}}'><i class="fa fa-trash"></i></a> </td>
				</tr>

			@endforeach
		</table>

		<div class="flex-item item2">
			{{ $profiles->appends(compact('request'))->links('pagination::bootstrap-4') }}
		</div>


	</div>



	<script language="JavaScript" src="{{ URL::asset('/js/all.js') }}"></script>

	<script>
	    $(function () {

	        $('.filters input').keypress(function (e) {
	            if (e.which == 13) {
	                $('#filter_form').submit();
	                return false;
	            }
	        });


	        $("#submit_mass_parameters").click(function () {

	            event.preventDefault();
	            // selected ids serialization
	            let ids = $(".checkline:checked").map(function (index) {
	                return this.id;
	            }).get().join();

	            $(".selection_ids").val(ids);

	            $("#form_mass_parameters").submit();

	        });


	        $("#submit_mass_import").click(function () {

	            event.preventDefault();
	            // selected ids serialization
	            let ids = $(".checkline:checked").map(function (index) {
	                return this.id;
	            }).get().join();

	            $(".selection_ids").val(ids);

	            $("#form_mass_import").submit();

	        });


	        $("#submit_mass_wallpaper").click(function () {

	            event.preventDefault();
	            // selected ids serialization
	            let ids = $(".checkline:checked").map(function (index) {
	                return this.id;
	            }).get().join();

	            $(".selection_ids").val(ids);

	            $("#form_mass_wallpaper").submit();

	        });



	        // selected ids serialization
	        var ids = $(".checkline:checked").map(function (index) {
	            return this.id;
	        }).get().join();

	        $("#ids").val(ids);

	    });
	</script>



@stop