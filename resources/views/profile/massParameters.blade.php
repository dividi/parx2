
@extends('layout')

@section('navtitle') OPTIONS de la sélection de profils  @stop

@section('help') Les paramètres du profil permettent de régler la sécurité et les paramètres du système. @stop

@section('buttons')
	<li class="hidden-button hide"> <a href="#" title='Sauvegarder les paramètres sélectionnés' onclick="myForm.submit();"><i class="fa fa-floppy-o"></i> Sauvegarder</a> </li>
@stop


@section('body')


<span id="warn-msg" class="text-danger"></span><br />
<span class="text-danger"> Modifier massivement des paramètres sur une sélection de profils peut être dangereux, restez concentré ! </span><br /><br />

	<div class="table-responsive">

	<form method="POST" id="myForm" action="{{ route('parameters.updateProfilesParameters') }}">

		{!! csrf_field() !!}

    	{{-- Ids of selected users (it is not really secured, but I do not give a sh**) --}}

		<input type="hidden" id="profile_ids" name="profile_ids" value={{ $ids }} />


	 	<table class="nice">

			<th>A modifier</th>
			<th>Option</th>
			<th>Valeur</th>
			<th>Description</th>


	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[1]"></td>
		<td class="small-font param-name">proxyUrlIE</td>
		<td class="param-value"><input class="originalInput" name="param[1]" type='text' value=""></td>
		<td class="small-font">adresse et port pour le proxy IE (url:port)</td>
	</tr>


	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[2]"></td>
		<td class="small-font param-name">proxyUrlFF</td>
		<td class="param-value"><input class="originalInput" name="param[2]" type='text' value=""></td>
		<td class="small-font">adresse et port pour le proxy Firefox (url:port)</td>
	</tr>



	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[3]"></td>
		<td class="small-font param-name">hiddenDrives</td>
		<td class="param-value"><input class="originalInput" name="param[3]" type='text' value=""></td>
		<td class="small-font">liste des lecteurs masqués (C,D,E,...)</td>
	</tr>



	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[4]"></td>
		<td class="small-font param-name">hideControlPanel</td>
		<td class="param-value">
			<select name="param[4]">
				<option value=1 >ACTIVE</option>
				<option value=0 selected >DESACTIVE</option>
			</select>
		</td>
		<td class="small-font">Masque le panneau de configuration</td>
	</tr>



	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[5]"></td>
		<td class="small-font param-name">hideRunMenu</td>
		<td class="param-value">
			<select name="param[5]">
				<option value=1 >ACTIVE</option>
				<option value=0 selected >DESACTIVE</option>
			</select>
		</td>
		<td class="small-font">montre ou masque le bouton d'exécution</td>
	</tr>



	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[6]"></td>
		<td class="small-font param-name">disableRegistry</td>
		<td class="param-value">
			<select name="param[6]" >
				<option value=1 >ACTIVE</option>
				<option value=0 selected >DESACTIVE</option>
			</select>
		</td>
		<td class="small-font">Désactive l'accès à la base de registre</td>
	</tr>



	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[7]"></td>
		<td class="small-font param-name">bindHomeFolder</td>
		<td class="param-value"><input class="originalInput" name="param[7]" type='text' value=""></td>
		<td class="small-font">lecteur à associer au dossier mes documents (par exemple : U)</td>
	</tr>



	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[8]"></td>
		<td class="small-font param-name">disallowRun</td>
		<td class="param-value"><input class="originalInput" name="param[8]" type='text' value=""></td>
		<td class="small-font">liste des applications bloquées (par ex : calc.exe, notepad.exe, ...)</td>
	</tr>



	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[9]"></td>
		<td class="small-font param-name">changeHomePageIE</td>
		<td class="param-value"><input class="originalInput" name="param[9]" type='text' value=""></td>
		<td class="small-font">url de la page de démarrage de IE (url)</td>
	</tr>



	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[10]"></td>
		<td class="small-font param-name">changeHomePageFF</td>
		<td class="param-value"><input class="originalInput" name="param[10]" type='text' value=""></td>
		<td class="small-font">url de la page de démarrage de Firefox (url)</td>
	</tr>



	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[11]"></td>
		<td class="small-font param-name">lockTimer</td>
		<td class="param-value"><input class="originalInput" name="param[11]" type='number' value=50></td>
		<td class="small-font">le délai avant verrouillage de la session (en minutes). mettre 0 pour ne pas utiliser.</td>
	</tr>



	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[12]"></td>
		<td class="small-font param-name">monitorSleep</td>
		<td class="param-value"><input class="originalInput" name="param[12]" type='number' value=30></td>
		<td class="small-font">Mise en veille du moniteur (en minutes)</td>
	</tr>



	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[13]"></td>
		<td class="small-font param-name">systemSleep</td>
		<td class="param-value"><input class="originalInput" name="param[13]" type='number' value=60></td>
		<td class="small-font">Délai de mise en veille du système (en minutes)</td>
	</tr>



	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[16]"></td>
		<td class="small-font param-name">shutdownTime</td>
		<td class="param-value"><input class="originalInput" name="param[16]" type='time' value=""></td>
		<td class="small-font">Heure d'extinction du client (rien pour pas d'arrêt, ou format HH:MM)</td>
	</tr>



	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[17]"></td>
		<td class="small-font param-name">proxyBypassIE</td>
		<td class="param-value"><input class="originalInput" name="param[17]" type='text' value=""></td>
		<td class="small-font">Ne pas utiliser le proxy pour ces adresses sous IE (adresses séparées par des virgules)</td>
	</tr>



	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[18]"></td>
		<td class="small-font param-name">proxyBypassFF</td>
		<td class="param-value"><input class="originalInput" name="param[18]" type='text' value=""></td>
		<td class="small-font">Ne pas utiliser le proxy pour ces adresses sous FF (adresses séparées par des virgules)</td>
	</tr>


	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[19]"></td>
		<td class="small-font param-name">proxyTypeFF</td>
		<td class="param-value">
			<select class="originalInput" name="param[19]" >
				<option value=0 selected>AUCUN</option>
				<option value=1 >MANUEL</option>
				<option value=2 >PAC</option>
				<option value=4 >AUTODETECT</option>
				<option value=5 >SYSTEM PROXY</option>
			</select>
		</td>
		<td class="small-font">Type de proxy Firefox</td>
	</tr>



	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[22]"></td>
		<td class="small-font param-name">proxyScriptIE</td>
		<td class="param-value"><input class="originalInput" name="param[22]" type='text' value=""></td>
		<td class="small-font">Adresse de configuration automatique du proxy IE</td>
	</tr>



	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[23]"></td>
		<td class="small-font param-name">proxyScriptFF</td>
		<td class="param-value"><input class="originalInput" name="param[23]" type='text' value=""></td>
		<td class="small-font">Adresse de configuration automatique du proxy FF</td>
	</tr>



	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[24]"></td>
		<td class="small-font param-name">disableDesktop</td>
		<td class="param-value">
			<select class="originalInput" name="param[24]">
				<option value=1 >ACTIVE</option>
				<option value=0 selected >DESACTIVE</option>
			</select>
		</td>
		<td class="small-font">Désactive l'écriture sur le bureau</td>
	</tr>



	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[25]"></td>
		<td class="small-font param-name">disableTaskManager</td>
		<td class="param-value">
			<select name="param[25]">
				<option value=1  >ACTIVE</option>
				<option value=0 selected >DESACTIVE</option>
			</select>
		</td>
		<td class="small-font">Désactive le gestionnaire des tâches</td>
	</tr>


	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[26]"></td>
		<td class="small-font param-name">autoEndTasks</td>
		<td class="param-value">
			<select name="param[26]">
				<option value=1  >ACTIVE</option>
				<option value=0 selected >DESACTIVE</option>
			</select>
		</td>
		<td class="small-font">Force l'arrêt des process sur fermeture de session </td>
	</tr>


	<tr>
		<td class="line-button"><input type="checkbox" class="checkline" name="check[27]"></td>
		<td class="small-font param-name">disableCMD</td>
		<td class="param-value">
			<select name="param[27]">
				<option value=1 >ACTIVE</option>
				<option value=0 selected >DESACTIVE</option>
			</select>
		</td>
		<td class="small-font">Désactive la ligne de commande</td>
	</tr>


		</table>
	</form>



	<script language="JavaScript" src="{{ URL::asset('/js/all.js') }}"></script>

	<script>
		
		$(function(){

			// selected ids serialization
			var ids = $(".checkline:checked").map(function(index) {
    			return this.id; 
			}).get().join();

			$("#warn-msg").html("Attention ! Vous allez modifier les paramètres de <b>" + $("#profile_ids").val().split(",").length + " profils </b> !");

		});


	</script>

@stop
