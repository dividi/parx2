
@extends('layout')


@section('navtitle') PROFIL {{ $profile->name }}  @stop

@section('help') La page complète du profil. @stop


@section('body')

	<a href={{ URL::route('profiles.index') }} class='btn btn-primary'><i class="fa fa-caret-left"></i></i> RETOUR A LA LISTE</a>


	<div class="page-title"> {{ $profile->name }} </div>

	<div class="page-actions">
		<a href={{ URL::route('profiles.delete', array('modal'=>'true', 'id'=>$profile->id) ) }} class='modalbox btn btn-danger' title='Supprimer {{$profile->name}}'><i class="fa fa-trash"></i> SUPPRIMER</a>
		<a href={{ URL::route('profiles.edit', array('id'=>$profile->id) ) }} class='btn btn-primary' title='Modifier {{$profile->name}}'><i class="fa fa-pencil"></i> MODIFIER</a>
	</div>


	<br>



<div style="display:flex; flex-wrap:wrap;">





<div class="panel panel-primary" style="max-width:200px; margin:15px;">
	  <div class="panel-heading">
	    <h3 class="panel-title">HÔTES <span class="badge">{{ count($profile->hosts) }}</span> </h3>
	  </div>
	  <div class="panel-body" style="overflow-y:auto; max-height:200px;">
		<ul>
			@forelse($profile->hosts as $host)
		    	<li>{{ $host->name }}</li>
			@empty
		   		<p>pas d'hôte associé</p>
			@endforelse
		</ul>
	  </div>
	</div>



	<div class="panel panel-primary" style="max-width:200px; margin:15px;">
	  <div class="panel-heading">
	  <h3 class="panel-title">UTILISATEURS <span class="badge">{{ count($profile->users) }}</span> </h3>
	  </div>
	  <div class="panel-body" style="overflow-y:auto; max-height:200px;">
		<ul>
			@forelse($profile->users as $user)
		    	<li>{{ $user->firstName }}  {{ $user->lastName }} ({{ $user->login }})</li>
			@empty
		   		<p>pas d'utilisateur associé</p>
			@endforelse
		</ul>
	  </div>
	</div>



	<div class="panel panel-primary" style="max-width:200px; margin:15px;">
	  <div class="panel-heading">
	  <h3 class="panel-title">GROUPES UTILISATEURS <span class="badge">{{ count($profile->usergroups) }}</span> </h3>
	  </div>
	  <div class="panel-body" style="overflow-y:auto; max-height:200px;">
		<ul>
			@forelse($profile->usergroups as $usergroup)
		    	<li>{{ $usergroup->name }}</li>
			@empty
		   		<p>pas de groupe d'utilisateurs associé</p>
			@endforelse
		</ul>
	  </div>
	</div>



	<div class="panel panel-primary" style="max-width:200px; margin:15px;">
	  <div class="panel-heading">
	  <h3 class="panel-title">GROUPES HOTES <span class="badge">{{ count($profile->hostgroups) }}</span> </h3>
	  </div>
	  <div class="panel-body" style="overflow-y:auto; max-height:200px;">
		<ul>
			@forelse($profile->hostgroups as $hostgroup)
		    	<li>{{ $hostgroup->name }}</li>
			@empty
		   		<p>pas de groupe d'hôtes associé</p>
			@endforelse
		</ul>
	  </div>
	</div>

	<div class="panel panel-primary" style="max-width:200px; margin:15px;">
	  <div class="panel-heading">
	    <h3 class="panel-title">Wallpaper </h3>
	  </div>
	  <div class="panel-body" style="overflow-y:auto; max-height:200px;">
	  	@if(file_exists('wallpapers/' . $profile->id . '.jpg')) 
			<img src="/wallpapers/{{$profile->id}}.jpg" style="width:200px;margin-left:auto;margin-right:auto;display:block;" />    
		@else
			Pas de wallpaper pour ce profil...
		@endif
	  </div>

	</div>



	<div class="panel panel-primary" style="max-width:200px; margin:15px;">
	  <div class="panel-heading">
	  	<h3 class="panel-title">IMPRIMANTES <span class="badge">{{ count($profile->printers) }}</span> </h3>
	  </div>
	  <div class="panel-body" style="overflow-y:auto; max-height:200px;">
		<ul>
			@forelse($profile->printers as $printer)
		    	<li>{{ $printer->name }} sur {{ $printer->target }}</li>
			@empty
		   		<p>pas d'imprimante associée</p>
			@endforelse
		</ul>
	  </div>
	</div>



	<div class="panel panel-primary" style="max-width:200px; margin:15px;">
	  <div class="panel-heading">
	  <h3 class="panel-title">PARTAGES <span class="badge">{{ count($profile->shares) }}</span> </h3>
	  </div>
	  <div class="panel-body" style="overflow-y:auto; max-height:200px;">
		<ul>
			@forelse($profile->shares as $share)
		    	<li>{{ $share->pivot->shareLetter}} : {{ $share->name }} sur {{ $share->target }}</li>
			@empty
		   		<p>pas de partage associé</p>
			@endforelse
		</ul>
	  </div>
	</div>



	<div class="panel panel-primary" style="max-width:200px; margin:15px;">
	  <div class="panel-heading">
	  <h3 class="panel-title">RACCOURCIS <span class="badge">{{ count($profile->shortcuts) }}</span> </h3>
	  </div>
	  <div class="panel-body" style="overflow-y:auto; max-height:200px;">
		<ul>
			@forelse($profile->shortcuts as $shortcut)
		    	<li>{{ $shortcut->name }} sur {{ $shortcut->target }}</li>
			@empty
		   		<p>pas de raccourci associé</p>
			@endforelse
		</ul>
	  </div>
	</div>



	<div class="panel panel-primary" style="max-width:200px; margin:15px;">
	  <div class="panel-heading">
	  <h3 class="panel-title">PARAMETRES</h3>
	  </div>
	  <div class="panel-body" style="overflow-y:auto; max-height:200px;">
		<ul>
			@forelse($profile->parameters as $parameter)
		    	<li>{{ $parameter->name }} : [ {{ $parameter->pivot->parameterValue }} ]</li>
			@empty
		   		<p>pas de paramètre associé</p>
			@endforelse
		</ul>
	  </div>
	</div>



</div>


@stop