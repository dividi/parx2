@extends('layout')

@section('navtitle')      Importer des données dans la sélection de profils

@stop

@section('body')

<div class="form-container">

    {!! Form::open(array('method'=>'PATCH', 'route' => 'profiles.massImport')) !!}

    {{-- Ids of selected items --}}
    <input type="hidden" id="ids" name="ids" value={{ $ids }} />

    <div class="cardbox text-danger">
        Attention ! Vous allez modifier les données de <b>{{ count($selected_profiles) }} profil(s)</b>
        ({{ $selected_profiles->implode(",") }})
    </div>

    <table class="form-table">

        <tr>
            <td> {!! Form::label('other_profiles', 'A partir du profil ') !!}</td>
            <td> {!! Form::select('other_profiles', $other_profiles, null, array('multiple' => false, 'class' => ['chosen'])) !!}</td>
        </tr>

        <tr valign="top">
            <td> {!! Form::label('matchType', 'Importer : ') !!} </td>
            <td>
                {!! Form::checkbox("import_shortcuts") !!} Raccourcis <br />
                {!! Form::checkbox("import_printers") !!} Imprimantes <br />
                {!! Form::checkbox("import_shares") !!} Partages <br />
                {!! Form::checkbox("import_options") !!} Options <br />
                {!! Form::checkbox("import_file_associations") !!} Associations de Fichiers <br />
            </td>
        </tr>
    </table>

    <div class="cardbox">

        {!! Form::submit('Importer', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>

    {!! Form::close() !!}

    <script>
        $(function () {

            $(".chosen").chosen();
        });
    </script>


    @stop