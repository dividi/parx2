<center>
	<form method="POST" action={{ URL::route( 'profiles.destroy', [$profile->id] )  }}>
		
		<input type="hidden" name="_method" value="DELETE">

		{!! csrf_field() !!}

		<div>

			Vous allez supprimer le profil {{ $profile->name }}?<br>
			
			<span class="small-font">
				Les raccourcis associés seront supprimés.
			</span>


		</div>
		
		
		<br />

		<input type='submit' class='btn btn-danger' value='Supprimer'>
		<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>

	</form>
</center>

