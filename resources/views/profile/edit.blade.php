@extends('layout')


@section('navtitle') Modification du profil {{$profile->name}}@stop

@section('help') Modification d'un profil @stop

@section('body')


<div class="form-container">

    {!! Form::model($profile, ['method'=>'PATCH', 'route'=>['profiles.update', $profile->id]]) !!}

    <table class="form-table">
        <tr>
            <td>{!! Form::label('name', 'Nom') !!}</td>
            <td>{!! Form::text('name', null, array('required' => '')) !!}</td>
        </tr>
        <tr>
            <td>{!! Form::label('matchType', 'Test sur') !!}</td>
            <td>{!! Form::select('matchType', [
                '0' => 'Hôte + Utilisateur',
                '1' => 'Utilisateur',
                '2' => 'Hôte',
                '3' => 'Groupe hôtes',
                '4' => 'Groupe utilisateurs',
                '5' => 'Groupe hôte + Groupe utilisateur',

                ], null, array("class" => "chosen")) !!}</td>
        </tr>
    </table>

    <br />

    {!! Form::submit('Modifier', ['class'=>'btn btn-primary']) !!}
    <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>


    {!! Form::close() !!}

</div>

<script>
    // give focus to first field
    $("input:first").focus();
    $(".chosen").chosen();

</script>

@stop
