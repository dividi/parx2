@extends('layout')


@section('navtitle') Importer des données dans le profil {{ $profile->name }}  @stop

@section('help') Permet d'importer des données d'un profil dans un autre. @stop


@section('body')

<div class="form-container">


    {!! Form::model($profile, ['method'=>'POST', 'route'=>['profiles.import', $profile->id]]) !!}

	<table class="form-table">

        <tr>
            <td> {!! Form::label('other_profiles', 'A partir du profil ') !!}</td>
            <td> {!! Form::select('other_profiles', $other_profiles, null, array('multiple' => false, 'class' => ['chosen']) ) !!}</td>
        </tr>

        <tr valign="top">
            <td> {!! Form::label('matchType', 'Importer') !!}</td>
            <td>    
                {!! Form::checkbox("import_shortcuts") !!} Raccourcis <br />
                {!! Form::checkbox("import_printers") !!} Imprimantes <br />
                {!! Form::checkbox("import_shares") !!} Partages <br />
                {!! Form::checkbox("import_options") !!} Options <br />
                {!! Form::checkbox("import_file_associations") !!} Associations de Fichiers <br />
            </td>
        </tr>
    </table>

    <br />

    {!! Form::submit('Importer', ['class'=>'btn btn-primary']) !!}
    <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>


    {!! Form::close() !!}


    <script>
        $("input:first").focus();
        $(".chosen").chosen();

    </script>

    @stop