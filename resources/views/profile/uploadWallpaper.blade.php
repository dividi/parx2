@extends('layout')


@section('navtitle') Papier peint pour le profil {{$profile->name}}@stop

@section('help') Modification d'un profil @stop

@section('body')

<div class="form-container">

	<div class="cardbox">
	    Le papier peint doit être au format JPG. <br />
	    Le poids du papier peint doit être inférieur à 5Mo. <br />
	    Si aucun papier peint n'est sélectionné, le papier peint stocké sera supprimé.
	</div>

	<div class="panel panel-primary" style="max-width:200px; margin:15px;">
	  <div class="panel-heading">
	    <h3 class="panel-title">Wallpaper actuel </h3>
	  </div>
	  <div class="panel-body" style="overflow-y:auto; max-height:200px;">
	  	@if(file_exists('wallpapers/' . $profile->id . '.jpg')) 
			<img src="/wallpapers/{{$profile->id}}.jpg" style="width:200px;margin-left:auto;margin-right:auto;display:block;" />    
		@else
			Pas de wallpaper pour ce profil...
		@endif
	  </div>

	</div>

	<br />
	
	{!! Form::open(array('route' => ['profiles.storeWallpaper', $id], 'method'=>'POST', 'files'=>true)) !!}
		{!! Form::label('Ajouter un nouveau wallpaper') !!}
		{!! Form::file('wallpaper') !!}
		{!! Form::hidden('id') !!}
		<br />
		{!! Form::submit('Envoyer', array('class'=>'btn btn-primary')) !!}
		<button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
	{!! Form::close() !!}

</div>

@stop