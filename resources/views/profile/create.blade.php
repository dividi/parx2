@extends('layout')


@section('navtitle') Création d'un profil @stop

@section('help') Création d'un nouveau profil. @stop


@section('body')

<div class="form-container">

    <form action="{{ route('profiles.store') }}" method="post" accept-charset="utf-8">

        {!! csrf_field() !!}

        <table class="form-table">
            <tr>
                <td><label for="name">Nom</label></td>
                <td><input type="text" name="name" required></td>
            </tr>

            <tr valign="top">
                <td><label for="matchType">Test sur</label></td>
                <td>
                    <select name="matchType" class="chosen">
                        <option value="0">Hôte + Utilisateur</option>
                        <option value="1">Utilisateur</option>
                        <option value="2">Hôte</option>
                        <option value="3">Groupe hôtes</option>
                        <option value="4">Groupe utilisateurs</option>
                        <option value="5">Groupe hôte + Groupe utilisateur</option>
                    </select>
                </td>
            </tr>

        </table>

        <div class="cardbox">
            <button type="submit" class="btn btn-primary">Créer</button>
            <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
        </div>
    </form>

</div>

<script>
    $("input:first").focus();
    $(".chosen").chosen();

</script>

@stop
