@extends('layout')


@section('navtitle') Association des fichiers dans le profil {{ $profile->name }} @stop

@section('help') Permet de choisir les applications associées à certains fichiers. @stop



@section('body')

<div class="form-container">

    <div class="">
        Vous pouvez choisir un programme dans la liste ou ajouter le <b>chemin complet</b>
        vers une autre application en le marquant dans la case en première ligne et en validant avec entrée.
    </div>


    {!! Form::open(array('route' => ['profiles.storeFileAssociation', $id], 'method'=>'PATCH')) !!}

    <br />

    <table class="nice">
        <thead>
            <tr>
                <th>Type</th>
                <th>File</th>
            </tr>
        </thead>
        <tbody>

            <tr class="chosen" valign="top">
                <td>Navigateur</td>
                <td>
                    <select name="browser" class="chosen-select">
                        @if($json)
                            <option value="{{ $json->browser }}">{{ $json->browser }}</option>
                        @endif
                        <option value="ne_pas_modifier">Ne pas modifier</option>
                        <option value="Firefox">Firefox</option>
                        <option value="Chrome">Chrome</option>
                        <option value="Edge">Edge</option>
                    </select>
                </td>
            </tr>

            <tr class="chosen" valign="top">
                <td>Bureautique</td>
                <td>
                    <select name="office" class="chosen-select">
                        @if($json)
                            <option value="{{ $json->office }}">{{ $json->office }}</option>
                        @endif
                        <option value="ne_pas_modifier">Ne pas modifier</option>
                        <option value="Libre Office">Libre Office</option>
                        <option value="Open Office">Open Office</option>
                        <option value="Office365">Office365</option>
                    </select>
                </td>
            </tr>

            <tr class="chosen" valign="top">
                <td>Lecteur PDF</td>
                <td>
                    <select name="pdf" class="chosen-select">
                        @if($json)
                            <option value="{{ $json->pdf }}">{{ $json->pdf }}</option>
                        @endif
                        <option value="ne_pas_modifier">Ne pas modifier</option>
                        <option value="Acrobat Reader">Acrobat Reader</option>
                        <option value="Foxit Reader">Foxit Reader</option>
                        <option value="Firefox">Firefox</option>
                        <option value="Chrome">Chrome</option>
                        <option value="Edge">Edge</option>
                    </select>
                </td>
            </tr>

            <tr class="chosen" valign="top">
                <td>Image</td>
                <td>
                    <select name="image" class="chosen-select">
                        @if($json)
                            <option value="{{ $json->image }}">{{ $json->image }}</option>
                        @endif
                        <option value="ne_pas_modifier">Ne pas modifier</option>
                        <option value="XnView">XnView</option>
                        <option value="Photos">Photos</option>
                    </select>
                </td>
            </tr>

            <tr class="chosen" valign="top">
                <td>Vidéo</td>
                <td>
                    <select name="video" class="chosen-select">
                        @if($json)
                            <option value="{{ $json->video }}">{{ $json->video }}</option>
                        @endif
                        <option value="ne_pas_modifier">Ne pas modifier</option>
                        <option value="VLC">VLC</option>
                        <option value="Photos">Photos</option>
                        <option value="Windows Media Player">Windows Media Player</option>
                    </select>
                </td>
            </tr>

            <tr class="chosen" valign="top">
                <td>Musique</td>
                <td>
                    <select name="music" class="chosen-select">
                        @if($json)
                            <option value="{{ $json->music }}">{{ $json->music }}</option>
                        @endif
                        <option value="ne_pas_modifier">Ne pas modifier</option>
                        <option value="VLC">VLC</option>
                        <option value="Windows Media Player">Windows Media Player</option>
                    </select>
                </td>
            </tr>

        </tbody>
    </table>

    <br />
    <br />

    {!! Form::submit('Envoyer', array('class'=>'btn btn-primary')) !!}
    <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    {!! Form::close() !!}


    </center>


    <script>
        jQuery(document).ready(function () {

            jQuery(".chosen-select").chosen({
                    width: "250px",
                    no_results_text: "Appuyez sur entrée pour ajouter "
                })
                .on('chosen:no_results', function (evt, data) {
                    jQuery(data.chosen.container).on('keydown', function (event) {

                        if (13 == event.keyCode && !jQuery(data.chosen.form_field).find(
                                'option[value="' + data.chosen.get_search_text() + '"]').length) {
                            jQuery(data.chosen.form_field).append('<option value="' + data.chosen
                                .get_search_text() + '" selected>' +
                                data.chosen.get_search_text() + '</option>');
                            jQuery(data.chosen.form_field).trigger('chosen:updated');
                            data.chosen.result_highlight = data.chosen.search_results.find(
                                'li.active-result').lasteturn;
                            data.chosen.result_select(evt);
                        }
                    });
                });
        });

    </script>


</div>

@stop
