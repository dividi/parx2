
@extends('layout')





@section('navtitle') OPTIONS de {{ $p["name"] }}  @stop

@section('help') Les paramètres du profil permettent de régler la sécurité et les paramètres du système. @stop

@section('buttons')
	<li> <a href="#" title='Sauvegarder les paramètres de {{ $p["name"] }}' onclick="myForm.submit();"><i class="fa fa-floppy-o"></i> Sauvegarder</a> </li>
@stop


@section('body')


	<div class="table-responsive">

	<form method="POST" id="myForm">

		{!! csrf_field() !!}


	 	<table class="nice">

			<th>Option</th>
			<th>Valeur</th>
			<th>Description</th>


	<tr>
		<td class="small-font param-name">proxyUrlIE</td>
		<td class="param-value"><input class="originalInput" name="param[1]" type='text' value={{ $p["parameters"]["1"] }}></td>
		<td class="small-font">adresse et port pour le proxy IE (url:port)</td>
	</tr>


	<tr>
		<td class="small-font param-name">proxyUrlFF</td>
		<td class="param-value"><input class="originalInput" name="param[2]" type='text' value={{ $p["parameters"]["2"] }}></td>
		<td class="small-font">adresse et port pour le proxy Firefox (url:port)</td>
	</tr>



	<tr>
		<td class="small-font param-name">hiddenDrives</td>
		<td class="param-value"><input class="originalInput" name="param[3]" type='text' value={{ $p["parameters"]["3"] }}></td>
		<td class="small-font">liste des lecteurs masqués (C,D,E,...)</td>
	</tr>



	<tr>
		<td class="small-font param-name">hideControlPanel</td>
		<td class="param-value">
			<select name="param[4]">
				<option value=1 {{ $p["parameters"]["4"]==1 ? ' selected' : '' }} >ACTIVE</option>
				<option value=0 {{ $p["parameters"]["4"]==0 ? ' selected' : '' }} >DESACTIVE</option>
			</select>
		</td>
		<td class="small-font">Masque le panneau de configuration</td>
	</tr>



	<tr>
		<td class="small-font param-name">hideRunMenu</td>
		<td class="param-value">
			<select name="param[5]">
				<option value=1 {{ $p["parameters"]["5"]==1 ? ' selected' : '' }} >ACTIVE</option>
				<option value=0 {{ $p["parameters"]["5"]==0 ? ' selected' : '' }} >DESACTIVE</option>
			</select>
		</td>
		<td class="small-font">montre ou masque le bouton d'exécution</td>
	</tr>



	<tr>
		<td class="small-font param-name">disableRegistry</td>
		<td class="param-value">
			<select name="param[6]" >
				<option value=1 {{ $p["parameters"]["6"]==1 ? ' selected' : '' }} >ACTIVE</option>
				<option value=0 {{ $p["parameters"]["6"]==0 ? ' selected' : '' }} >DESACTIVE</option>
			</select>
		</td>
		<td class="small-font">Désactive l'accès à la base de registre</td>
	</tr>



	<tr>
		<td class="small-font param-name">bindHomeFolder</td>
		<td class="param-value"><input class="originalInput" name="param[7]" type='text' value={{ $p["parameters"]["7"] }}></td>
		<td class="small-font">lecteur à associer au dossier mes documents (par exemple : U)</td>
	</tr>



	<tr>
		<td class="small-font param-name">disallowRun</td>
		<td class="param-value"><input class="originalInput" name="param[8]" type='text' value={{ $p["parameters"]["8"] }}></td>
		<td class="small-font">liste des applications bloquées (par ex : calc.exe, notepad.exe, ...)</td>
	</tr>



	<tr>
		<td class="small-font param-name">changeHomePageIE</td>
		<td class="param-value"><input class="originalInput" name="param[9]" type='text' value={{ $p["parameters"]["9"] }}></td>
		<td class="small-font">url de la page de démarrage de IE (url)</td>
	</tr>



	<tr>
		<td class="small-font param-name">changeHomePageFF</td>
		<td class="param-value"><input class="originalInput" name="param[10]" type='text' value={{ $p["parameters"]["10"] }}></td>
		<td class="small-font">url de la page de démarrage de Firefox (url)</td>
	</tr>



	<tr>
		<td class="small-font param-name">lockTimer</td>
		<td class="param-value"><input class="originalInput" name="param[11]" type='number' value={{ $p["parameters"]["11"] }}></td>
		<td class="small-font">le délai avant verrouillage de la session (en minutes). mettre 0 pour ne pas utiliser.</td>
	</tr>



	<tr>
		<td class="small-font param-name">monitorSleep</td>
		<td class="param-value"><input class="originalInput" name="param[12]" type='number' value={{ $p["parameters"]["12"] }}></td>
		<td class="small-font">Mise en veille du moniteur (en minutes)</td>
	</tr>



	<tr>
		<td class="small-font param-name">systemSleep</td>
		<td class="param-value"><input class="originalInput" name="param[13]" type='number' value={{ $p["parameters"]["13"] }}></td>
		<td class="small-font">Délai de mise en veille du système (en minutes)</td>
	</tr>



	<tr>
		<td class="small-font param-name">shutdownTime</td>
		<td class="param-value"><input class="originalInput" name="param[16]" type='time' value={{ $p["parameters"]["16"] }}></td>
		<td class="small-font">Heure d'extinction du client (rien pour pas d'arrêt, ou format HH:MM)</td>
	</tr>



	<tr>
		<td class="small-font param-name">proxyBypassIE</td>
		<td class="param-value"><input class="originalInput" name="param[17]" type='text' value={{ $p["parameters"]["17"] }}></td>
		<td class="small-font">Ne pas utiliser le proxy pour ces adresses sous IE (adresses séparées par des virgules)</td>
	</tr>



	<tr>
		<td class="small-font param-name">proxyBypassFF</td>
		<td class="param-value"><input class="originalInput" name="param[18]" type='text' value={{ $p["parameters"]["18"] }}></td>
		<td class="small-font">Ne pas utiliser le proxy pour ces adresses sous FF (adresses séparées par des virgules)</td>
	</tr>


	<tr>
		<td class="small-font param-name">proxyTypeFF</td>
		<td class="param-value">
			<select class="originalInput" name="param[19]" value={{ $p["parameters"]["19"] }} >
				<option value=0 {{ $p["parameters"]["19"]==0 ? ' selected' : '' }}>AUCUN</option>
				<option value=1 {{ $p["parameters"]["19"]==1 ? ' selected' : '' }}>MANUEL</option>
				<option value=2 {{ $p["parameters"]["19"]==2 ? ' selected' : '' }}>PAC</option>
				<option value=4 {{ $p["parameters"]["19"]==4 ? ' selected' : '' }}>AUTODETECT</option>
				<option value=5 {{ $p["parameters"]["19"]==5 ? ' selected' : '' }}>SYSTEM PROXY</option>
			</select>
		</td>
		<td class="small-font">Type de proxy Firefox</td>
	</tr>



	<tr>
		<td class="small-font param-name">proxyScriptIE</td>
		<td class="param-value"><input class="originalInput" name="param[22]" type='text' value={{ $p["parameters"]["22"] }}></td>
		<td class="small-font">Adresse de configuration automatique du proxy IE</td>
	</tr>



	<tr>
		<td class="small-font param-name">proxyScriptFF</td>
		<td class="param-value"><input class="originalInput" name="param[23]" type='text' value={{ $p["parameters"]["23"] }}></td>
		<td class="small-font">Adresse de configuration automatique du proxy FF</td>
	</tr>



	<tr>
		<td class="small-font param-name">disableDesktop</td>
		<td class="param-value">
			<select class="originalInput" name="param[24]">
				<option value=1 {{ $p["parameters"]["24"]==1 ? ' selected' : '' }} >ACTIVE</option>
				<option value=0 {{ $p["parameters"]["24"]==0 ? ' selected' : '' }} >DESACTIVE</option>
			</select>
		</td>
		<td class="small-font">Désactive l'écriture sur le bureau</td>
	</tr>



	<tr>
		<td class="small-font param-name">disableTaskManager</td>
		<td class="param-value">
			<select name="param[25]">
				<option value=1 {{ $p["parameters"]["25"]==1 ? ' selected' : '' }} >ACTIVE</option>
				<option value=0 {{ $p["parameters"]["25"]==0 ? ' selected' : '' }} >DESACTIVE</option>
			</select>
		</td>
		<td class="small-font">Désactive le gestionnaire des tâches</td>
	</tr>


	<tr>
		<td class="small-font param-name">autoEndTasks</td>
		<td class="param-value">
			<select name="param[26]">
				<option value=1 {{ $p["parameters"]["26"]==1 ? ' selected' : '' }} >ACTIVE</option>
				<option value=0 {{ $p["parameters"]["26"]==0 ? ' selected' : '' }} >DESACTIVE</option>
			</select>
		</td>
		<td class="small-font">Force l'arrêt des process sur fermeture de session </td>
	</tr>


	<tr>
		<td class="small-font param-name">disableCMD</td>
		<td class="param-value">
			<select name="param[27]">
				<option value=1 {{ $p["parameters"]["27"]==1 ? ' selected' : '' }} >ACTIVE</option>
				<option value=0 {{ $p["parameters"]["27"]==0 ? ' selected' : '' }} >DESACTIVE</option>
			</select>
		</td>
		<td class="small-font">Désactive la ligne de commande</td>
	</tr>


		</table>
	</form>



	<script language="JavaScript" src="{{ URL::asset('/js/all.js') }}"></script>

@stop
