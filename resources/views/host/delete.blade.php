<center>
	<form method="POST" action={{ URL::route( 'hosts.destroy', [$host->id] )  }}>
		
		<input type="hidden" name="_method" value="DELETE">

		{!! csrf_field() !!}

		<div>

			Vous allez supprimer l'hôte {{ $host->name }}?<br>
			
			<span class="small-font">
				
			</span>


		</div>
		
		
		<br />

		<input type='submit' class='btn btn-danger' value='Supprimer'>
		<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>

	</form>
</center>
