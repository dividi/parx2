@extends('layout')


@section('navtitle') Importer des hôtes par fichier CSV @stop

@section('body')

<div class="form-container">

    <div class="cardbox">

        Attention ! le fichier d'export généré par Parx ne peut pas être importé directement ici.<br />
        <br />
        Formalisme du fichier CSV (pas d'entête) :<br />
        nom;os;version;groupe
    </div>

    {!! Form::open(array('route' => 'hosts.importStore','method'=>'POST', 'files'=>true)) !!}


    <table class="form-table">

        <tr>
            <td>{!! Form::label('file', 'Fichier') !!}</td>
            <td>{!! Form::file('file') !!}</td>
        </tr>

    </table>

    <div class="cardbox">

        {!! Form::submit('Importer', array('class'=>'btn btn-primary')) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>

    {!! Form::close() !!}
</div>

@stop
