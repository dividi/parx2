@extends('layout')


@section('navtitle') Modifier un hôte @stop

@section('help') Permet de modifier un hôte @stop

@section('body')

<div class="form-container">

    {!! Form::model($host, ['method'=>'PATCH', 'route'=>['hosts.update', $host->id]]) !!}

	<table class="form-table">
        <tr>
            <td>{!! Form::label('name', 'Nom') !!}</td>
            <td>{!! Form::text('name', null, array('required' => '')) !!}</td>
        </tr>
        <tr>
            <td>{!! Form::label('os', 'Système') !!}</td>
            <td>{!! Form::text('os', null, array('required' => '')) !!}</td>
        </tr>

        <tr valign="top">
            <td>{!! Form::label('hostgroups', 'Groupes') !!}</td>
            <td>{!! Form::select('hostgroups[]', $hostgroups, $host->hostgroups()->pluck('id')->toArray(),
                array('multiple' => true, "class" => ['chosen'])) !!}</td>
        </tr>
        <tr valign="top">
            <td>{!! Form::label('profiles', 'Profils') !!}</td>
            <td>{!! Form::select('profiles[]', $profiles, $host->profiles()->pluck('id')->toArray(), array('multiple' =>
                true, "class" => ['chosen'])) !!}</td>
        </tr>

    </table>

    <div class="cardbox">

        {!! Form::submit('Modifier', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>

    {!! Form::close() !!}
</div>


<script>
    $(function () {
        $("input:first").focus();

        $(".chosen").chosen();
    });

</script>

@stop
