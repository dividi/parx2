@extends('layout')


@section('navtitle') Importer des hôtes par fichier CSV @stop

@section('body')

<div class="form-container">

    <div class="cardbox">
	<span class="text-danger" >
				<b>!! ATTENTION !! </b><br />
			</span>

			<div class="text-danger">
				Vous allez modifier la configuration du client de <b>{{ count($selected_hosts) }} machine(s)</b>
			</div>

			<div class="text-danger" >
				Se tromper en remplissant ce fichier peut interrompre définitivement le lien entre le client et le serveur.<br/>
			</div>
    </div> 
			
		{!! Form::open(array('method'=>'PATCH', 'route' => 'hosts.massCreateConfig')) !!}

			{{-- Ids of selected hosts (it is not really secured) --}}
			<input type="hidden" id="ids" name="ids" value={{ $ids }} />
				
<textarea name="config" style="width:100%;height:375px;">
# Version du client
$version = "20230522"


# Active / désactive le lancement du client Parx (1:activé, 0:désactivé)
$enable_parx = 1


# Adresse du serveur Parx SANS SLASH à la fin (ex : http://10.231.45.110:8000/api)
$address = ""


# Adresse du proxy (TOUJOURS mettre http:// devant l'adresse du proxy. NE PAS renseigner sous SERCOL !)
$webproxy = ""


# Kick les users sans profil (1:activé, 0:désactivé)
$noneShallPass = 0


# Montre ou cache les menus sensibles dans le gui (1:montre, 0:cache)
$full_gui = 0


# Montre ou cache les messages de parxgui (Information profil chargé, erreurs sur printer, share, shortcut)
$show_notification = 0


# Refresh rate (en minutes) de la recherche de tâches à effectuer par le service (0 : désactivé, sinon 5 minimum. 1 à 4 ne seront pas pris en compte.)
$service_refresh = 0


# Chargement automatique du profil (ie. desktop, taskbar, options, programmes, imprimantes, partages, tâches du profil)
$global:autoload_options = 1
$global:autoload_printers = 1
$global:autoload_shares = 1
$global:autoload_desktop = 1
$global:autoload_taskbar = 1
$global:autoload_associations = 1


# admin de profils (séparé par des virgules, tokens acceptés)
# Les admins de profils ne chargent pas le profil à l'ouverture de session, ont accès au fullgui et peuvent sauvegarder/restaurer des profils depuis le GUI.
$profiles_admins = "adm.*"
</textarea>
	

<div class="cardbox">	
	{!! Form::submit('Modifier', ['class'=>'btn btn-primary']) !!}
	<button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
</div>
		{!! Form::close() !!}



@stop