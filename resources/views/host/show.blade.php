
@extends('layout')


@section('navtitle') HÔTE {{ $host->name }}  @stop

@section('help') La page complète de l'hôte. @stop


@section('body')


<a href={{ URL::route('hosts.index') }} class='btn btn-primary'><i class="fa fa-caret-left"></i></i> RETOUR A LA LISTE</a>


<div class="page-title"> {{ $host->name }} </div>

<div class="page-actions">
	<a href={{ URL::route('hosts.delete', array('modal'=>'true', 'id'=>$host->id) ) }} class='modalbox btn btn-danger' title='Supprimer {{$host->name}}'><i class="fa fa-trash"></i> SUPPRIMER</a>
	<a href={{ URL::route('hosts.edit', array('id'=>$host->id) ) }} class='btn btn-primary' title='Modifier {{$host->name}}'><i class="fa fa-pencil"></i> MODIFIER</a>
</div>


<br>


<div style="display:flex; flex-wrap:wrap;">


<div class="panel panel-primary" style="max-width:200px; margin:15px;">
	  <div class="panel-heading">
	  <h3 class="panel-title">GROUPES D'HÔTES <span class="badge">{{ count($host->hostgroups) }}</span></h3>
	  </div>
	  <div class="panel-body" style="overflow-y:auto; max-height:200px;">
		<ul>
		@forelse($host->hostgroups as $hostgroup)
		    	<li>{{ $hostgroup->name }}</li>
			@empty
		   		<p>pas de groupe associé</p>
			@endforelse
		</ul>
	  </div>
	</div>



	<div class="panel panel-primary" style="max-width:200px; margin:15px;">
	  <div class="panel-heading">
	  <h3 class="panel-title">PROFILS  <span class="badge">{{ count($host->profiles) }}</span></h3>
	  </div>
	  <div class="panel-body" style="overflow-y:auto; max-height:200px;">
		<ul>
		@forelse($host->profiles as $profile)
		    	<li>{{ $profile->name }}</li>
			@empty
		   		<p>pas de profil associé</p>
			@endforelse
		</ul>
	  </div>
	</div>


	<div class="panel panel-primary" style="max-width:200px; margin:15px;">
	  <div class="panel-heading">
	  <h3 class="panel-title">TÂCHES  <span class="badge">{{ count($host->tasks) }}</span></h3>
	  </div>
	  <div class="panel-body" style="overflow-y:auto; max-height:200px;">
		<ul>
		@forelse($host->tasks as $task)
		    	<li>{{ $task->name }}</li>
			@empty
		   		<p>pas de tâche associée</p>
			@endforelse
		</ul>
	  </div>
	</div>


</div>


@stop