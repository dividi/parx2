@extends('layout')


@section('navtitle') Ajouter un nouvel hôte @stop

@section('help') Permet d'ajouter un nouvel hôte @stop

@section('body')

<div class="form-container">

    {!! Form::open(array('route' => 'hosts.store')) !!}

    <table class="form-table">
        <tr>
            <td>{!! Form::label('name', 'Nom') !!}</td>
            <td>{!! Form::text('name', null, array('required' => '')) !!}</td>
        </tr>
        <tr>
            <td>{!! Form::label('os', 'Système') !!}</td>
            <td>{!! Form::text('os', null, array('required' => '')) !!}</td>
        </tr>

        <tr valign="top">
            <td>{!! Form::label('hostgroups', 'Groupes') !!}</td>
            <td>{!! Form::select('hostgroups[]', $hostgroups, null, array('multiple' => true, "class" => ['chosen']))
                !!}</td>

        </tr>

        <tr valign="top">
            <td>{!! Form::label('profiles', 'Profils') !!}</td>
            <td>{!! Form::select('profiles[]', $profiles, null, array('multiple' => true,"class" => ['chosen'])) !!}
            </td>
        </tr>

    </table>

    
	<div class="cardbox">
        
		{!! Form::submit('Ajouter', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>

    {!! Form::close() !!}

</div>


<script>
    $(function () {
        $("input:first").focus();
        $(".chosen").chosen();
    });

</script>

@stop
