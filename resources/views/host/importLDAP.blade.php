@extends('layout')


@section('navtitle') Import des hôtes par LDAP @stop

@section('help') Permet d'ajouter et mettre à jour les hôtes par le LDAP @stop

@section('body')

<div class="form-container">

    {!! Form::open(array('route' => 'hosts.importLDAPStore','method'=>'POST')) !!}


    <table class="form-table">

        <tr class="chosen" valign="top">
            <td>{!! Form::label('ldap_server', 'Serveur') !!}</td>
            <td>{!! Form::text('ldap_server', $ldap_server->value) !!}</td>
        </tr>

        <tr class="chosen" valign="top">
            <td>{!! Form::label('ldap_dn_host', 'Chemin Machines') !!}</td>
            <td>{!! Form::text('ldap_dn_host', $ldap_host_dn->value) !!}</td>
        </tr>

        <tr class="chosen" valign="top">
            <td>{!! Form::label('ldap_login', 'Utilisateur') !!}</td>
            <td>{!! Form::text('ldap_login', $ldap_login->value) !!}</td>
        </tr>

        <tr class="chosen" valign="top">
            <td>{!! Form::label('ldap_pwd', 'Mot de passe') !!}</td>
            <td>{!! Form::password('ldap_pwd') !!}</td>
        </tr>

        <tr class="chosen" valign="top">
            <td>{!! Form::label('delete_all', 'Vider la liste avant') !!}</td>
            <td>{!! Form::checkbox('delete_all') !!}</td>
        </tr>

    </table>


    <div class="cardbox">
        {!! Form::submit('Importer', array('class'=>'btn btn-primary')) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>
    
	{!! Form::close() !!}

    @stop
