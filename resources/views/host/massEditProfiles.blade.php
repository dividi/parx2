@extends('layout')


@section('navtitle') Modification des profils associés @stop

@section('body')

<div class="form-container">

    {!! Form::open(array('method'=>'PATCH', 'route' => 'hosts.massUpdateProfiles')) !!}


    {{-- Ids of selected items --}}
    <input type="hidden" id="ids" name="ids" value={{ $ids }} />

    <div class="cardbox text-danger">
        Attention ! <br />
        Vous allez modifier les profils de <b>{{ $count_ids }} machine(s)</b><br />
        La liste des profils sera vidée.
    </div>

    <table class="form-table">

        <tr valign="top">
            <td>{!! Form::label('profiles', 'Profils') !!}</td>
            <td>{!! Form::select('profiles[]', $profiles, null, array('multiple' => true, "class" => ['chosen'])) !!}</td>
        </tr>

    </table>

    <div class="cardbox">
        {!! Form::submit('Modifier', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>

    {!! Form::close() !!}


    <script>
        $(function () {
            $(".chosen").chosen();
        });
    </script>

    @stop