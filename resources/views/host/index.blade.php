@extends('layout')

@section('navtitle') HÔTES <li id="selectedCount" class="title-info hidden-button hide"></li>
<li class="title-info hidden-button hide" title="sélectionnés">/</li>
<li class="title-info"> <span title="affichés">{{ count($hosts) }} </span> / <span title="total">{{$total_count}}</li> @stop


@section('help') Les hôtes sont les machines clientes dans Parx. Ces machines sont normalement uniques sur le réseau. @stop


@section('buttons')
<li class="topbar-button"> <a href={{ URL::route('hosts.export' ) }} title='export CSV'><i class="fa fa-file-text"></i> Export CSV</a> </li>
<li class="topbar-button"> <a href={{ URL::route('hosts.importCreate' ) }} class='' title='Importer des hôtes'><i class="fa fa-file-text"></i> Importer CSV</a> </li>
<li class="topbar-button"> <a href={{ URL::route('hosts.importLDAPCreate' ) }} class='' title='Importer par LDAP'><i class="fa fa-database"></i> Import LDAP</a> </li>

<li> <a href={{ URL::route('hosts.create' ) }} data-title="Ajouter un Hôte" class='' title='Ajouter un Hôte'><i class="fa fa-plus-circle"></i> Ajouter</a> </li>
<li class="hidden-button topbar-button hide"> <a href="" class='' id="submit_mass_config" title='Configuration des clients'><i class="fa fa-file-text"></i> Configuration</a> </li>
<li class="hidden-button topbar-button hide"> <a href="" class='' id="submit_mass_groups" title="Assigner la sélection à des groupes"><i class="fa fa-object-group"></i> Groupes </a> </li>
<li class="hidden-button topbar-button hide"> <a href="" class='' id="submit_mass_profiles" title="Assigner la sélection à des profils"><i class="fa fa-briefcase"></i> Profils </a> </li>
<li class="hidden-button topbar-button hide"> <a href={{ URL::route('hosts.massDelete' ) }} class='modalbox' data-title="Supprimer les hôtes sélectionnés" title='Supprimer les hôtes sélectionnés'><i class="fa fa-times-circle"></i> Suppression </a> </li>

@stop



@section('body')

<form id="form_mass_config" method="POST" action="{{ route('hosts.massConfig') }}">
    @csrf
	<input type="hidden" name="selection_ids" class="selection_ids" value="">
</form>

<form id="form_mass_groups" method="POST" action="{{ route('hosts.massEditGroups') }}">
    @csrf
	<input type="hidden" name="selection_ids" class="selection_ids" value="">
</form>

<form id="form_mass_profiles" method="POST" action="{{ route('hosts.massEditProfiles') }}">
    @csrf
	<input type="hidden" name="selection_ids" class="selection_ids" value="">
</form>



<div class="table-responsive">


{!! Form::open( ['url' => route('hosts.index'), 'method' => 'get', 'id' => 'filter_form'] )!!}


	<table class="nice">
		<thead>
			<tr>
				<th data-parser="false" data-sorter="false" data-filter="false"><input type="checkbox" class="checkall"></th>
				<th>Nom</th>
				<th>Système</th>
				<th>Version</th>
				<th>Dernière connexion</th>
				<th>Dernier Profil</th>
				<th><i class="fa fa-suitcase" title="Nombre de profils associés"></i></th>
				<th>Profils</th>
				<th>Groupes</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
			</tr>

			<tr class="filters">
				<th>&nbsp;</th>
				<th>{!! Form::text('filter_name', $request['filter_name'], array('size' => 6) ) !!}</th>
				<th>{!! Form::text('filter_system', $request['filter_system'], array('size' => 6) ) !!}</th>
				<th>{!! Form::text('filter_version', $request['filter_version'], array('size' => 6) ) !!}</th>
				<th>{!! Form::text('filter_last_connection', $request['filter_last_connection'], array('size' => 6) ) !!}</th>
				<th>{!! Form::text('filter_last_profile', $request['filter_last_profile'], array('size' => 6) ) !!}</th>
				<th>{!! Form::text('filter_profile_count', $request['filter_profile_count'], array('size' => 6) ) !!}</th>
				<th>{!! Form::text('filter_profiles', $request['filter_profiles'], array('size' => 6) ) !!}</th>
				<th>{!! Form::text('filter_groups', $request['filter_groups'], array('size' => 6) ) !!}</th>
				<th colspan=4>
						{!! Form::Button("<i class='fa fa-search'></i>", array('onclick' => "submit()")) !!}
						{!! Form::Button("<i class='fa fa-times-circle'></i>", array('onclick' => "$('#filter_form').find('input').val('') ; submit() ")) !!}
					</th>
			</tr>



		</thead>

		@foreach( $hosts as $host )

		<tr>
			<td><input type="checkbox" class="checkline" name="{{$host->id}}" id="{{$host->id}}"></td>

			<td> <a href={{ URL::route('hosts.show', array('id'=>$host->id) ) }}>{{$host->name}}</a> </td>
			<td> {{ $host->os }} </td>
			<td> {{ $host->version }} </td>
			<td> {{ $host->last_connection }} </td>
			<td> {{ $host->last_profile_name }} </td>

			<td> <span class="badge">{{ count($host->profiles) }}</span> </td>
			<td> {{ $host->profiles->implode('name', ', ') }} </td>
			<td> {{ $host->hostgroups->implode('name', ', ') }} </td>

			<td class="line-button"><a href={{ URL::route('hosts.show', array('id'=>$host->id) ) }} class='btn' title="Information de {{$host->name}}" data-title="Information de {{$host->name}}"><i class="fa fa-info-circle"></i></a></td>
			<td class="line-button"><a href={{ URL::route('hosts.edit', array('id'=>$host->id) ) }} class='btn' title='Modifier {{$host->name}}' data-title='Modifier {{$host->name}}'><i class="fa fa-pencil"></i></a></td>
			<td class="line-button"><a href={{ URL::route('hosts.delete', array('id'=>$host->id) ) }} class='modalbox btn' title='Supprimer {{$host->name}}' data-title='Supprimer {{$host->name}}'><i class="fa fa-trash"></i></a> </td>

		</tr>

		@endforeach
	</table>


	<div class="flex-item item2">
		{{ $hosts->appends(compact('request'))->links('pagination::bootstrap-4') }}
	</div>

</div>



<script language="JavaScript" src="{{ URL::asset('/js/all.js') }}"></script>
<script>
    $(function () {

        $('.filters input').keypress(function (e) {
            if (e.which == 13) {
                $('#filter_form').submit();
                return false;
            }
        });


        $("#submit_mass_config").click(function () {

            event.preventDefault();
            // selected ids serialization
            let ids = $(".checkline:checked").map(function (index) {
                return this.id;
            }).get().join();

            $(".selection_ids").val(ids);

            $("#form_mass_config").submit();

        });


        $("#submit_mass_groups").click(function () {

            event.preventDefault();
            // selected ids serialization
            let ids = $(".checkline:checked").map(function (index) {
                return this.id;
            }).get().join();

            $(".selection_ids").val(ids);

            $("#form_mass_groups").submit();

        });

        $("#submit_mass_profiles").click(function () {

            event.preventDefault();
            // selected ids serialization
            let ids = $(".checkline:checked").map(function (index) {
                return this.id;
            }).get().join();

            $(".selection_ids").val(ids);

            $("#form_mass_profiles").submit();

        });




    });
</script>

@stop