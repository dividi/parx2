{{-- 
    To get this file : 
    php artisan vendor:publish --tag=laravel-pagination
--}}


<nav style="display:fixed; bottom:0; right:0;">
        <ul class="pagination">

@if ($paginator->hasPages())
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <span class="page-link" aria-hidden="true">&lsaquo;</span>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active" aria-current="page"><span class="page-link">{{ $page }}</span></li>
                        @else
                            <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="page-item">
                    <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
                </li>
            @else
                <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <span class="page-link" aria-hidden="true">&rsaquo;</span>
                </li>
                @endif
                
          
@endif

<li class="page-item">           
                    &nbsp;
                    <form class="form-inline" method='GET' action="{{ url()->current() }}">
                        <select class="form-control" name="perPage" onchange="submit()" style="float:right; width:100px;"  >
                            <option name="25" @if ($paginator->perPage() == "25") selected @endif > 25 </option>
                            <option name="50" @if ($paginator->perPage() == "50") selected @endif > 50 </option>
                            <option name="100" @if ($paginator->perPage() == "100") selected @endif > 100 </option>
                            <option name="200" @if ($paginator->perPage() == "200") selected @endif > 200 </option>
                            <option name="500" @if ($paginator->perPage() == "500") selected @endif > 500 </option>
                            <option name="1000" @if ($paginator->perPage() == "1000") selected @endif > 1000 </option>
                            <option name="10000" @if ($paginator->perPage() == "10000") selected @endif > 10000 </option>
                        </select>
                    </form>
                </li>
            </ul>
    </nav>