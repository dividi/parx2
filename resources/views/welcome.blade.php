
@extends('layout')


@section('navtitle') ACCUEIL @stop

@section('help') Hello. @stop


@section('buttons')

@stop


@section('body')

    
<h2>Stats</h2>
<table class="nice">
    <tr><td>Profils</td><td>{{$profiles_count}}</td></tr>
    <tr><td>Hôtes</td><td>{{$hosts_count}}</td></tr>
    <tr><td>Utilisateurs</td><td>{{$users_count}}</td></tr>
    <tr><td>Groupes d'hôtes</td><td>{{$hostgroups_count}}</td></tr>
    <tr><td>Groupes d'utilisateurs</td><td>{{$usergroups_count}}</td></tr>
    <tr><td>Utilisateurs sans profil</td><td>{{$users_without_profile_count}}</td></tr>
    <tr><td>Hôtes sans profil</td><td>{{$hosts_without_profile_count}}</td></tr>
    <tr><td>Imprimantes</td><td>{{$printers_count}}</td></tr>
    <tr><td>Partages</td><td>{{$shares_count}}</td></tr>
    <tr><td>Tâches actives</td><td>{{$active_tasks_count}}</td></tr>
</table>


    <script>var pattern=["ArrowUp","ArrowUp","ArrowDown","ArrowDown","ArrowLeft","ArrowRight","ArrowLeft","ArrowRight","b","a"],current=0,keyHandler=function(r){if(0>pattern.indexOf(r.key)||r.key!==pattern[current]){current=0;return}current++,pattern.length===current&&(current=0,window.alert("Dividi was here"))};document.addEventListener("keydown",keyHandler,!1);</script>

@stop
