<center>
	{!! Form::open(array('method'=>'DELETE', 'route'=>['shortcuts.destroy', $profile->id, $shortcut->id] )) !!}

		<div>

			Voulez vous supprimer le raccourci <b> {{ $shortcut->name}} </b> du profil <b> {{ $profile->name }} </b>  ?<br>
			
			<span class="small-font"></span>


		</div>
		
		
		<br />
			
		{!! Form::submit('Supprimer', ['class'=>'btn btn-danger']) !!}
		<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>

	{!! Form::close() !!}

	</center>


