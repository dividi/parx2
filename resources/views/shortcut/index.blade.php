
@extends('layout')


@section('navtitle') RACCOURCIS de {{ $profile->name }}  <li class="title-info"> {{ count($shortcuts) }} </li> @stop

@section('help') Les raccourcis sont des icones cliquables à ajouter au bureau Utilisateur ou Public. @stop


@section('buttons')
	<li> <a href={{ URL::route('shortcuts.assignShortcuts', array('id'=>$profile->id) ) }} class='' title='Associer des raccourcis'><i class="fa fa-link"></i> Associer</a> </li>
	<li> <a href={{ URL::route('shortcuts.create', array('id'=>$profile->id) ) }} class='' title='Ajouter un raccourci'><i class="fa fa-plus-circle"></i> Ajouter</a> </li>
@stop



@section('body')

	<div class="table-responsive col-md-12">

	{!! Form::open( ['url' => route('shortcuts.index', array('id'=>$profile->id)), 'method' => 'get', 'id' => 'filter_form'] )!!}


		<table class="nice">
			<thead>
				<tr>
					<th>Nom</th>
					<th>Chemin</th>
					<th>Paramètres</th>
					<th>Démarrer dans</th>
					<th>Description</th>
					<th>Localisation</th>
					<th>Démarrage</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
				</tr>

				<tr class="filters">
					<th>{!! Form::text('filter_name', $request['filter_name'], array('size' => 6) ) !!}</th>				
					<th>{!! Form::text('filter_target', $request['filter_target'], array('size' => 6) ) !!}</th>				
					<th>{!! Form::text('filter_arguments', $request['filter_arguments'], array('size' => 6) ) !!}</th>				
					<th>{!! Form::text('filter_workingdir', $request['filter_workingdir'], array('size' => 6) ) !!}</th>				
					<th>{!! Form::text('filter_description', $request['filter_description'], array('size' => 6) ) !!}</th>				
					<th>
						{!! Form::select('filter_location', [
						''	=> '',
						'0' => 'Bureau Utilisateur',
						'1' => 'Bureau Public',
						'2' => 'Désactivé',
						'3' => 'Barre des tâches',

						], $request['filter_location'], array('id' => 'location')) !!}
					</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th width=80px>
						{!! Form::Button("<i class='fa fa-search'></i>", array('onclick' => "submit()")) !!}
						{!! Form::Button("<i class='fa fa-times-circle'></i>", array('onclick' => "$('#filter_form').find('input').val('') ; $('#location option:eq(0)').prop('selected', true) ; submit() ")) !!}
					</th>
				</tr>



			</thead>

			@foreach ($shortcuts as $shortcut)

				<tr>
					<td> {{ $shortcut->name }} </td>
					<td> {{ $shortcut->target }} </td>
					<td> {{ $shortcut->arguments }} </td>
					<td> {{ $shortcut->workingDir }} </td>
					<td> {{ $shortcut->description }} </td>
					<td align=center> 
						@if ($shortcut->location  == 2)
							Désactivé
						@elseif ($shortcut->location  == 0)
							Bureau Utilisateur
						@elseif ($shortcut->location  == 1)
							Bureau Public
						@elseif ($shortcut->location  == 3)
							Barre des tâches
						@endif
					</td>

					<td align=center> 
						@if ($shortcut->startup  == 1)
							<i class="fa fa-check-circle-o" aria-hidden="true"></i>
						@endif
					</td>


					<td class="line-button"><a href={{ URL::route('shortcuts.edit', array('id'=>$profile->id, 'shortcut_id'=>$shortcut->id) ) }} class='btn' title="Modifier {{$shortcut->name}}"><i class="fa fa-pencil"></i></a> </td>
					<td class="line-button"><a href={{ URL::route('shortcuts.delete', array('modal'=>'true', 'id'=>$profile->id, 'shortcut_id'=>$shortcut->id) ) }} class='modalbox btn' title="Supprimer {{$shortcut->name}}"><i class="fa fa-trash"></i></a> </td>

				</tr>

			@endforeach
		</table>

		<div class="flex-item item2">
			{{ $shortcuts->appends(compact('request'))->links('pagination::bootstrap-4') }}
		</div>


	</div>



	<script language="JavaScript" src="{{ URL::asset('/js/all.js') }}"></script>
	<script>
    $(function () {

        $('.filters input').keypress(function (e) {
            if (e.which == 13) {
                $('#filter_form').submit();
                return false;
            }
        });

    });
</script>


@stop