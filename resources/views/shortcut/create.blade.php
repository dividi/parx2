@extends('layout')


@section('navtitle') Création d'un raccourci @stop

@section('help') Création d'un raccourci @stop


@section('body')


<div class="form-container">

    {!! Form::open(array('method'=>'POST', 'route'=>['shortcuts.store', $profile->id] )) !!}


    <table class="form-table">
        <tr>
            <td>{!! Form::label('name', 'Nom') !!}</td>
            <td>{!! Form::text('name', null, array('required' => '')) !!}</td>
        </tr>


        <tr>
            <td>{!! Form::label('target', 'Chemin') !!}</td>
            <td>{!! Form::textarea('target', null, ['size' => '25x3', 'required'=>'']) !!}</td>
        </tr>


        <tr>
            <td>{!! Form::label('arguments', 'Paramètres') !!}</td>
            <td>{!! Form::textarea('arguments', null, ['size' => '25x3']) !!}</td>
        </tr>


        <tr>
            <td>{!! Form::label('workingDir', 'Démarrer dans') !!}</td>
            <td>{!! Form::text('workingDir') !!}</td>
        </tr>

        <tr>
            <td>{!! Form::label('description', 'Description') !!}</td>
            <td>{!! Form::text('description') !!}</td>
        </tr>


        <tr valign="top">
            <td>{!! Form::label('location', 'Position') !!}</td>
            <td>{!! Form::select('location', array('0' => 'Bureau Utilisateur', '1' => 'Bureau Public', '2' => 'Désactivé', '3' => 'Barre des tâches'), null, array("class" => "chosen")) !!}</td>
        </tr>

        <tr title="Lance le raccouri au démarrage de la session">
            <td>{!! Form::label('startup', 'Au démarrage') !!}</td>
            <td>{!! Form::checkbox('startup') !!}</td>
        </tr>

    </table>


    <div class="cardbox">
        {!! Form::submit('Ajouter', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>

    {!! Form::close() !!}

</div>


<script>
    $(function() {
        $("input:first").focus();
        $(".chosen").chosen();
    });

</script>


@stop
