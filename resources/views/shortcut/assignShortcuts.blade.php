{{-- This page is no more useful, users should use the shortcut management page --}}

@extends('layout')


@section('navtitle') Association d'un raccourci @stop

@section('help') Association d'un raccourci au profil courant@stop


@section('body')


<div class="form-container">

    {!! Form::open(array('method'=>'POST', 'route'=>['shortcuts.updateShortcuts', $profile->id] )) !!}


    <table class="form-table">
        <tr valign="top" title="Le raccourci à ajouter à ce profil">
            <td>{!! Form::label('shortcuts', 'Raccourcis') !!}</td>
            <td>{!! Form::select('shortcuts[]', $shortcuts, null, array('multiple' => true, "class" => "chosen")) !!}</td>
        </tr>

        <tr valign="top">
            <td>{!! Form::label('location', 'Position') !!}</td>
            <td>{!! Form::select('location', array('0' => 'Bureau Utilisateur', '1' => 'Bureau Public', '2' => 'Désactivé', '3' => 'Barre des tâches'), null, array("class" => "chosen")) !!}</td>
        </tr>

    </table>


    <div class="cardbox">
        {!! Form::submit('Associer', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>

    {!! Form::close() !!}

</div>


<script>
    $(function() {
        $("input:first").focus();
        $(".chosen").chosen();
    });

</script>


@stop
