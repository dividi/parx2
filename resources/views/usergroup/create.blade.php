@extends('layout')


@section('navtitle') Création d'un groupe d'utilisateurs @stop

@section('help') Création d'un groupe d'utilisateurs @stop


@section('body')

<div class="form-container">

    {!! Form::open(array('method'=>'POST', 'route' => 'usergroups.create')) !!}

    <table class="form-table">
        <tr>
            <td><label for="name">Nom</label></td>
            <td><input type="text" name="name" required></td>
        </tr>

    </table>

    <div class="cardbox">
        {!! Form::submit('Ajouter', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>


    {!! Form::close() !!}

</div>

<script>
    $(function() {
        $("input:first").focus();
    });

</script>

@stop
