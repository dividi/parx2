@extends('layout')


@section('navtitle') Modification des profils des utilisateurs du groupe @stop

@section('help') Modification des profils des utilisateurs du groupe @stop


@section('body')

<div class="form-container">

    {!! Form::open(array('method'=>'PATCH', 'route'=>['usergroups.updateProfiles', $usergroup->id] )) !!}

    <table class="form-table">

        <tr>
            <td>{!! Form::label('profiles', 'Profils') !!}</td>
            <td>{!! Form::select('profiles[]', $profiles, null, array('multiple' => true, "class" => "chosen")) !!}</td>
        </tr>

        <tr>
            <td>Vider la liste des profils</td>
            <td>{!! Form::checkbox("clearProfiles") !!}</td>
        </tr>

    </table>

    <div class="cardbox">
        {!! Form::submit('Modifier', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>

    {!! Form::close() !!}
</div>

<script>
    $(function() {
        $("input:first").focus();
        $(".chosen").chosen();
    });

</script>


@stop
