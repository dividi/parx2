<center>

	{!! Form::open(array('method'=>'DELETE', 'route'=>['usergroups.destroy', $usergroup->id] )) !!}

		<div>

			Vous allez supprimer le groupe d'utilisateurs {{ $usergroup->name }}?<br>
			
			<span class="small-font">
				Les utilisateurs du groupe ne seront pas supprimés.
			</span>


		</div>
		
		
		<br />

		{!! Form::submit('Supprimer', ['class'=>'btn btn-danger']) !!}
		<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>

	{!! Form::close() !!}


</center>
