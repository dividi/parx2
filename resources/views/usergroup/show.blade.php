@extends('layout')


@section('navtitle') GROUPE D'UTILISATEURS {{ $usergroup->name }} @stop

@section('help') La page complète du groupe d'utilisateurs. @stop


@section('body')


<a href={{ URL::route('usergroups.index') }} class='btn btn-primary'><i class="fa fa-caret-left"></i></i> RETOUR A LA LISTE</a>


<div class="page-title"> {{ $usergroup->name }} </div>

<div class="page-actions">
    <a href={{ URL::route('usergroups.editProfiles', array('id'=>$usergroup->id) ) }} class='btn btn-success' title="Ajouter des profils aux hôtes de {{$usergroup->name}}"><i class="fa fa-briefcase"></i> ASSOCIER DES PROFILS</a>
    <a href={{ URL::route('usergroups.edit', array('id'=>$usergroup->id) ) }} class='btn btn-primary' title="Modifier {{$usergroup->name}}"><i class="fa fa-pencil"></i> MODIFIER</a>
    <a href={{ URL::route('usergroups.delete', array('modal'=>'true', 'id'=>$usergroup->id) ) }} class='modalbox btn btn-danger' title="Supprimer {{$usergroup->name}}"><i class="fa fa-trash"></i> SUPPRIMER</a>
</div>



<div style="display:flex; flex-wrap:wrap;">


    <div class="panel panel-primary" style="max-width:200px; margin:15px;">
        <div class="panel-heading">
            <h3 class="panel-title">UTILISATEURS <span class="badge">{{ count($usergroup->users) }}</span></h3>
        </div>
        <div class="panel-body" style="overflow-y:auto; max-height:200px;">
            <ul>
                @forelse($usergroup->users as $user)
                <li>{{ $user->login }}</li>
                @empty
                <p>pas d'utilisateur associé</p>
                @endforelse
            </ul>
        </div>
    </div>



    <div class="panel panel-primary" style="max-width:200px; margin:15px;">
        <div class="panel-heading">
            <h3 class="panel-title">PROFILS DE GROUPE <span class="badge">{{ count($usergroup->profiles) }}</span></h3>
        </div>
        <div class="panel-body" style="overflow-y:auto; max-height:200px;">
            <ul>
                @forelse($usergroup->profiles as $profile)
                <li>{{ $profile->name }}</li>
                @empty
                <p>pas de profil de groupe associé</p>
                @endforelse
            </ul>
        </div>
    </div>


</div>

@stop
