
@extends('layout')


@section('navtitle') GROUPES d'UTILISATEURS <li id="selectedCount" class="title-info hidden-button hide"></li>
<li class="title-info hidden-button hide" title="sélectionnés">/</li>
<li class="title-info"> <span title="affichés">{{ count($usergroups) }} </span> / <span title="total">{{$total_count}}</li> @stop


@section('help') Les groupes d'utilisateurs permettent une gestion groupée des utilisateurs dans parx. @stop


@section('buttons')
	<li class="hidden-button hide"> <a href={{ URL::route('usergroups.massDelete') }} class='modalbox' title='Supprimer les groupes sélectionnés'><i class="fa fa-times-circle"></i> Suppression	</a> </li>

	<li> <a href="{{ URL::route('usergroups.create' ) }}" class='' title="Ajouter un groupe d'utilisateurs"><i class="fa fa-plus-circle"></i> Ajouter</a> </li>

@stop


@section('body')


{!! Form::open( ['url' => route('usergroups.index'), 'method' => 'get', 'id' => 'filter_form'] )!!}


	<div class="table-responsive">

		<table class="nice">
			<thead>
				<tr>
					<th><input type="checkbox" class="checkall"></th>
					<th>Nom</th>
					<th><i class="fa fa-user" title="Nombre d'utilisateurs associés"></i></th>
					<th>Profils de groupe</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th></th>
				</tr>

				<tr class="filters">
					<th></th>
					<th>{!! Form::text('filter_name', $request['filter_name'], array('size' => 6) ) !!}</th>
					<th>{!! Form::text('filter_user_count', $request['filter_user_count'], array('size' => 6) ) !!}</th>
					<th>{!! Form::text('filter_profile', $request['filter_profile'], array('size' => 6) ) !!}</th>

					<th</th>
					<th</th>
					<th</th>
					<th colspan=4>
						{!! Form::Button("<i class='fa fa-search'></i>", array('onclick' => "submit()")) !!}
						{!! Form::Button("<i class='fa fa-times-circle'></i>", array('onclick' => "$('#filter_form').find('input').val('') ; submit() ")) !!}
					</th>
				</tr>


			</thead>


			@foreach ($usergroups as $usergroup)

				<tr>
					<td class="line-button"><input type="checkbox" class="checkline" name={{ $usergroup->id }} id={{ $usergroup->id }}></td>
					<td> {{$usergroup->name}} </td>
					<td> <span class="badge">{{ count($usergroup->users) }} </span></td>
					<td> {{ $usergroup->profiles->implode('name', ', ') }} </td>
					<td class="line-button"><a href={{ URL::route('usergroups.show', array('id'=>$usergroup->id) ) }} class='btn' title="Information de {{$usergroup->name}}"><i class="fa fa-info-circle"></i></a></td>
					<td class="line-button"><a href={{ URL::route('usergroups.editProfiles', array('id'=>$usergroup->id) ) }} class='btn' title="Ajouter des profils aux utilisateurs de {{$usergroup->name}}"><i class="fa fa-briefcase"></i></a></td>
					<td class="line-button"><a href={{ URL::route('usergroups.edit', array('id'=>$usergroup->id) ) }} class='btn' title="Modifier {{$usergroup->name}}"><i class="fa fa-pencil"></i></a></td>
					<td class="line-button"><a href={{ URL::route('usergroups.delete', array('id'=>$usergroup->id) ) }} class='modalbox btn' title="Supprimer {{$usergroup->name}}"><i class="fa fa-trash"></i></a> </td>

				</tr>

			@endforeach

		</table>

		<div class="flex-item item2">
			{{ $usergroups->appends(compact('request'))->links('pagination::bootstrap-4') }}
		</div>


	</div>


	<script language="JavaScript" src="{{ URL::asset('/js/all.js') }}"></script>

	<script>
    $(function () {

        $('.filters input').keypress(function (e) {
            if (e.which == 13) {
                $('#filter_form').submit();
                return false;
            }
        });

    });
</script>


@stop
