@extends('layout')


@section('navtitle') Affection de imprimante {{ $printer->name }} @stop

@section('help') Affection de l'imprimante {{ $printer->name }} à une liste de profils @stop


@section('body')

<div class="form-container">

    <div class="cardbox">
        Sur quels profils voulez vous ASSOCIER l'imprimante <b>{{ $printer->name }}</b> ?<br />
        L'ajout ne se fera que si l'imprimante n'existe pas mais le paramètre PAR DEFAUT sera appliqué
    </div>

    {!! Form::open(array('method'=>'POST', 'route'=>['printers.affect', $printer->id] )) !!}

    <table class="form-table">

        <tr>
            <td>{!! Form::label('profiles', 'Profils') !!}</td>
            <td>{!! Form::select('all_profiles[]', $all_profiles, null, array('multiple' => true, "class" => "chosen")) !!}</td>
        </tr>

        <tr>
            <td>{!! Form::label('default', 'Par défaut') !!}</td>
            <td>{!! Form::checkbox('default') !!}</td>
        </tr>

    </table>

    <div class="cardbox">
        {!! Form::submit('Associer', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>


    {!! Form::close() !!}

</div>

<script>
    $(function() {
        $(".chosen").chosen();
        $("input:first").focus();
    });

</script>

@stop
