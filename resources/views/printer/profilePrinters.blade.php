{{-- List of assigned shares to a profile --}}



@extends('layout')


@section('navtitle') IMPRIMANTES de {{ $profile->name }} <li class="title-info"> {{ count($profile->printers) }} </li> @stop

@section('help') Les imprimantes disponibles sur le profile {{ $profile->name}} . @stop


@section('buttons')
	<li> <a href={{ URL::route('profiles.assignPrinter', array('id'=>$profile->id) ) }} class='btn' title='Régler les imprimantes de {{$profile->name}}'><i class="fa fa-link"></i> Associer</a> </li>
@stop


@section('body')

	<div class="table-responsive">

		<table class="nice">
			<thead>
				<tr>
				<th>Nom</th>
				<th>Chemin</th>
				<th>Défaut</th>
				<th>&nbsp;</th>
				</tr>
			</thead>


			@foreach ($profile->printers as $printer)
			
				<tr>
					<td> {{$printer->name}} </td>
					<td> {{$printer->target}} </td>
					<td align=center> 
						@if ( $printer->pivot->default == 1 )
							<i class="fa fa-check"></i>
						@else 
							<a href={{ URL::route('profiles.defaultPrinter', array('id'=>$profile->id, 'printer_id'=>$printer->id) ) }} class='btn' title="Retirer l'imprimante {{$printer->name}}"><i class="fa fa-check" style="color:#C2C2C2;"></i></a>
						@endif
					</td>
					
					<td class="line-button"><a href={{ URL::route('profiles.unassignPrinter', array('id'=>$profile->id, 'printer_id'=>$printer->id) ) }} class='modalbox btn' title="Retirer l'imprimante {{$printer->name}}"><i class="fa fa-chain-broken"></i></a> </td>
				
				</tr>

			@endforeach

		</table>
	</div>

@stop