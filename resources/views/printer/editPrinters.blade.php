{{-- Affect printers to profile --}}


	<center>
		<br />      
			
		{!! Form::open(array('method'=>'PATCH', 'route'=>['profiles.updatePrinters', $profile->id] )) !!}

			<table style="vertical-align:top;">

				<tr class="chosen" valign="top">
					<td>{!! Form::label('profiles', 'Imprimantes') !!}</td>
					<td>{!! Form::select('printers[]', $printers, null, array('multiple' => true)) !!}</td>
				</tr>

			</table>

			<br />
			
			{!! Form::submit('Assigner', ['class'=>'btn btn-primary']) !!}
			<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>

		{!! Form::close() !!}

	</center>