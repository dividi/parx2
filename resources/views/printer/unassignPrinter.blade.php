<center>
	{!! Form::open(array('method'=>'DELETE', 'route'=>['profiles.destroyPrinter', $profile->id, $printer->id] )) !!}

		<div>

			Voulez vous supprimer l'imprimante {{ $printer->name }} du profil {{ $profile->name }}?<br>
			
			<span class="small-font">
				Les imprimantes associées ne seront pas supprimés.
			</span>


		</div>
		
		
		<br />
			
		{!! Form::submit('Retirer', ['class'=>'btn btn-danger']) !!}
		<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>

	{!! Form::close() !!}

	</center>


