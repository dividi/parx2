@extends('layout')


@section('navtitle') Modification d'une imprimante @stop

@section('help') Modification d'une imprimante @stop


@section('body')

<div class="form-container">

    {!! Form::model($printer, ['method'=>'PATCH', 'route'=>['printers.update', $printer->id]]) !!}

    <table class="form-table">
        <tr>
            <td>{!! Form::label('name', 'Nom') !!}</td>
            <td>{!! Form::text('name', null, array('required' => '')) !!}</td>
        </tr>
        <tr>
            <td>{!! Form::label('target', 'Chemin') !!}</td>
            <td>{!! Form::textarea('target', null, ['size' => '25x3', 'required'=>'']) !!}</td>
        </tr>

    </table>

    <div class="cardbox">
        {!! Form::submit('Modifier', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>


    {!! Form::close() !!}

</div>

<script>
    $(function() {
        $("input:first").focus();
    });

</script>

@stop
