@extends('layout')


@section('navtitle') IMPRIMANTES 
	<li class="title-info"> <span title="affichés">{{ count($printers) }} </span> / <span title="total">{{$total_count}}</li> @stop


@section('help') gestion des imprimantes disponibles sur le parc. @stop


@section('buttons')
<li> <a href={{ URL::route('printers.create' ) }} class='' title="Ajouter une imprimante"><i class="fa fa-plus-circle"></i> Ajouter</a> </li>
@stop


@section('body')

<div class="table-responsive">

{!! Form::open( ['url' => route('printers.index'), 'method' => 'get', 'id' => 'filter_form'] )!!}

	<table class="nice">
		<thead>
			<tr>
				<th>Nom</th>
				<th>Chemin</th>
				<th>Profils</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
			</tr>

			<tr class="filters">
				<th>{!! Form::text('filter_name', $request['filter_name'], array('size' => 6) ) !!}</th>				
				<th>{!! Form::text('filter_target', $request['filter_target'], array('size' => 6) ) !!}</th>				
				<th>{!! Form::text('filter_profile', $request['filter_profile'], array('size' => 6) ) !!}</th>				
				
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th width=80px>
					{!! Form::Button("<i class='fa fa-search'></i>", array('onclick' => "submit()")) !!}
					{!! Form::Button("<i class='fa fa-times-circle'></i>", array('onclick' => "$('#filter_form').find('input').val('') ; submit() ")) !!}
				</th>
			</tr>

		</thead>


		@foreach ($printers as $printer)

		<tr>
			<td> {{$printer->name}} </td>
			<td> {{$printer->target}} </td>
			<td> {{$printer->profiles->implode('name', ', ') }} </td>

			<td class="line-button"><a href={{ URL::route('printers.affect', array('id'=>$printer->id) ) }} class='btn' title="Affecter {{$printer->name}} à des profils"><i class="fa fa-link"></i></a></td>
			<td class="line-button"><a href={{ URL::route('printers.edit', array('id'=>$printer->id) ) }} class='btn' title="Modifier {{$printer->name}}"><i class="fa fa-pencil"></i></a></td>
			<td class="line-button"><a href={{ URL::route('printers.delete', array('modal'=>'true', 'id'=>$printer->id) ) }} class='modalbox btn' title="Supprimer {{$printer->name}}"><i class="fa fa-trash"></i></a> </td>

		</tr>

		@endforeach

	</table>
	
	<div class="flex-item item2">
		{{ $printers->appends(compact('request'))->links('pagination::bootstrap-4') }}
	</div>

</div>


<script language="JavaScript" src="{{ URL::asset('/js/all.js') }}"></script>
<script>

	$(function(){

		$('.filters input').keypress(function (e) {
			if (e.which == 13) {
				$('#filter_form').submit();
				return false;
			}
		});

	});

</script>
@stop