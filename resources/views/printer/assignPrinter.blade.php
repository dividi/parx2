@extends('layout')


@section('navtitle') Assigner une imprimante au profil {{$profile->name}} @stop

@section('help') Assigner une imprimante au profil {{$profile->name}} @stop


@section('body')

<div class="form-container">  
			
		{!! Form::open(array('method'=>'PATCH', 'route'=>['profiles.updatePrinter', $profile->id] )) !!}

			<table class="form-table">

				<tr valign="top">
					<td>{!! Form::label('printer', 'Imprimante') !!}</td>
					<td>{!! Form::select('printer', $printers, null, array('required', "class" => "chosen")) !!}</td>
				</tr>

				<tr valign="top">
					<td>{!! Form::label('default', 'Par défaut') !!}</td>
					<td>{!! Form::checkbox('default') !!}</td>
				</tr>

			</table>


    <div class="cardbox">
        {!! Form::submit('Assiger', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>

    {!! Form::close() !!}
</div>

<script>
    $(function() {
        $("input:first").focus();
        $(".chosen").chosen();
    });

</script>


@stop