<center>
	<form method="POST" action={{ URL::route( 'printers.destroy', [$printer->id] )  }}>
		
		<input type="hidden" name="_method" value="DELETE">

		{!! csrf_field() !!}

		<div>

			Vous allez supprimer l'imprimante' {{ $printer->name }}?<br>

		</div>
		
		
		<br />

		<input type='submit' class='btn btn-danger' value='Supprimer'>
		<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>

	</form>
</center>
