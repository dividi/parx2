
@extends('layout')


@section('navtitle') GROUPE d'HÔTES
<li class="title-info"> <span title="affichés">{{ count($hostgroups) }} </span> / <span title="total">{{$total_count}}</li> @stop

@section('help') Les groupes d'hôtes permettent une gestion groupée des hôtes dans parx. @stop


@section('buttons')
	<li> <a href={{ URL::route('hostgroups.create' ) }} title="Ajouter un groupe d'hôte" data-title="Ajouter un groupe d'hôte"><i class="fa fa-plus-circle"></i> Ajouter</a> </li>
@stop


@section('body')

	{!! Form::open( ['url' => route('hostgroups.index'), 'method' => 'get', 'id' => 'filter_form'] )!!}

	<div class="table-responsive">

		<table class="nice">
			<thead>
				<tr>
					<th>Nom</th>
					<th><i class="fa fa-desktop" title="Nombre d'hôtes associés"></i></th>
					<th>Profils de groupe</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
				</tr>

				<tr class="filters">
					<th>{!! Form::text('filter_name', $request['filter_name'], array('size' => 6) ) !!}</th>
					<th>{!! Form::text('filter_host_count', $request['filter_host_count'], array('size' => 6) ) !!}</th>
					<th>{!! Form::text('filter_profile', $request['filter_profile'], array('size' => 6) ) !!}</th>

					<th</th>
					<th</th>
					<th</th>
					<th colspan=4>
						{!! Form::Button("<i class='fa fa-search'></i>", array('onclick' => "submit()")) !!}
						{!! Form::Button("<i class='fa fa-times-circle'></i>", array('onclick' => "$('#filter_form').find('input').val('') ; submit() ")) !!}
					</th>
				</tr>

			</thead>


			@foreach ($hostgroups as $hostgroup)
			
				<tr>
					<td> <a href={{ URL::route('hostgroups.show', array('id'=>$hostgroup->id) ) }} >{{$hostgroup->name}}</a> </td>
					<td> <span class="badge">{{ count($hostgroup->hosts) }} </span></td>
					<td> {{ $hostgroup->profiles->implode('name', ', ') }} </td>
					<td class="line-button"><a href={{ URL::route('hostgroups.show', array('modal'=>'true', 'id'=>$hostgroup->id) ) }} class='btn' title="Information de {{$hostgroup->name}}"><i class="fa fa-info-circle"></i></a></td>
										
					<td class="line-button"><a href={{ URL::route('hostgroups.editProfiles', array('id'=>$hostgroup->id) ) }} class='btn' title="Ajouter des profils aux hôtes de {{$hostgroup->name}}"><i class="fa fa-briefcase"></i></a></td>
					<td class="line-button"><a href={{ URL::route('hostgroups.edit', array('id'=>$hostgroup->id) ) }} class='btn' title="Modifier {{$hostgroup->name}}"><i class="fa fa-pencil"></i></a></td>
					<td class="line-button"><a href={{ URL::route('hostgroups.delete', array('id'=>$hostgroup->id) ) }} class='modalbox btn' title="Supprimer {{$hostgroup->name}}"><i class="fa fa-trash"></i></a> </td>
				
				</tr>

			@endforeach

		</table>
	
		<div class="flex-item item2">
			{{ $hostgroups->appends(compact('request'))->links('pagination::bootstrap-4') }}
		</div>
	
	</div>


	<script language="JavaScript" src="{{ URL::asset('/js/all.js') }}"></script>

	<script>

		$(function(){

			$('.filters input').keypress(function (e) {
				if (e.which == 13) {
					$('#filter_form').submit();
					return false;
				}
  			});
		
		});
	
	</script>

@stop