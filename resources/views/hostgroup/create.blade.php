@extends('layout')


@section('navtitle') Création d'un groupe d'hôtes @stop

@section('help') Création d'un nouveau groupe pour les machines @stop


@section('body')

<div class="form-container">

		<form action="{{ route('hostgroups.store') }}" method="post" accept-charset="utf-8">

			{!! csrf_field() !!}

			<table>
				<tr>
					<td><label for="name">Nom</label></td>
					<td><input type="text" name="name" required></td>
				</tr>
	
			</table>

			<div class="cardbox">
            	<button type="submit" class="btn btn-primary">Créer</button>
            	<button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
        	</div>

		</form>

	<script>
		$("input:first").focus();
	</script>

@stop