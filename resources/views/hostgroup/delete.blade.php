<center>
	<form method="POST" action={{ URL::route( 'hostgroups.destroy', [$hostgroup->id] )  }}>
		
		<input type="hidden" name="_method" value="DELETE">

		{!! csrf_field() !!}

		<div>

			Vous allez supprimer le groupe d'hôte <b>{{ $hostgroup->name }}</b> ?<br>


			<br />
		
			{!! Form::checkbox("deleteHosts") !!} Supprimer aussi les hôtes du groupe

			<span class="small-font"></span>


		</div>
		
		<br />
		<br />

		<input type='submit' class='btn btn-danger' value='Supprimer'>
		<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>

	</form>
</center>
