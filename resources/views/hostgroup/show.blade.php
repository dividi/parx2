@extends('layout')


@section('navtitle') GROUPE D'HÔTES {{ $hostgroup->name }} @stop

@section('help') La page complète du groupe d'hôtes. @stop


@section('body')


<a href={{ URL::route('hostgroups.index') }} class='btn btn-primary'><i class="fa fa-caret-left"></i></i> RETOUR A LA LISTE</a>


<div class="page-title"> {{ $hostgroup->name }} </div>

<div class="page-actions">
    <a href={{ URL::route('hostgroups.editProfiles', array('id'=>$hostgroup->id) ) }} class='btn btn-success' title="Ajouter des profils aux hôtes de {{$hostgroup->name}}"><i class="fa fa-briefcase"></i> ASSOCIER DES PROFILS</a>
    <a href={{ URL::route('hostgroups.edit', array('id'=>$hostgroup->id) ) }} class='btn btn-primary' title="Modifier {{$hostgroup->name}}"><i class="fa fa-pencil"></i> MODIFIER</a>
    <a href={{ URL::route('hostgroups.delete', array('modal'=>'true', 'id'=>$hostgroup->id) ) }} class='modalbox btn btn-danger' title="Supprimer {{$hostgroup->name}}"><i class="fa fa-trash"></i> SUPPRIMER</a>
</div>

<br>


<div style="display:flex; flex-wrap:wrap;">


    <div class="panel panel-primary" style="max-width:200px; margin:15px;">
        <div class="panel-heading">
            <h3 class="panel-title">HÔTES <span class="badge">{{ count($hostgroup->hosts) }}</span></h3>
        </div>
        <div class="panel-body" style="overflow-y:auto; max-height:200px;">
            <ul>
                @forelse($hostgroup->hosts as $host)
                <li>{{ $host->name }}</li>
                @empty
                <p>pas d'hôte associé</p>
                @endforelse
            </ul>
        </div>
    </div>


    <div class="panel panel-primary" style="max-width:200px; margin:15px;">
        <div class="panel-heading">
            <h3 class="panel-title">PROFILS DE GROUPE <span class="badge">{{ count($hostgroup->profiles) }}</span></h3>
        </div>
        <div class="panel-body" style="overflow-y:auto; max-height:200px;">
            <ul>
                @forelse($hostgroup->profiles as $profile)
                <li>{{ $profile->name }}</li>
                @empty
                <p>pas de profil de groupe associé</p>
                @endforelse
            </ul>
        </div>
    </div>

</div>

@stop
