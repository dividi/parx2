@extends('layout')


@section('navtitle') Modification d'un groupe d'hôtes @stop

@section('help') Modification d'un groupe d'hôtes. @stop


@section('body')


<div class="form-container">

    <div class="cardbox">
        Un profil de groupe est un profil attaché au groupe pas aux hôtes du groupe.<br />
        Il suffit qu'un hôte soit dans un groupe pour que le profil de groupe soit appliqué.<br />
        Très pratique si on utilise l'import LDAP avec l'affectation automatique des hôtes.
    </div>

    {!! Form::model($hostgroup, ['method'=>'PATCH', 'route'=>['hostgroups.update', $hostgroup->id]]) !!}
    <table class="form-table">
        <tr>
            <td>{!! Form::label('name', 'Nom') !!}</td>
            <td>{!! Form::text('name', null, array('required' => '')) !!}</td>
        </tr>

        <tr valign="top">
            <td>{!! Form::label('profiles', 'Profils de groupe') !!}</td>
            <td>{!! Form::select('profiles[]', $profiles, $hostgroup->profiles()->pluck('id')->toArray(), array('multiple' => true, "class" => "chosen")) !!}</td>
        </tr>

    </table>

    <div class="cardbox">
        {!! Form::submit('Modifier', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>

    {!! Form::close() !!}
</div>

<script>
    $(function() {
        $("input:first").focus();
        $(".chosen").chosen();
    });

</script>

@stop
