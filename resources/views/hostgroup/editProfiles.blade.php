@extends('layout')


@section('navtitle') Modifier les profils des hôtes du groupe @stop

@section('help') Modifier les profils des hôtes du groupe @stop


@section('body')

<div class="form-container">
    {!! Form::open(array('method'=>'PATCH', 'route'=>['hostgroups.updateProfiles', $hostgroup->id] )) !!}



    <table class="form-table">

        <tr>
            <td>{!! Form::label('clearProfiles', 'Vider la liste des profils') !!} </td>
            <td> {!! Form::checkbox("clearProfiles") !!} </td>
        </tr>

        <tr valign="top">
            <td>{!! Form::label('profiles', 'Profils') !!}</td>
            <td>{!! Form::select('profiles[]', $profiles, null, array('multiple' => true, "class" => "chosen")) !!}</td>
        </tr>

    </table>


    <div class="cardbox">
        {!! Form::submit('Modifier', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>

    {!! Form::close() !!}
</div>

<script>
    $("input:first").focus();
    $(".chosen").chosen();

</script>

@stop
