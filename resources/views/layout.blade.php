<!DOCTYPE html>

<html>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('images/logo2.png') }}" type="image/x-icon">

    <title>Par:x</title>

    {{-- Styles --}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    {{-- Scripts --}}
    <script language="JavaScript" src="{{ URL::asset('/js/all.js') }}"></script>
    <script language="JavaScript" src="{{ URL::asset('/js/main.js') }}"></script>



</head>


<body>

    <div class="back-top"></div>

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="header">
        <div class="container-fluid">

            {{-- Left Part o' the page --}}
            <ul class="nav navbar-nav">
                <li><a class="navbar-brand" href="#" title="Parx que je le vaux bien..."></a></li>
                <li class="navbar-title">@yield('navtitle')</li>
            </ul>


            {{-- Right Part o' the page --}}
            <ul class="nav navbar-nav navbar-right">

                {{-- Help --}}
                <li>
                    <a href="#" id="help" data-container="body" data-toggle="popover popover-dismiss"
                        data-placement="bottom" data-content="@yield('help', 'Le bouton aide !')"><i
                            class="fa fa-info-circle"></i> Info </a></li>

                @yield('buttons')


            </ul>
        </div>
    </nav>

    <div class='spacer'></div>


    <div id="wrapper">

        {{-- Sidebar --}}
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li></li>
                {{-- My menu --}}
                <li><a href={{ URL::to('/') }}> <i class="fa fa-home"></i> ACCUEIL </a></li>
                <li><a href={{ URL::route('profiles.index') }}> <i class="fa fa-briefcase"></i>
                        PROFILS </a></li>
                <li><a href={{ URL::route('hosts.index') }}> <i class="fa fa-desktop"></i> HÔTES
                    </a></li>
                <li><a href={{ URL::route('hostgroups.index') }}> <i class="fa fa-desktop"></i>
                        GROUPES d'HÔTES </a></li>
                <li><a href={{ URL::route('tasks.index') }}> <i class="fa fa-list"></i> TÂCHES
                    </a></li>
                <li><a href={{ URL::route('tasks.active') }}> <i class="fa fa-list-ol"></i> TÂCHES
                        ACTIVES</a></li>
                <li><a href={{ URL::route('users.index') }}> <i class="fa fa-user"></i>
                        UTILISATEURS </a></li>
                <li><a href={{ URL::route('usergroups.index') }}> <i class="fa fa-users"></i>
                        GROUPES d'UTILISATEURS </a></li>
                <li><a href={{ URL::route('shortcutsMgt.index') }}> <i
                            class="fa fa-share-square"></i> RACCOURCIS </a></li>
                <li><a href={{ URL::route('printers.index') }}> <i class="fa fa-print"></i>
                        IMPRIMANTES </a></li>
                <li><a href={{ URL::route('shares.index') }}> <i class="fa fa-share-alt"></i>
                        PARTAGES </a></li>

                <li><a href={{ URL::route('auth.index') }}> <i class="fa fa-key"></i> COMPTES </a>
                </li>


                <li title="See you soon Space Cowboy..."><a href="{{ route('logout') }}"
                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i
                            class="fa fa-power-off"></i> DÉCONNEXION </a></li>

                {{-- <li style="display:flex;"> {{ auth()->user()->name }}
                </li> --}}

                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                    style="display: none;">
                    @csrf
                </form>

            </ul>
        </div>


        {{-- Page content wrapper --}}
        <div id="page-content-wrapper">
            {{-- My Content --}}
            @yield('body')

        </div>

    </div>


    {{-- Flashbag content --}}
    <div class="flashbag">
        @if(Session::has('message'))
            <div class="alert alert-info">
                <p>{!! Session::get('message') !!}</p>
            </div>
        @endif
        @if($errors->any())
            <div class='alert alert-danger'>
                @foreach( $errors->all() as $error )
                    <p>{!! $error !!}</p>
                @endforeach
            </div>
        @endif
    </div>


</body>


<script>
    const items = [
        "Parx je le vaux bien",
        "Si vis Parxem para bellum",
        "Si on remplace le a par un i, le r et le x par des z et qu'on rajoute un a ça fait Pizza",
        "Alone in the Parx",
        "A utiliser avec Parximonie"
    ];

    $("a.navbar-brand")[0].title = items[Math.floor(Math.random() * items.length)];

</script>


</html>
