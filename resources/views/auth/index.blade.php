@extends('layout')

@section('navtitle') COMPTES 
	<li class="title-info"> <span title="affichés">{{ count($logins) }} </span> / <span title="total">{{$total_count}}</li> @stop


@section('help') Page de visualisation des comptes autorisés à ouvrir une session @stop


@section('buttons')
	<li> <a href="{{ route('auth.create') }}" class='' title='Ajouter un compte'><i class="fa fa-plus-circle"></i> Ajouter</a> </li>
@stop

                   
@section('body')



	<div class="table-responsive">
	
		{!! Form::open( ['url' => route('auth.index'), 'method' => 'get', 'id' => 'filter_form'] )!!}
	
		<table class="nice">
			<thead>
				<tr>
					<th>Nom</th>
					<th>Email</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
				</tr>

				<tr class="filters">
					<th>{!! Form::text('filter_name', $request['filter_name'], array('size' => 6) ) !!}</th>				
					<th>{!! Form::text('filter_email', $request['filter_email'], array('size' => 6) ) !!}</th>				
					<th>&nbsp;</th>
					<th width=80px>
						{!! Form::Button("<i class='fa fa-search'></i>", array('onclick' => "submit()")) !!}
						{!! Form::Button("<i class='fa fa-times-circle'></i>", array('onclick' => "$('#filter_form').find('input').val('') ; submit() ")) !!}
					</th>
				</tr>
			</thead>

			@foreach ($logins as $login)

				<tr>
					<td> {{$login->name}} </td>
					<td> {{$login->email}} </td>
					<td class="line-button"><a href={{ URL::route('auth.delete', array('id'=>$login->id) ) }} class='modalbox btn' title='Supprimer {{$login->name}}'><i class="fa fa-trash"></i></a> </td>
{{-- 
					<td class="line-button"><a href={{ URL::route('profiles.edit', array('id'=>$profile->id) ) }} class='modalbox btn' title='Modifier {{$profile->name}}'><i class="fa fa-pencil"></i></a></td>

--}}
				</tr>

			@endforeach
		</table>	
		
		<div class="flex-item item2">
			{{ $logins->appends(compact('request'))->links('pagination::bootstrap-4') }}
		</div>



	</div>

	<script language="JavaScript" src="{{ URL::asset('/js/all.js') }}"></script>

	<script>

		$(function(){

			$('.filters input').keypress(function (e) {
				if (e.which == 13) {
					$('#filter_form').submit();
					return false;
				}
  			});
		
		});
	
	</script>



@stop