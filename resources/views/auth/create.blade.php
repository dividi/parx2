@extends('layout')


@section('navtitle') Création d'un nouveau compte @stop

@section('help') Création d'un nouveau compte pour se connecter au serveur. @stop


@section('body')
 

<div class="form-container">


    {!! Form::open(array('route' => 'auth.store')) !!}


        <div class="form-group row">
            <label for="name" class="col-lg-2 col-form-label text-md-left">Utilisateur</label>

            <div class="col-lg-3">
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>


        <div class="form-group row">
            <label for="email" class="col-lg-2 col-form-label text-md-left">Email</label>

            <div class="col-lg-3">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>


        <div class="form-group row">
            <label for="password" class="col-lg-2 col-form-label text-md-left">Mot de passe</label>

            <div class="col-lg-3">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>


        <div class="form-group row">
            <label for="password-confirm" class="col-lg-2 col-form-label text-sm-left">Confirmation mot de passe</label>

            <div class="col-lg-3">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
            </div>
        </div>


        <div class="form-group row">
            <div class="col-lg-3 text-sm-center">
                    {!! Form::submit('Ajouter le compte', ['class'=>'btn btn-primary']) !!}
                    <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
            </div>
        </div>

            {!! Form::close() !!}

    </div>

@stop
