@extends('layouts.app')

@section('content')


    <h2 class="text-center"> Reset mot de passe</h2>
    <br/>

            <div class="card">
  
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-6 col-form-label text-md-right">E-Mail</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0 text-center">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary"> Envoyer le lien de réinitialisation </button>
                                <a href={{ URL::to('/') }} class="btn btn-default"> Retour</a>
                            </div>

                        </div>

                    </form>
                </div>
            </div>

@endsection
