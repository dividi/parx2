@extends('layouts.app')

@section('content')

    <h2 class="text-center"> Authentification</h2>
    <br/>


    <div class="card">
    
        <div class="card-body">
            <form method="POST" action="{{ route('login') }}">
                @csrf


                <div class="form-group row">
                    <label for="email" class="col-sm-6 col-form-label text-md-left">{{ __('E-Mail Address') }}</label>
                    <div class="col-sm-12">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>




                <div class="form-group row">
                    <label for="password" class="col-sm-6 col-form-label text-md-left">{{ __('Password') }}</label>
                    <div class="col-sm-12">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div>


                <div class="form-group row">
                    <div class="form-check col-sm-6">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="form-check-label" for="remember"> {{ __('Remember Me') }} </label>
                    </div>

<!--                     <div class="col-sm-6">
                        @if (Route::has('password.request'))
                            <a class="btn-xs bt-link" href="{{ route('password.request') }}" title="shaaaaaaame !"> {{ __('Forgot Your Password ?') }} </a>
                        @endif
                    </div> -->

                </div>


                <div class="form-group row text-center">
                    <button type="submit" class="btn btn-primary"> {{ __('Login') }} </button>
                </div>


            </form>

        </div>

    </div>

@endsection
