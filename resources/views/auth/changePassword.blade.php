{{-- change password --}}

	<center>
		<br />

		{!! Form::open(array('method'=>'POST', 'route'=>['auth.updatePassword'] )) !!}
			<table>

				<tr>
					<td>{!! Form::label('oldPwd', 'Ancien MDP') !!}</td>
					<td>{!! Form::password('oldPwd', null, array('required' => '')) !!}</td>
				</tr>

				<tr>
					<td>{!! Form::label('newPwd', 'Nouveau MDP') !!}</td>
					<td>{!! Form::password('newPwd', null, array('required' => '', 'id' => 'newPwd')) !!}</td>
				</tr>

				<tr>
					<td>{!! Form::label('newPwd2', 'Confirmer MDP') !!}</td>
					<td>{!! Form::password('newPwd2', null, array('required' => '', 'id' => 'newPwd2')) !!} <span><i id="confirm" class="fa"></i></span> </td>
				</tr>
	
			</table>
	
			<br />
			
			{!! Form::submit('Modifier', ['class'=>'btn btn-primary disabled', 'id'=>'submit']) !!}

		{!! Form::close() !!}

	</center>

	<script>
		// give focus to first field
		$("input:first").focus();

		$(function () {

			$( "#newPwd2" ).keyup(function() {
  				
  				if ( $("#newPwd").val() != $(this).val() ) {
  					$("#confirm").removeClass("fa-check-circle");
  					$("#confirm").addClass("fa-times-circle");
  					$("#confirm").css("color", "red");
  					$("#submit").addClass("disabled");
  				}
  				else {
  					$("#confirm").addClass("fa-check-circle");
  					$("#confirm").removeClass("fa-times-circle");
  					$("#confirm").css("color", "green");

  					$("#submit").removeClass("disabled");
  				}

			});

		});



	</script>
