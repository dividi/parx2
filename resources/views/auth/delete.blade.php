<center>
	<form method="POST" action={{ URL::route( 'auth.destroy', [$login->id] )  }}>
		
		<input type="hidden" name="_method" value="DELETE">

		{!! csrf_field() !!}

		<div>
		
			@if ( Auth::user()->email === $login->email )
					Pour des tas de raisons, vous ne pouvez pas supprimer votre propre compte !
					<br />
					<br />
			@else
				Vous allez supprimer le login {{ $login->name }} ( {{ $login->email }} ) ?
				<br />
				<br />
				<input type='submit' class='btn btn-danger' value='Supprimer'>
			@endif
			

		

		<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
		</div>
	</form>
</center>

