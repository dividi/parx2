@extends('layout')


@section('navtitle') Modification d'une tâche @stop

@section('help') Modification d'une tâche @stop


@section('body')


<div class="form-container">

	<div class="cardbox">
		Le fichier Package doit être au format ZIP. <br />
		Il doit contenir au moins un fichier de script nommé <b>init.ps1</b>
	</div>


	{!! Form::open(array('route' => 'storePackage','method'=>'POST', 'files'=>true)) !!}

	<table class="form-table">
		<tr>
			<td>{!! Form::label('name', 'Nom') !!}</td>
			<td>{!! Form::text('name') !!}</td>
		</tr>
		<tr>
			<td>{!! Form::label('package', 'Package') !!}</td>
			<td>{!! Form::file('package') !!}</td>
		</tr>
	</table>


	<div class="cardbox">
		{!! Form::submit('Envoyer', array('class'=>'btn btn-primary')) !!}
		<button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
	</div>


	{!! Form::close() !!}
</div>
@stop
