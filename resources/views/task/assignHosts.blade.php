@extends('layout')


@section('navtitle') Ajouter une tâche à des hôtes @stop

@section('help') Ajouter une tâche à des hôtes @stop


@section('body')

<div class="form-container">

    {!! Form::open(array('method'=>'POST', 'route'=>['tasks.updateHosts'] )) !!}

    <table class="form-table" style="vertical-align:top;">

        <tr valign="top">
            <td>{!! Form::label('hosts', 'Hôte') !!}</td>
            <td>{!! Form::select('hosts[]', $hosts, null, array('multiple' => true, "class" => "chosen")) !!}</td>
            <td><a href="#" data-toggle="popover" data-content="La liste des hôtes sur lesquels exécuter la tâche." class="help"><i class="fa fa-question-circle"></i></a></td>
        </tr>

        <tr valign="top">
            <td>{!! Form::label('task', 'Tâche') !!}</td>
            <td>{!! Form::select('task', $tasks, null, array('id' => "task", "class" => "chosen")) !!}</td>
            <td><a href="#" data-toggle="popover" data-content="La tâche à exécuter sur les hôtes selectionnés." class="help"><i class="fa fa-question-circle"></i></a></td>
        </tr>

        <tr class="hide" valign="top">
            <td>{!! Form::label('recurtion', 'Récurrence') !!}</td>
            <td>{!! Form::select('recurtion', array('0' => 'Une fois', '1' => 'En boucle'), null, array("class" => "chosen")) !!}</td>
            <td><a href="#" data-toggle="popover" data-content="<b>Une fois :</b> La tâche sera supprimée après lancement.<br> <b>En boucle :</b> La tâche sera lancée à chaque rafraichissement du client" class="help"><i class="fa fa-question-circle"></i></a></td>
        </tr>

        <tr class="scheduledType scheduledDisplay">
            <td><b>Déclencheur</b></td>
            <td>
                <select id="schTypeSelect" class="chosen">
                    <option value="">aucun</option>
                    <option value="once">Une fois</option>
                    <option value="weekly">Hebdomadaire</option>
                    <option value="atLogon">Démarrage Session</option>
                    <option value="atStartup">Démarrage PC</option>
                </select>
            </td>
            <td><a href="#" data-toggle="popover" data-content="Quand déclencher la tâche. <br><b>Attention</b> Cette option n'est valable que pour les tâches lancées asAdmin" class="help"><i class="fa fa-question-circle"></i></a></td>
        </tr>

        <tr class="scheduledDay scheduledDisplay hide">
            <td><b>Jour</b></td>
            <td><input type="date" id="date"></td>
            <td><a href="#" data-toggle="popover" data-content="Jour de déclenchement de la tâche" class="help"><i class="fa fa-question-circle"></i></a></td>
        </tr>

        <tr class="scheduledDays scheduledDisplay hide">
            <td><b>Jours</b></td>
            <td>
                <input type="checkbox" id="Monday" class="days">Lundi<br>
                <input type="checkbox" id="Tuesday" class="days">Mardi<br>
                <input type="checkbox" id="Wednesday" class="days">Mercredi<br>
                <input type="checkbox" id="Thursday" class="days">Jeudi<br>
                <input type="checkbox" id="Friday" class="days">Vendredi<br>
                <input type="checkbox" id="Saturday" class="days">Samedi<br>
                <input type="checkbox" id="Sunday" class="days">Dimanche
            </td>
            <td><a href="#" data-toggle="popover" data-content="Jour de déclenchement de la tâche" class="help"><i class="fa fa-question-circle"></i></a></td>
        </tr>

        <tr class="scheduledAt scheduledDisplay hide">
            <td><b>Heure</b></td>
            <td><input id="time" type='time' value="00:00"></td>
            <td><a href="#" data-toggle="popover" data-content="Heure de déclenchement de la tâche" class="help"><i class="fa fa-question-circle"></i></a></td>
        </tr>


        <tr class="hide">
            <td>{!! Form::label('schParameter', 'Planification') !!}</td>
            <td>{!! Form::checkbox('schParameter') !!}</td>
        </tr>

    </table>

        {{-- schedule parameters --}}
        <input type='hidden' id='form_schParameter' name='schedule'>

    <div class="cardbox">
        {!! Form::submit('Assigner', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>
    
	{!! Form::close() !!}

</div>


<script>
    $(function() {

        $(".chosen").chosen();


        var $trigger = "";
        var $days = "";
        var $time = "";


        // POPO veur
        $('[data-toggle="popover"]').popover({
            placement: 'right'
            , html: true
            , trigger: "hover"
            , template: '<div class="popover help-popover"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>'
        , });



        // Task "Executer" can't provide scheduled task without asAdmin
        $("#form_asAdmin").on("change", function() {

            if ($(this).is(":checked")) {
                $(".scheduledType").removeClass("hide");
            } else {

                $("#schTypeSelect").val("");
                $trigger = "";
                $(".scheduledDisplay").addClass("hide");
            }

        });



        // change task type
        $("#task").on("change", function() {

            $option = $("#task option:selected").text();

            $(".classRecurtion").addClass("hide");
            $(".classAsadmin").addClass("hide");
            $(".classParameter").addClass("hide");

            if ($option == "update") {
                $(".scheduledType").addClass("hide");
                $(".scheduledDay").addClass("hide");
                $(".scheduledDays").addClass("hide");
                $(".scheduledAt").addClass("hide");
            } else {
                $(".scheduledType").removeClass("hide");
            }

        });




        // change scheduled type
        $("#schTypeSelect").on("change", function() {


            if ($(this).val() == "once") {
                $(".scheduledDays").addClass("hide");
                $(".scheduledDay").removeClass("hide");
                $(".scheduledAt").removeClass("hide");

                $trigger = "-Once";
            }



            if ($(this).val() == "weekly") {
                $(".scheduledDay").addClass("hide");
                $(".scheduledDays").removeClass("hide");
                $(".scheduledAt").removeClass("hide");

                $trigger = "-Weekly";

            }



            if ($(this).val() == "atLogon") {
                $(".scheduledDay").addClass("hide");
                $(".scheduledDays").addClass("hide");
                $(".scheduledAt").addClass("hide");

                $trigger = "-atLogon";
                $days = "";
                $time = "";
            }




            if ($(this).val() == "atStartup") {
                $(".scheduledDay").addClass("hide");
                $(".scheduledDays").addClass("hide");
                $(".scheduledAt").addClass("hide");

                $trigger = "-atStartup";
                $days = "";
                $time = "";
            }



            if ($(this).val() == "") {
                $(".scheduledDay").addClass("hide");
                $(".scheduledDays").addClass("hide");
                $(".scheduledAt").addClass("hide");

                $trigger = "";
                $days = "";
                $time = "";
            }

            setParam();

        });



        // change scheduled date
        $("#date").on("change", function() {

            $days = $(this).val();

            console.log($days);

            setParam();

        });



        // change scheduled time
        $("#time").on("change", function() {

            $time = $(this).val();

            console.log($time);

            setParam();

        });


        // change scheduled days
        $(".days").on("change", function() {


            var daysArr = [];

            $.each($(".days"), function(id, value) {


                // Push days in an array
                if ($(this).is(":checked")) {
                    daysArr.push($(this).prop("id"));
                }

            })

            // Join days together
            $days = daysArr.join(",");

            console.log($days);

            setParam();

        });



        // Set all the parameters for the scheduled task
        function setParam() {

            if ($trigger != "") {
                $("#form_schParameter").val($trigger + ';' + $time + ';' + $days);
            } else {
                $("#form_schParameter").val('');
            }

        }



    });

</script>

@stop
