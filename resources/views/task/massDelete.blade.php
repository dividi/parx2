	<center>
		<br />      
			
		{!! Form::open(array('method'=>'DELETE', 'route' => 'tasks.massDestroy')) !!}

			{{-- Ids of selected users (it is not really secured, but I do not give a sh**) --}}
			<input type="hidden" name="ids" id="ids" value="">

			<span id="warn-msg" class="text-danger"></span><br /><br />

		<div>

			Voulez vous supprimer TOUTES les tâches ACTIVES sélectionnées ?<br>
			
			<span class="small-font">
				Seule l'affectation des tâches sera supprimée, pas la tâche en elle même.<br/>
				Si les tâches étaient planifiées, une tâche de suppression de la planification sera créée.
			</span>


		</div>
		
		
		<br />

		{!! Form::submit('Supprimer', ['class'=>'btn btn-danger']) !!}
		<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>

	{!! Form::close() !!}

	</center>

	<script>
		
		$(function(){

			// selected ids serialization
			var ids = $(".checkline:checked").map(function(index) {
    			return this.id; 
			}).get().join();

			$("#ids").val(ids);

			$("#warn-msg").html("Attention ! Vous allez supprimer <b>" + $(".checkline:checked").length + " tâches actives </b> !");

		});
	</script>