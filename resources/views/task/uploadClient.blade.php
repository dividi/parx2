@extends('layout')


@section('navtitle') Modification d'une tâche @stop

@section('help') Modification d'une tâche @stop


@section('body')


<div class="form-container">

	<div class="cardbox">
		Le fichier de MAJ doit être au format ZIP. <br />
		Il doit contenir les fichiers à mettre à jour sur le client. <br />
		Tout doit être à la racine de l'archive, pas de sous dossier. <br />
		Ne pas oublier de créer une tâche de MAJ après upload.
	</div>

		{!! Form::open(array('route' => 'storeClient','method'=>'POST', 'files'=>true)) !!}

		<table class="form-table">
			<tr>
				<td>{!! Form::label('version', 'Version') !!}</td>
				<td>{!! Form::number('version') !!}</td>
			</tr>
			<tr>
				<td>{!! Form::label('client', 'Mise à jour') !!}</td>
				<td>{!! Form::file('client') !!}</td>
			</tr>
		</table>

		<div class="cardbox">
			{!! Form::submit('Envoyer', array('class'=>'btn btn-primary')) !!}
			<button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
		</div>

		{!! Form::close() !!}

	</div>

	@stop