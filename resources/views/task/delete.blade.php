<center>
	<form method="POST" action={{ URL::route( 'tasks.destroy', [$task->id] )  }}>
		
		<input type="hidden" name="_method" value="DELETE">

		{!! csrf_field() !!}

		<div>

			Vous allez supprimer la tâche {{ $task->name }}?<br>
			
			<span class="small-font">
				Les tâches actives seront supprimées en même temps !
			</span>


		</div>
		
		
		<br />

		<input type='submit' class='btn btn-danger' value='Supprimer'>
		<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>

	</form>
</center>
