
@extends('layout')


@section('navtitle') TÂCHES
<li class="title-info"> <span title="affichés">{{ count($tasks) }} </span> / <span title="total">{{$total_count}}</li> @stop


@section('help') Les tâches sont les actions qu'on peut lancer sur les clients. Ici c'est la réserve de tâches. Il faut associer une tâche à un hôte. @stop


@section('buttons')
	<li> <a href={{ URL::route('uploadPackage' ) }} data-title="Envoyer un Package" class='' title='Envoyer un Package'><i class="fa fa-file-archive-o"></i> Packages</a> </li>
	<li> <a href={{ URL::route('uploadClient' ) }} data-title="Envoyer une archive de MAJ" class='' title='Envoyer une archive de MAJ'><i class="fa fa-file-archive-o"></i> MAJ Client</a> </li>

	<li> <a href={{ URL::route('tasks.assignHosts' ) }} class='' title='Ajouter une tâche à des hôtes'><i class="fa fa-plus-circle"></i> Assigner Hôtes</a> </li>
	<li> <a href={{ URL::route('tasks.assignHostGroup' ) }} class='' title="Ajouter un groupe d'hôtes"><i class="fa fa-plus-circle"></i> Assigner Groupe</a> </li>
	<li> <a href={{ URL::route('tasks.create' ) }} class='' title='Ajouter une tâche'><i class="fa fa-plus-circle"></i> Ajouter</a> </li>
@stop


@section('body')

	{!! Form::open( ['url' => route('tasks.index'), 'method' => 'get', 'id' => 'filter_form'] )!!}

	<div class="table-responsive">

		<table class="nice">
			<thead>
				<tr>
					<th>Nom</th>
					<th>Programme</th>
					<th>Paramètres</th>
					<th>Admin</th>
					<th>Description</th>
					<th><i class="fa fa-desktop" title="Nombre de d'hôtes associés"></i></th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
				</tr>
		
				<tr class="filters">
					<th>{!! Form::text('filter_name', $request['filter_name'], array('size' => 6) ) !!}</th>
					<th>{!! Form::text('filter_exe', $request['filter_exe'], array('size' => 6) ) !!}</th>
					<th>{!! Form::text('filter_args', $request['filter_args'], array('size' => 6) ) !!}</th>
					<th></th>
					<th>{!! Form::text('filter_description', $request['filter_description'], array('size' => 6) ) !!}</th>
					<th>{!! Form::text('filter_host_count', $request['filter_host_count'], array('size' => 4) ) !!}</th>
					
					<th colspan=3>
						{!! Form::Button("<i class='fa fa-search'></i>", array('onclick' => "submit()")) !!}
						{!! Form::Button("<i class='fa fa-times-circle'></i>", array('onclick' => "$('#filter_form').find('input').val('') ; submit() ")) !!}
					</th>
				</tr>

			</thead>

			@foreach( $tasks as $task )

				@if ($task->name != "delete-schTask")

					<tr>
						<td class='name'> {{$task->name}} </td>
						<td class="exe"> {{ $task->exe }} </td>
						<td> {{ $task->args }} </td>
						<td align=center> 
							@if ( $task->asAdmin == 1 )
								<i class="fa fa-check"></i>
							@endif
						</td>
						<td> {{ $task->description }} </td>
						<td> <span class="badge">{{ count($task->hosts) }}</span> </td>


						<td class="line-button edit-button">
							@if ($task->exe != "package" && $task->exe != "update" && $task->name != "shutdown" && $task->name != "reboot" && $task->name != "logout" and $task->name != "update" && $task->name != "restart")
								<a href={{ URL::route('tasks.edit', array('id'=>$task->id) ) }} class='btn' title='Modifier {{$task->name}}'><i class="fa fa-pencil"></i></a>
							@endif
						</td>

						<td class="line-button">
							@if ($task->name != "shutdown" && $task->name != "reboot" && $task->name != "logout" and $task->name != "update" && $task->name != "restart")	
								<a href={{ URL::route('tasks.delete', array('modal'=>'true', 'id'=>$task->id) ) }} class='modalbox btn' title='Supprimer {{$task->name}}'><i class="fa fa-trash"></i></a> 
							@endif
						</td>

					</tr>
				@endif

			@endforeach

		</table>

		<div class="flex-item item2">
			{{ $tasks->appends(compact('request'))->links('pagination::bootstrap-4') }}
		</div>

	</div>

<script language="JavaScript" src="{{ URL::asset('/js/all.js') }}"></script>

<script>
    $(function () {

        $('.filters input').keypress(function (e) {
            if (e.which == 13) {
                $('#filter_form').submit();
                return false;
            }
        });

    });
</script>


@stop