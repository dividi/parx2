@extends('layout')


@section('navtitle') Modification d'une tâche @stop

@section('help') Modification d'une tâche @stop


@section('body')
 

<div class="form-container">    
			
		{!! Form::model($task, ['method'=>'PATCH', 'route'=>['tasks.update', $task->id]]) !!}


			<table class="form-table">
				<tr>
					<td>{!! Form::label('name', 'Nom') !!}</td>
					<td>{!! Form::text('name', null, array('required' => '')) !!}</td>
				</tr>

				
				<tr>
					<td>{!! Form::label('exe', 'Programme') !!}</td>
					<td>{!! Form::text('exe', null, array('required' => '')) !!}</td>
				</tr>

				
				<tr>
					<td>{!! Form::label('args', 'Paramètres') !!}</td>
					<td>{!! Form::text('args') !!}</td>
				</tr>

				<tr>
					<td>{!! Form::label('asAdmin', 'Admin') !!}</td>
					<td>{!! Form::checkbox('asAdmin') !!}</td>
					<td><a href="#" data-toggle="popover" data-content="Lance la tâche avec le compte SYSTEME et pas avec le compte COURANT.<br>Attention ! Lancer une tâche en ADMIN ne lance RIEN dans la session de l'utilisateur courant. Une tâche planifiée sera toujours lancée avec l'utilisateur SYSTEM " class="help"><i class="fa fa-question-circle"></i></a></td>
				</tr>

				
				<tr>
					<td>{!! Form::label('description', 'Description') !!}</td>
					<td>{!! Form::text('description') !!}</td>
				</tr>

			</table>


			<div class="cardbox">
				{!! Form::submit('Modifier', ['class'=>'btn btn-primary']) !!}
				<button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
			</div>


		{!! Form::close() !!}

</div>


	<script>

		$(function(){

			// give focus to first field
			$("input:first").focus();


			// POPO veur
			$('[data-toggle="popover"]').popover({
		    	placement : 'right',
		    	html: true,
		    	trigger: "hover",
		    	template: '<div class="popover help-popover"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>',
		    });

		});

		
	</script>

@stop