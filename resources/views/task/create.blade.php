@extends('layout')


@section('navtitle') Création d'une tâche @stop

@section('help') Création d'une nouvelle tâche. @stop


@section('body')
 

<div class="form-container">

			
		{!! Form::open(array('route' => 'tasks.store')) !!}


			<table class="form-table">
				<tr>
					<td>{!! Form::label('name', 'Nom') !!}</td>
					<td>{!! Form::text('name', null, array('required' => '')) !!}</td>
					<td><a href="#" data-toggle="popover" data-content="Le nom de la tâche." class="help"><i class="fa fa-question-circle"></i></a></td>
				</tr>

				
				<tr>
					<td>{!! Form::label('exe', 'Programme') !!}</td>
					<td>{!! Form::text('exe', null, array('required' => '')) !!}</td>
					<td><a href="#" data-toggle="popover" data-content="Le programme à exécuter." class="help"><i class="fa fa-question-circle"></i></a></td>
				</tr>

				
				<tr>
					<td>{!! Form::label('args', 'Paramètres') !!}</td>
					<td>{!! Form::text('args') !!}</td>
					<td><a href="#" data-toggle="popover" data-content="Les paramètres du programme à exécuter." class="help"><i class="fa fa-question-circle"></i></a></td>
				</tr>


				<tr>
					<td>{!! Form::label('asAdmin', 'Admin') !!}</td>
					<td>{!! Form::checkbox('asAdmin') !!}</td>
					<td><a href="#" data-toggle="popover" data-content="Lance la tâche avec le compte SYSTEME et pas avec le compte COURANT.<br>Attention ! Lancer une tâche en ADMIN ne lance RIEN dans la session de l'utilisateur courant. Une tâche planifiée sera toujours lancée avec l'utilisateur SYSTEM " class="help"><i class="fa fa-question-circle"></i></a></td>
				</tr>

				
				<tr>
					<td>{!! Form::label('description', 'Description') !!}</td>
					<td>{!! Form::text('description') !!}</td>
				</tr>

			</table>


			<div class="cardbox">
				{!! Form::submit('Ajouter', ['class'=>'btn btn-primary']) !!}
				<button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
			</div>

		{!! Form::close() !!}

</div>

<script>
	$(function() {

		$("input:first")
			.focus();
	});
</script>

@stop