<center>

		{!! Form::open(array('method'=>'DELETE', 'route'=>['tasks.destroyHost', $couple_id] )) !!}

		<div>

			Vous allez supprimer la tâche <b> {{ $task->name }} de l'hôte {{ $host->name }}</b> ?<br>
			
			<span class="small-font">
				Seule l'affectation de la tâche sera supprimée, pas la tâche en elle même.<br/>
				Si la tâche était planifiée, une tâche de suppression de la planification sera créée.
			</span>


		</div>
		
		
		<br />

		{!! Form::submit('Supprimer', ['class'=>'btn btn-danger']) !!}
		<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>

	{!! Form::close() !!}
</center>
