
@extends('layout')


@section('navtitle') TÂCHES ACTIVES 
	<li id="selectedCount" class="title-info hidden-button hide"></li>
	<li class="title-info hidden-button hide" title="sélectionnés">/</li>
	<li class="title-info"> <span title="affichés">{{ count($hosts) }} </span> / <span title="total">{{$total_count}}</li> @stop


@section('help') Les tâches actives lancées sur les clients. @stop


@section('buttons')

	@if ($show == '1')
		<li> <a href={{ URL::route('tasks.toggleHiddenTasks', array('show'=>0) ) }} title='Masque les tâches invisibles'><i class="fa fa-eye"></i> Masque</a> </li>
	@else
		<li> <a href={{ URL::route('tasks.toggleHiddenTasks', array('show'=>1) ) }} title='Montre les tâches invisibles'><i class="fa fa-eye-slash"></i> Masque</a> </li>
	@endif

	<li> <a href={{ URL::route('tasks.assignHosts' ) }} class='' title='Ajouter une tâche à des hôtes'><i class="fa fa-plus-circle"></i> Assigner Hôtes</a> </li>
	<li> <a href={{ URL::route('tasks.assignHostGroup' ) }} class='' title="Ajouter un groupe d'hôtes"><i class="fa fa-plus-circle"></i> Assigner Groupe</a> </li>
	<li class="hidden-button hide"> <a href={{ URL::route('tasks.massDelete' ) }} class='modalbox' title='Supprimer les tâches sélectionnées'><i class="fa fa-times-circle"></i> Suppression	</a> </li>
@stop

@section('body')
	

	{!! Form::open( ['url' => route('tasks.active'), 'method' => 'get', 'id' => 'filter_form'] )!!}

	<div class="table-responsive">

		<table class="nice">
			<thead>
				<tr>	
					<th data-sorter="false" data-filter="false"><input type="checkbox" class="checkall"></th>
					<th>Num</th>				
					<th>Hôte</th>				
					<th>Tâche</th>
					<th>Programme</th>
					<th>Paramètres</th>
					<th>Description</th>
					<th data-sorter="false" data-filter="false">asAdmin</th>
					<th>Planification</th>
					<th data-sorter="false" data-filter="false"></th>
				</tr>

				<tr class="filters">	
					<th></th>
					<th>{!! Form::text('filter_id', $request['filter_id'], array('size' => 3) ) !!}</th>				
					<th>{!! Form::text('filter_host', $request['filter_host'], array('size' => 6) ) !!}</th>				
					<th>{!! Form::text('filter_task_name', $request['filter_task_name'], array('size' => 6) ) !!}</th>
					<th>{!! Form::text('filter_exe', $request['filter_exe'], array('size' => 6) ) !!}</th>
					<th>{!! Form::text('filter_args', $request['filter_args'], array('size' => 6) ) !!}</th>
					<th>{!! Form::text('filter_description', $request['filter_description'], array('size' => 6) ) !!}</th>
					<th>&nbsp;</th>
					<th>{!! Form::text('filter_schedule', $request['filter_schedule'], array('size' => 6) ) !!}</th>
					
					<th width=80px>
						{!! Form::Button("<i class='fa fa-search'></i>", array('onclick' => "submit()")) !!}
						{!! Form::Button("<i class='fa fa-times-circle'></i>", array('onclick' => "$('#filter_form').find('input').val('') ; submit() ")) !!}
					</th>

				</tr>

			</thead>

			@foreach( $hosts as $host )

				@if ( $host->tasks )

					@foreach ($host->tasks as $task)

						@if ($task->name != "delete-schTask" || $show == 1)

							<tr>
								<td class="line-button"><input type="checkbox" class="checkline" name={{ $task->id }} id={{ $task->pivot->id }}></td>

								<td> {{ $task->pivot->id }} </td>
								
								<td> {{ $host->name }} </td>
								
								<td> {{ $task->name }} </td>
								<td> {{ $task->exe }} </td>
								<td> {{ $task->args }} </td>
								<td> {{ $task->description }} </td>
								<td align=center> 
									@if ( $task->asAdmin == 1 )
										<i class="fa fa-check"></i>
									@endif
								</td>

								<td> {{ $task->pivot->schedule }} </td>
								<td class="line-button"><a href={{ URL::route('tasks.unassignHost', array('modal'=>'true', 'task_id'=>$task->id, 'host_id'=>$host->id, 'couple_id'=>$task->pivot->id) ) }} class='modalbox btn' title='Supprimer {{$task->name}}'><i class="fa fa-trash"></i></a> </td>
							</tr>

						@endif

					@endforeach

				@endif
				
			@endforeach
		</table>

		<div class="flex-item item2">
			{{ $hosts->appends(compact('request'))->links('pagination::bootstrap-4') }}
		</div>


	</div>

	<script language="JavaScript" src="{{ URL::asset('/js/all.js') }}"></script>

	<script>
    $(function () {

        $('.filters input').keypress(function (e) {
            if (e.which == 13) {
                $('#filter_form').submit();
                return false;
            }
        });

    });
</script>


@stop