@extends('layout')


@section('navtitle') Duplication d'un raccourci @stop

@section('help') Duplication d'un raccourci et affectation à une liste de profils @stop


@section('body')


<div class="form-container">

    <div class="cardbox">
        Sur quels profils voulez vous DUPLIQUER le raccourci : <br />
        <b>{{ $shortcut->name }} ({{$shortcut->target}})</b>
    </div>

    {!! Form::open(array('method'=>'POST', 'route'=>['shortcutsMgt.duplicateStore', $shortcut->id] )) !!}


    <table class="form-table">

        <tr valign="top">
            <td>{!! Form::label('profiles', 'Profils') !!}</td>
            <td>{!! Form::select('other_profiles[]', $other_profiles, null, array('multiple' => true, "class" => "chosen")) !!}</td>
        </tr>

    </table>


    <div class="cardbox">
        {!! Form::submit('Dupliquer', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>

    </div>

    {!! Form::close() !!}

</div>


<script>
    $(function() {
        $("input:first").focus();
        $(".chosen").chosen();
    });

</script>


@stop
