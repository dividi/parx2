
@extends('layout')


@section('navtitle') RACCOURCIS 
	<li id="selectedCount" class="title-info hidden-button hide"></li>
	<li class="title-info hidden-button hide" title="sélectionnés">/</li>
	<li class="title-info"> <span title="affichés">{{ count($shortcuts) }} </span> / <span title="total">{{$total_count}}</li> @stop


@section('help') Tous les raccourcis de tous les profils, pour une gestion simplifiée. @stop


@section('buttons')
	<li> <a href={{ URL::route('shortcutsMgt.massCreate') }} class='' title='Ajouter un raccourci'><i class="fa fa-plus-circle"></i> Ajouter</a> </li>

	<li class="hidden-button hide"> <a href={{ URL::route('shortcutsMgt.massDelete' ) }} class='modalbox' title='Supprimer les raccourcis sélectionnées'><i class="fa fa-times-circle"></i> Suppression	</a> </li>
@stop


@section('body')

	<div class="table-responsive">

	{!! Form::open( ['url' => route('shortcutsMgt.index'), 'method' => 'get', 'id' => 'filter_form'] )!!}

		<table class="nice">
			<thead>
				<tr>
					<th><input type="checkbox" class="checkall"></th>
					<th>Raccourci</th>
					<th>Profil</th>
					<th>Localisation</th>
					<th>Chemin</th>

					<th>&nbsp;</th>
					<th>&nbsp;</th>
				</tr>
				<tr class="filters">
					<th>&nbsp;</th>
					<th>{!! Form::text('filter_name', $request['filter_name'], array('size' => 6) ) !!}</th>				
					<th>{!! Form::text('filter_profile', $request['filter_profile'], array('size' => 6) ) !!}</th>				
					<th>
						{!! Form::select('filter_location', [
						''	=> '',
						'0' => 'Bureau Utilisateur',
						'1' => 'Bureau Public',
						'2' => 'Désactivé',
						'3' => 'Barre des tâches',

						], $request['filter_location'], array('id' => 'location')) !!}
					</th>
					<th>{!! Form::text('filter_target', $request['filter_target'], array('size' => 6) ) !!}</th>				
					
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th width=80px>
						{!! Form::Button("<i class='fa fa-search'></i>", array('onclick' => "submit()")) !!}
						{!! Form::Button("<i class='fa fa-times-circle'></i>", array('onclick' => "$('#filter_form').find('input').val('') ; $('#location option:eq(0)').prop('selected', true) ; submit() ")) !!}
					</th>
				</tr>		
			</thead>


			@foreach( $shortcuts as $shortcut )
				
					<tr>
						<td class="line-button"><input type="checkbox" class="checkline" name={{ $shortcut->id }} id={{ $shortcut->id }}></td>
						
						<td title="{{ $shortcut->target }}  {{ $shortcut->arguments }}"> {{ $shortcut->name }} </td>
						
						<td class="width-auto">{{ $shortcut->profile->name }}</td>

						<td>
							@if ($shortcut->location  == 2)
								Désactivé
							@elseif ($shortcut->location  == 0)
								Bureau Utilisateur
							@elseif ($shortcut->location  == 1)
								Bureau Public
							@elseif ($shortcut->location  == 3)
								Barre des tâches
							@endif
						</td>

						<td class="width-auto">{{ $shortcut->target }}</td>


						<td class="line-button"><a href={{ URL::route('shortcutsMgt.duplicate', array('id'=>$shortcut->profile->id, 'shortcut_id'=>$shortcut->id) ) }} class='btn' title="Dupliquer {{$shortcut->name}}"><i class="fa fa-files-o"></i></a> </td>
						<td class="line-button"><a href={{ URL::route('shortcuts.edit', array('id'=>$shortcut->profile->id, 'shortcut_id'=>$shortcut->id) ) }} class='btn' title="Modifier {{$shortcut->name}}"><i class="fa fa-pencil"></i></a> </td>
						<td class="line-button"><a href={{ URL::route('shortcuts.delete', array('modal'=>'true', 'id'=>$shortcut->profile->id, 'shortcut_id'=>$shortcut->id) ) }} class='modalbox btn' title="Supprimer {{$shortcut->name}}"><i class="fa fa-trash"></i></a> </td>

					</tr>

			@endforeach
		</table>

		<div class="flex-item item2">
			{{ $shortcuts->appends(compact('request'))->links('pagination::bootstrap-4') }}
		</div>


	</div>


	<script language="JavaScript" src="{{ URL::asset('/js/all.js') }}"></script>
	<script>
    $(function () {

        $('.filters input').keypress(function (e) {
            if (e.which == 13) {
                $('#filter_form').submit();
                return false;
            }
        });

    });
</script>

@stop
