
@extends('layout')


@section('navtitle') UTILISATEUR {{ $user->name }}  @stop

@section('help') La page complète de l'utilisateur. @stop


@section('body')


<a href={{ URL::route('users.index') }} class='btn btn-primary'><i class="fa fa-caret-left"></i></i> RETOUR A LA LISTE</a>


<div class="page-title"> {{ $user->name }} </div>

<div class="page-actions">
	<a href={{ URL::route('users.delete', array('modal'=>'true', 'id'=>$user->id) ) }} class='modalbox btn btn-danger' title='Supprimer {{$user->name}}'><i class="fa fa-trash"></i> SUPPRIMER</a>
	<a href={{ URL::route('users.edit', array('id'=>$user->id) ) }} class='btn btn-primary' title='Modifier {{$user->name}}'><i class="fa fa-pencil"></i> MODIFIER</a>

</div>

<br>


<div style="display:flex; flex-wrap:wrap;">


<div class="panel panel-primary" style="max-width:200px; margin:15px;">
	<div class="panel-heading">
		<h3 class="panel-title">GROUPES D'UTILISATEURS <span class="badge">{{ count($user->usergroups) }}</span></h3>
	</div>
	  <div class="panel-body" style="overflow-y:auto; max-height:200px;">
		<ul>
			@forelse($user->usergroups as $usergroup)
		    	<li>{{ $usergroup->name }}</li>
			@empty
		   		<p>pas de groupe associé</p>
			@endforelse
		</ul>
	  </div>
	</div>



<div class="panel panel-primary" style="max-width:200px; margin:15px;">
	<div class="panel-heading">
		<h3 class="panel-title">PROFILS <span class="badge">{{ count($user->profiles) }}</span></h3>
	</div>
	  <div class="panel-body" style="overflow-y:auto; max-height:200px;">
		<ul>
			@forelse($user->profiles as $profile)
		    	<li>{{ $profile->name }}</li>
			@empty
		   		<p>pas de profil associé</p>
			@endforelse
		</ul>
	  </div>
	</div>

@stop