@extends('layout')


@section('navtitle') Import des données d'un utilisateur @stop

@section('help') Importation des groupes et/ou profils d'un utilisateur dans une sélection d'utilisateurs @stop


@section('body')

<div class="form-container">

    <div class="cardbox text-danger">
        Attention ! <br />
        Vous allez modifier les groupes et/ou les profils de <b>{{ $count_ids }} utilisateur(s)</b> <br />
        Les utilisateurs recevront les profils et/ou les groupes de l'utilisateur sélectionné ci-dessous.
    </div>

    {!! Form::open(array('method'=>'PATCH', 'route' => 'users.massImport')) !!}

    {{-- Ids of selected items --}}
    <input type="hidden" id="ids" name="ids" value={{ $ids }} />


    <table class="form-table">

        <tr>
            <td>
                {!! Form::label('source_user', 'A partir de ') !!}
            </td>

            <td>
                <select class="form-control chosen" name="source_user">
                    @foreach($users as $user)
                    <option value="{{$user->id}}">{{$user->lastName}} {{$user->firstName}} ({{$user->login}})</option>
                    @endforeach
                </select>
            </td>
        </tr>

        <tr>
            <td>{!! Form::label('import_groups', 'Importer les groupes') !!}</td>
            <td>{!! Form::checkbox('import_groups') !!}</td>
        </tr>

        <tr>
            <td>{!! Form::label('import_profiles', 'Importer les profils') !!}</td>
            <td>{!! Form::checkbox('import_profiles') !!}</td>
        </tr>

    </table>

    <div class="cardbox">
        {!! Form::submit('Importer', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>


    {!! Form::close() !!}

</div>

<script>
    $(function() {
        $(".chosen").chosen();
    });

</script>

@stop
