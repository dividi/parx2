@extends('layout')

@section('navtitle') Création d'un nouvel utilisateur @stop

@section('help') Création d'un nouvel utilisateur. @stop

@section('body')


<div class="form-container">

    {!! Form::open(array('route' => 'users.store')) !!}

    <table class="form-table">
        <tr>
            <td>{!! Form::label('login', 'Login') !!}</td>
            <td>{!! Form::text('login', null, array('required' => '')) !!}</td>
        </tr>

        <tr>
            <td>{!! Form::label('firstName', 'Prénom') !!}</td>
            <td>{!! Form::text('firstName', null, array('required' => '')) !!}</td>
        </tr>

        <tr>
            <td>{!! Form::label('lastName', 'Nom') !!}</td>
            <td>{!! Form::text('lastName', null, array('required' => '')) !!}</td>
        </tr>

        <tr>
            <td> {!! Form::label('usergroups', 'Groupes') !!} </td>
            <td>{!! Form::select('usergroups[]', $usergroups, null, array('multiple' => true, "class" => "chosen")) !!}</td>
        </tr>

        <tr>
            <td> {!! Form::label('profiles', 'Profils') !!} </td>
            <td>{!! Form::select('profiles[]', $profiles, null, array('multiple' => true, "class" => "chosen")) !!}</td>
        </tr>


    </table>

    <div class="cardbox">
        {!! Form::submit('Ajouter', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>

    {!! Form::close() !!}
</div>

<script>
    $(function() {
        $("input:first").focus();
        $(".chosen").chosen();
    });

</script>

@stop
