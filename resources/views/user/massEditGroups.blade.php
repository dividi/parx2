@extends('layout')


@section('navtitle') Modification des groupes d'utilisateurs @stop

@section('help') Modificaiton des groupes d'une sélection d'utilisateurs @stop


@section('body')


<div class="form-container">


    <div class="cardbox text-danger">
        Attention ! <br />
        Vous allez modifier les groupes de <b>{{ $count_ids }} utilisateur(s)</b> <br />
        La liste des groupes sera vidée.
    </div>

    {!! Form::open(array('method'=>'PATCH', 'route' => 'users.massUpdateGroups')) !!}

    {{-- Ids of selected items --}}
    <input type="hidden" id="ids" name="ids" value={{ $ids }} />

    <table class="form-table">

        <tr>
            <td>{!! Form::label('usergroups', 'Groupes') !!}</td>
            <td>{!! Form::select('usergroups[]', $usergroups, null, array('multiple' => true, "class" => "chosen")) !!}</td>
        </tr>

    </table>


    <div class="cardbox">
        {!! Form::submit('Modifier', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>

    {!! Form::close() !!}

</div>


<script>
    $(function() {
        $(".chosen").chosen();
    });

</script>

@stop