@extends('layout')


@section('navtitle') Import CSV @stop

@section('help') Import CSV d'utilisateurs @stop


@section('body')


<div class="form-container">


    <div class="cardbox">

        Attention ! le fichier d'export généré par Parx ne peut pas être importé directement ici.<br />
        <br />
        Formalisme du fichier CSV (pas d'entête) :<br>
        login;prenom;nom;groupe
    </div>


    {!! Form::open(array('route' => 'users.importCSVStore','method'=>'POST', 'files'=>true)) !!}


    <table class="form-table">

        <tr valign="top">
            <td>{!! Form::label('group', 'Groupe') !!}</td>
            <td>{!! Form::select('group', array('ELEVE' => 'ELEVE', 'PROF' => 'PROF', 'AUTRE' => 'AUTRE'), null, array("class" => "chosen")) !!}</td>
        </tr>

        <tr>
            <td>{!! Form::label('file', 'Fichier') !!}</td>
            <td>{!! Form::file('file') !!}</td>
        </tr>

    </table>


    <div class="cardbox">
        {!! Form::submit('Importer', array('class'=>'btn btn-primary')) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>

    {!! Form::close() !!}


<script>
	$(function () {
		$(".chosen").chosen();
    });
</script>


    @stop
