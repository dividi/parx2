<center>

		{!! Form::open(array('method'=>'DELETE', 'route'=>['users.destroy', $user->id] )) !!}

		<div>

			Vous allez supprimer l'utilisateur <b> {{ $user->firstName }} {{ $user->lastName }}</b> ?<br>
			
			<span class="small-font">
				
			</span>


		</div>
		
		
		<br />

		{!! Form::submit('Supprimer', ['class'=>'btn btn-danger']) !!}
		<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>

	{!! Form::close() !!}
</center>
