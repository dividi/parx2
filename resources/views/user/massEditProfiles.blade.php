@extends('layout')


@section('navtitle') Modification des profils d'utilisateurs @stop

@section('help') Modificaiton des profils d'une sélection d'utilisateurs @stop


@section('body')


<div class="form-container">

    <div class="cardbox text-danger">
        Attention ! <br />
        Vous allez modifier les profils de <b>{{ $count_ids }} utilisateur(s)</b> <br />
        La liste des profils sera vidée.
    </div>

    {!! Form::open(array('method'=>'PATCH', 'route' => 'users.massUpdateProfiles')) !!}
    
	{{-- Ids of selected items --}}
    <input type="hidden" id="ids" name="ids" value={{ $ids }} />

    <table class="form-table">

        <tr>
            <td>{!! Form::label('profiles', 'Profils') !!}</td>
            <td>{!! Form::select('profiles[]', $profiles, null, array('multiple' => true, "class" => "chosen")) !!}</td>
        </tr>

    </table>

    <div class="cardbox">
        {!! Form::submit('Modifier', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>

    {!! Form::close() !!}

</div>


<script>
    $(function() {
        $(".chosen").chosen();
    });

</script>

@stop
