@extends('layout')


@section('navtitle') Import des données d'un utilisateur @stop

@section('help') Importation des groupes et/ou profils d'un utilisateur dans un autre @stop


@section('body')


<div class="form-container">

    <div class="cardbox">
        L'utilisateur {{$user->lastName}} {{$user->firstName}} ({{$user->login}}) <br />
        recevra les profils et/ou les groupes de l'utilisateur sélectionné ci-dessous.
    </div>



    {!! Form::model($user, ['method'=>'PATCH', 'route'=>['users.import', $user->id]]) !!}



    <table class="form-table">
        <tr>
            <td>
                {!! Form::label('source_user', 'A partir de ') !!}
            </td>

            <td>
                <select class="form-control chosen" name="source_user">
                    @foreach($users as $user)
                    <option value="{{$user->id}}">{{$user->lastName}} {{$user->firstName}} ({{$user->login}})</option>
                    @endforeach
                </select>
            </td>
        </tr>

        <tr>
            <td>{!! Form::label('import_groups', 'Importer les Groupes') !!}</td>
            <td>{!! Form::checkbox('import_groups') !!}</td>
        </tr>
        <tr>
            <td>{!! Form::label('import_profiles', 'Importer les Profils') !!}</td>
            <td>{!! Form::checkbox('import_profiles') !!}</td>
        </tr>
    </table>


    <div class="cardbox">
        {!! Form::submit('Importer', ['class'=>'btn btn-primary']) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>


    {!! Form::close() !!}

    <script>
        $(function() {
            $(".chosen").chosen();
        });

    </script>

    @stop
