
@extends('layout')


@section('navtitle') UTILISATEURS <li id="selectedCount" class="title-info hidden-button hide"></li><li class="title-info hidden-button hide" title="sélectionnés">/</li><li class="title-info"> <span title="affichés">{{ count($users) }} </span> / <span title="total">{{$total_count}}</li> @stop

@section('help') Les utilisateurs sont les personnes qui se connectent sur les hôtes. @stop



@section('buttons')
	
	<li> <a href={{ URL::route('users.export' ) }} title='export CSV'><i class="fa fa-file-text"></i> Export CSV</a> </li>
	<li> <a href={{ URL::route('users.importCSVCreate' ) }} class='' title='Importer par CSV'><i class="fa fa-file-text"></i> Import CSV</a> </li>
	<li> <a href={{ URL::route('users.importLDAPCreate' ) }} class='' title='Importer par LDAP'><i class="fa fa-database"></i> Import LDAP</a> </li>
	<li> <a href={{ URL::route('users.create' ) }} class='' title='Ajouter un utilisateur'><i class="fa fa-plus-circle"></i> Ajouter</a> </li>

	<li class="hidden-button hide"> <a href="" class='' id="submit_mass_import" title="Importer les groupes/profils d'un utilisateur à la sélection"><i class="fa fa-download"></i> Import	</a> </li>
	<li class="hidden-button hide"> <a href="" class='' id="submit_mass_groups" title='Assigner la sélection à des groupes'><i class="fa fa-object-group"></i> Groupes	</a> </li>
	<li class="hidden-button hide"> <a href="" class='' id="submit_mass_profiles" title='Assigner la sélection à des profils'><i class="fa fa-briefcase"></i> Profils	</a> </li>
	
	<li class="hidden-button hide"> <a href={{ URL::route('users.massDelete' ) }} class='modalbox' title='Supprimer les utilisateurs sélectionnés'><i class="fa fa-times-circle"></i> Suppression	</a> </li>
@stop



@section('body')


<form id="form_mass_import" method="POST" action="{{ route('users.showMassImport') }}">
    @csrf
	<input type="hidden" name="selection_ids" class="selection_ids" value="">
</form>

<form id="form_mass_groups" method="POST" action="{{ route('users.massEditGroups') }}">
    @csrf
	<input type="hidden" name="selection_ids" class="selection_ids" value="">
</form>

<form id="form_mass_profiles" method="POST" action="{{ route('users.massEditProfiles') }}">
    @csrf
	<input type="hidden" name="selection_ids" class="selection_ids" value="">
</form>




{!! Form::open( ['url' => route('users.index'), 'method' => 'get', 'id' => 'filter_form'] )!!}

	<div class="flex-container">
		<div class="flex-item item1">
		<table class="nice" >
			<thead>
				<tr>
					<th><input type="checkbox" class="checkall"></th>
					<th>Login</th>
					<th>Prénom</th>
					<th>Nom</th>
					<th>Profils</th>
					<th>Groupes</th>
					
					<th</th>
					<th</th>
					<th</th>
					<th></th>
				</tr>

				<tr class="filters">
					<th></th>
					<th>{!! Form::text('filter_login', $request['filter_login'], array('size' => 6) ) !!}</th>
					<th>{!! Form::text('filter_firstname', $request['filter_firstname'], array('size' => 6) ) !!}</th>
					<th>{!! Form::text('filter_lastname', $request['filter_lastname'], array('size' => 6) ) !!}</th>
					<th>{!! Form::text('filter_profiles', $request['filter_profiles'], array('size' => 6) ) !!}</th>
					<th>{!! Form::text('filter_groups', $request['filter_groups'], array('size' => 6) ) !!}</th>
					
					<th</th>
					<th</th>
					<th</th>
					<th colspan=4>
						{!! Form::Button("<i class='fa fa-search'></i>", array('onclick' => "submit()")) !!}
						{!! Form::Button("<i class='fa fa-times-circle'></i>", array('onclick' => "$('#filter_form').find('input').val('') ; submit() ")) !!}
					</th>
				</tr>

			</thead>

			@foreach( $users as $user )

				<tr>
					<td class="line-button"><input type="checkbox" class="checkline" name={{ $user->id }} id={{ $user->id }}></td>
					<td> {{ $user->login }} </td>
					<td> {{ $user->firstName }} </td>
					<td> {{ $user->lastName }} </td>

					<td> {{ $user->profiles->implode('name', ', ') }} </td>
					<td> {{ $user->usergroups->implode('name', ', ') }}	</td>

					<td class="line-button"><a href={{ URL::route('users.show', array('id'=>$user->id) ) }} class='btn' title="Information de {{$user->login}}"><i class="fa fa-info-circle"></i></a></td>
					<td class="line-button"><a href={{ URL::route('users.edit', array('id'=>$user->id) ) }} class='btn' title='Modifier {{$user->login}}'><i class="fa fa-pencil"></i></a></td>
					<td class="line-button"><a href={{ URL::route('users.showImport', array('id'=>$user->id) ) }} class='btn' title="Importer les groupes/profils vers {{$user->login}}"><i class="fa fa-download"></i></a></td>
					<td class="line-button"><a href={{ URL::route('users.delete', array('id'=>$user->id) ) }} class='modalbox btn' title='Supprimer {{$user->login}}'><i class="fa fa-trash"></i></a> </td>
					
				</tr>

			@endforeach
		</table>
		</div>
		
		<div class="flex-item item2">
			{{ $users->appends(compact('request'))->links('pagination::bootstrap-4') }}
		</div>


	</div>

	<script language="JavaScript" src="{{ URL::asset('/js/all.js') }}"></script>

	<script>
    $(function () {

        $('.filters input').keypress(function (e) {
            if (e.which == 13) {
                $('#filter_form').submit();
                return false;
            }
        });


        $("#submit_mass_import").click(function () {

            event.preventDefault();
            // selected ids serialization
            let ids = $(".checkline:checked").map(function (index) {
                return this.id;
            }).get().join();

            $(".selection_ids").val(ids);

            $("#form_mass_import").submit();

        });

		$("#submit_mass_groups").click(function () {

            event.preventDefault();
            // selected ids serialization
            let ids = $(".checkline:checked").map(function (index) {
                return this.id;
            }).get().join();

            $(".selection_ids").val(ids);

            $("#form_mass_groups").submit();

        });

		$("#submit_mass_profiles").click(function () {

            event.preventDefault();
            // selected ids serialization
            let ids = $(".checkline:checked").map(function (index) {
                return this.id;
            }).get().join();

            $(".selection_ids").val(ids);

            $("#form_mass_profiles").submit();

        });




    });
</script>


@stop