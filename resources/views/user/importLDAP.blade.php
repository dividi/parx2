@extends('layout')


@section('navtitle') Import LDAP Utilisateurs @stop

@section('help') Import LDAP d'utilisateurs @stop


@section('body')


<div class="form-container">

    {!! Form::open(array('route' => 'users.importLDAPStore','method'=>'POST')) !!}

    <table class="form-table">

        <tr>
            <td>{!! Form::label('ldap_server', 'Serveur LDAP') !!}</td>
            <td>{!! Form::text('ldap_server', $ldap_server->value) !!}</td>
            <td><a href="#" data-toggle="popover" data-content="Adresse IP du serveur" class="help"><i class="fa fa-question-circle"></i></a></td>
        </tr>

        <tr>
            <td>{!! Form::label('ldap_dn_user', 'Chemin Utilisateurs') !!}</td>
            <td>{!! Form::text('ldap_dn_user', $ldap_user_dn->value) !!}</td>
            <td><a href="#" data-toggle="popover" data-content="Chemin LDAP vers les utilisateurs. <small>ex : OU=Utilisateurs, OU=0132205B, OU=MARSEILLE CENTRE, OU=Groupes, OU=GIC, DC=SERCOL, DC=LAN</small>" class="help"><i class="fa fa-question-circle"></i></a></td>
        </tr>

        <tr>
            <td>{!! Form::label('ldap_login', 'Utilisateur') !!}</td>
            <td>{!! Form::text('ldap_login', $ldap_login->value) !!}</td>
            <td><a href="#" data-toggle="popover" data-content="Login autorisé. ex : DOMAIN\admin" class="help"><i class="fa fa-question-circle"></i></a></td>
        </tr>

        <tr>
            <td>{!! Form::label('ldap_pwd', 'Mot de passe') !!}</td>
            <td>{!! Form::password('ldap_pwd') !!}</td>
        </tr>

        <tr>
            <td>{!! Form::label('delete_all', 'Vider la liste avant') !!}</td>
            <td>{!! Form::checkbox('delete_all') !!}</td>
            <td><a href="#" data-toggle="popover" data-content="Vider la liste des utilisateurs avant de faire l'import" class="help"><i class="fa fa-question-circle"></i></a></td>
        </tr>

        <tr>
            <td>{!! Form::label('ldap_allowed_groups', 'Filtrer ces groupes') !!}</td>
            <td>{!! Form::textarea('ldap_allowed_groups', $ldap_allowed_groups->value, ['size' => '25x3']) !!}</td>
            <td><a href="#" data-toggle="popover" data-content="N'ajoute que ces groupes, séparés par un point virgule. Si le champ est vide tous les groupes sont importés. Les utilisateurs sont créés dans tous les cas. ex:Eleve;Professeur;Personnels Administratifs" class="help"><i class="fa fa-question-circle"></i></a></td>
        </tr>

    </table>


    <div class="cardbox">
        {!! Form::submit('Importer', array('class'=>'btn btn-primary')) !!}
        <button type="button" class="btn btn-default" onclick="javascript:history.back()">Annuler</button>
    </div>

    {!! Form::close() !!}

    @stop
