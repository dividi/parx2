<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

use Redirect;

use App\Host;
use App\User;
use App\Parameter;
use App\Shortcut;
use App\Profile;
use App\Task;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Http\Resources\ProfileResource;
use App\Http\Resources\UsergroupResource;
use App\Http\Resources\HostResource;
use App\Http\Resources\HostgroupResource;



class RestController extends Controller {




    /**
     * send a simple response to client (test purpose)
     */
    public function rest() {
        return json_encode( array ("code"=>0, "message"=>"Hello Parx Client") );
    }



    /**
     * get all hosts with profiles, groups...
     */
    public function getAllHosts() {
        
        $q = Host::with(["profiles", "hostgroups"])->get();
        $hosts = HostResource::collection($q);

        return json_encode($hosts);
    }


    /**
     * get all users with profiles, groups...
     */
    public function getAllUsers()
    {

        $q = User::with(["profiles", "usergroups"])->get();
        $users = UserResource::collection($q);

        return json_encode($users);
    }


    /**
     * update client version in database
     */
    public function updateVersion (Request $request) {

        $hostName = $request['hostName'];
        $hostVersion = $request['version'];
        
        $host = Host::where('name' , '=', $hostName)->first();

        if ( $host === NULL )  {

            $code = 1;
            $message = "HOST does not exist";

        } else {

            // update the version and last connection
            $host->version = $hostVersion;
            $host->save();

            $code = 0;
            $message = $host->id;
        }

        return json_encode( array ("code"=>$code, "message"=>$message) );
    }



    
    /**
     * update client version, os and last co in database
     */
    public function updateHost (Request $request) {

        $hostName = $request['hostName'];
        $hostOs = $request['hostOs'];
        $hostVersion = $request['version'];
        $lastConnection = $request['last_connection'];


        $host = Host::where('name' , '=', $hostName)->first();


        if ( $host === NULL )  {

            $code = 1;
            $message = "HOST does not exist";
        } else {

            // update the version
            $host->version = $hostVersion;
            $host->os = $hostOs;
            $host->last_connection = $lastConnection;

            $host->save();

            $code = 0;
            $message = $host->id;
        }

        return json_encode( array ("code"=>$code, "message"=>$message) );

    }



    /**
     * update client last profile name in database (cannot be done in updateHost from the parxshell)
     */
    public function updateHostLastProfile (Request $request) {

        $hostName = $request['hostName'];
        $lastProfile = $request['last_profile_name'];

        $host = Host::where('name' , '=', $hostName)->first();


        if ( $host === NULL )  {

            $code = 1;
            $message = "HOST does not exist";
        } else {

            // update the last_profile_name
            $host->last_profile_name = $lastProfile;

            $host->save();

            $code = 0;
            $message = $host->id;
        }

        return json_encode( array ("code"=>$code, "message"=>$message) );

    }




    /**
     * client send host to database
     */
    public function addHost (Request $request) {

        $hostName = $request['hostName'];
        $hostOs = $request['hostOs'];
        $hostVersion = $request['version'];

        $host = Host::where('name' , '=', $hostName)->first();


        if ( $host === NULL )  {

            // Add the host
            $host = New Host;
            $host->name = $hostName;
            $host->os = $hostOs;
            $host->version = $hostVersion;

            $host->save();

            $code = 0;
            $message = $host->id;

        } else {

            $code = 1;
            $message = "Host already in database";
        }


        return json_encode( array ("code"=>$code, "message"=>$message) );


    }






    /**
     * client get host id by hostname
     */
    public function getHostId ($hostName) {

        $host = Host::where('name' , '=', $hostName)->first();

        if ( $host === NULL )  {
            $code = 1;
            $message = "HOST " . $hostName . " doest not exist";
        }
        else {
            $code = 0;
            $message = $host->id;
        }

        return json_encode( array ("code"=>$code, "message"=>$message) );

    }



    /**
     * client get profiles  by hostname and userName
     */
    public function getProfiles ($hostName, $userLogin) {


        // check on host only
        $host_profiles = Profile::from("Profile as p")
                                ->join ("host_profile as hp", "p.id", "=", "hp.profile_id")
                                ->join ("Host as h", "h.id", "=", "hp.host_id")
                                ->where("h.name", "=", "$hostName")
                                ->where("p.matchType", "=", 2)
                                ->get(["p.id", "p.name", "p.matchType"]);


        // check on user only
        $user_profiles = Profile::from("Profile as p")
                                ->join ("user_profile as up", "p.id", "=", "up.profile_id")
                                ->join ("User as u", "u.id", "=", "up.user_id")
                                ->where("u.login", "=", "$userLogin")
                                ->where("p.matchType", "=", 1)
                                ->get(["p.id", "p.name", "p.matchType"]);


        // check on user+host
        $user_host_profiles = Profile::from("Profile as p")
                                ->join ("user_profile as up", "p.id", "=", "up.profile_id")
                                ->join ("User as u", "u.id", "=", "up.user_id")
                                ->join ("host_profile as hp", "p.id", "=", "hp.profile_id")
                                ->join ("Host as h", "h.id", "=", "hp.host_id")
                                ->where("h.name", "=", "$hostName")
                                ->where("u.login", "=", "$userLogin")
                                ->where("p.matchType", "=", 0)
                                ->get(["p.id", "p.name", "p.matchType"]);




        // check on hostgroup only
        $hostgroup_profiles = Profile::from("Profile as p")
                                ->join ("hostgroup_profile as hgp", "p.id", "=", "hgp.profile_id")
                                ->join ("host_hostgroup as hhg", "hhg.hostgroup_id", "=", "hgp.hostgroup_id")
                                ->join ("Host as h", "h.id", "=", "hhg.host_id")
                                ->where("h.name", "=", "$hostName")
                                ->where("p.matchType", "=", 3)
                                ->get(["p.id", "p.name", "p.matchType"]);


        // check on usergroup only
        $usergroup_profiles = Profile::from("Profile as p")
                                ->join ("usergroup_profile as ugp", "p.id", "=", "ugp.profile_id")
                                ->join ("user_usergroup as uug", "uug.usergroup_id", "=", "ugp.usergroup_id")
                                ->join ("User as u", "u.id", "=", "uug.user_id")
                                ->where("u.login", "=", "$userLogin")
                                ->where("p.matchType", "=", 4)
                                ->get(["p.id", "p.name", "p.matchType"]);


        // check on usergroup only
        $usergroup_hostgroup_profiles = Profile::from("Profile as p")
                                ->join ("hostgroup_profile as hgp", "p.id", "=", "hgp.profile_id")
                                ->join ("host_hostgroup as hhg", "hhg.hostgroup_id", "=", "hgp.hostgroup_id")
                                ->join ("Host as h", "h.id", "=", "hhg.host_id")
                                ->join ("usergroup_profile as ugp", "p.id", "=", "ugp.profile_id")
                                ->join ("user_usergroup as uug", "uug.usergroup_id", "=", "ugp.usergroup_id")
                                ->join ("User as u", "u.id", "=", "uug.user_id")
                                ->where("h.name", "=", "$hostName")
                                ->where("u.login", "=", "$userLogin")
                                ->where("p.matchType", "=", 5)
                                ->get(["p.id", "p.name", "p.matchType"]);                                

        // Merge all results in one table (to rule them all)
        $profiles = array_merge($user_host_profiles->toArray(), $usergroup_hostgroup_profiles->toArray(), $host_profiles->toArray(), $usergroup_profiles->toArray(), $hostgroup_profiles->toArray(), $user_profiles->toArray());

        usort($profiles, function($a, $b) {
            return  $a['name'] > $b['name'];
        });


        if ( $profiles ) {
            $code = 0;
            $message = json_encode($profiles);
        }
        else {
            $code = 1;
            $message = "Pas de profil correspondant.";
        }

        return json_encode( array ("code"=>$code, "message"=>$message) );

    }


/**
     * client get full profile (with shortcuts...) by the profile id
     */
    public function getFullProfile ($profile_id) {
        
        $profile = Profile::select("id", "name", "revision", "fileAssoc")
            ->where("id", $profile_id)
            ->with(["shortcuts", "printers", "shares", "parameters"])
            ->get();

        /*if ($profile->isEmpty()) {
            return $profile->toJson();
        }*/

        return $profile->toJson();

    }



    /**
     * client get full profiles (with shortcuts...) by hostname and userName
     */
    public function getFullProfiles ($hostName, $userLogin) {


        // check on host only
        $host_profiles = Profile::from("Profile as p")
                                ->join ("host_profile as hp", "p.id", "=", "hp.profile_id")
                                ->join ("Host as h", "h.id", "=", "hp.host_id")
                                ->where("h.name", "=", "$hostName")
                                ->where("p.matchType", "=", 2)
                                ->get(["p.id", "p.name", "p.matchType", "p.fileAssoc"]);

        // check on user only
        $user_profiles = Profile::from("Profile as p")
                                ->join ("user_profile as up", "p.id", "=", "up.profile_id")
                                ->join ("User as u", "u.id", "=", "up.user_id")
                                ->where("u.login", "=", "$userLogin")
                                ->where("p.matchType", "=", 1)
                                ->get(["p.id", "p.name", "p.matchType", "p.fileAssoc"]);


        // check on user+host
        $user_host_profiles = Profile::from("Profile as p")
                                ->join ("user_profile as up", "p.id", "=", "up.profile_id")
                                ->join ("User as u", "u.id", "=", "up.user_id")
                                ->join ("host_profile as hp", "p.id", "=", "hp.profile_id")
                                ->join ("Host as h", "h.id", "=", "hp.host_id")
                                ->where("h.name", "=", "$hostName")
                                ->where("u.login", "=", "$userLogin")
                                ->where("p.matchType", "=", 0)
                                ->get(["p.id", "p.name", "p.matchType", "p.fileAssoc"]);


        // check on hostgroup only
        $hostgroup_profiles = Profile::from("Profile as p")
                ->join ("hostgroup_profile as hgp", "p.id", "=", "hgp.profile_id")
                ->join ("host_hostgroup as hhg", "hhg.hostgroup_id", "=", "hgp.hostgroup_id")
                ->join ("Host as h", "h.id", "=", "hhg.host_id")
                ->where("h.name", "=", "$hostName")
                ->where("p.matchType", "=", 3)
                ->get(["p.id", "p.name", "p.matchType", "p.fileAssoc"]);


        // check on usergroup only
        $usergroup_profiles = Profile::from("Profile as p")
                ->join ("usergroup_profile as ugp", "p.id", "=", "ugp.profile_id")
                ->join ("user_usergroup as uug", "uug.usergroup_id", "=", "ugp.usergroup_id")
                ->join ("User as u", "u.id", "=", "uug.user_id")
                ->where("u.login", "=", "$userLogin")
                ->where("p.matchType", "=", 4)
                ->get(["p.id", "p.name", "p.matchType", "p.fileAssoc"]);


        // check on usergroup only
        $usergroup_hostgroup_profiles = Profile::from("Profile as p")
                ->join ("hostgroup_profile as hgp", "p.id", "=", "hgp.profile_id")
                ->join ("host_hostgroup as hhg", "hhg.hostgroup_id", "=", "hgp.hostgroup_id")
                ->join ("Host as h", "h.id", "=", "hhg.host_id")
                ->join ("usergroup_profile as ugp", "p.id", "=", "ugp.profile_id")
                ->join ("user_usergroup as uug", "uug.usergroup_id", "=", "ugp.usergroup_id")
                ->join ("User as u", "u.id", "=", "uug.user_id")
                ->where("h.name", "=", "$hostName")
                ->where("u.login", "=", "$userLogin")
                ->where("p.matchType", "=", 5)
                ->get(["p.id", "p.name", "p.matchType", "p.fileAssoc"]);                                

        // Merge all results in one table (to rule them all)
        $profiles = array_merge($user_host_profiles->toArray(), $usergroup_hostgroup_profiles->toArray(), $host_profiles->toArray(), $usergroup_profiles->toArray(), $hostgroup_profiles->toArray(), $user_profiles->toArray());


        usort($profiles, function($a, $b) {
            return  $a['name'] > $b['name'];
        });



        $newProfiles = array();

        // Add Shortcuts (ie printers, shares, options) to each profiles (or nothing)
        foreach ($profiles as $p) {

            // get all parts
            $shortcuts = Profile::find($p["id"])->shortcuts;
            $shares = Profile::find($p["id"])->shares;
            $printers = Profile::find($p["id"])->printers;
            $parameters = Profile::find($p["id"])->parameters;

            // add all parts
            $p["shortcuts"] = $shortcuts;
            $p["shares"] = $shares;
            $p["printers"] = $printers;
            $p["parameters"] = $parameters;

            // put the modified profile in a new list
            array_push ($newProfiles, $p);

        }

        // encode without return codes
        return json_encode( $newProfiles );

    }







    /**
     * client get profile by id
     */
    public function getProfile ($profileId) {

        $profile = Profile::find($profileId);

        if ( $profile === NULL )  {
            $code = 1;
            $message = "NO profile found";
        }
        else {
            $code = 0;
            $message = json_encode($profile->toArray());
        }

        return json_encode( array ("code"=>$code, "message"=>$message) );

    }



    /**
     * client get options by profile id
     */
    public function getParameters ($profileid) {

        $options = Profile::find($profileid)->parameters;

        if ( count($options) == 0 )  {
            $code = 1;
            $message = "No OPTIONS for profile #" . $profileid;
        }
        else {
            $code = 0;
            $message = json_encode($options);
        }

        return json_encode( array ("code"=>$code, "message"=>$message) );

    }



    /**
     * client get desktop shortcuts by profile id
     */
    public function getShortcuts ($profileid) {

        // remove disable and taskbar shortcuts
        $profile = Profile::with(array('shortcuts' => function($query){
            $query->where("location", "<>", 3)->where("location", "<>", 2);
         }))->find($profileid);

        $shortcuts = $profile->shortcuts;

        if ( count($shortcuts) == 0 )  {
            $code = 1;
            $message = "No SHORTCUTS for profile #" . $profileid;
        }
        else {
            $code = 0;
            $message = json_encode($shortcuts);
        }

        return json_encode( array ("code"=>$code, "message"=>$message) );

    }



    /**
     * client get taskbar shortcuts by profile id
     */
    public function getTaskbarShortcuts ($profileid) {

        // only get taskbar shortcuts
        $profile = Profile::with(array('shortcuts' => function($query){
            $query->where("location", 3);
         }))->find($profileid);

        $shortcuts = $profile->shortcuts;

        if ( count($shortcuts) == 0 )  {
            $code = 1;
            $message = "No SHORTCUTS for profile #" . $profileid;
        }
        else {
            $code = 0;
            $message = json_encode($shortcuts);
        }

        return json_encode( array ("code"=>$code, "message"=>$message) );

    }
    





    /**
     * client get shares by profile id
     */
    public function getShares ($profileid) {

        $shares = Profile::find($profileid)->shares;


        if ( count($shares) == 0 )  {
            $code = 1;
            $message = "No SHARES for profile #" . $profileid;
        }
        else {
            $code = 0;
            $message = json_encode($shares);
        }

        return json_encode( array ("code"=>$code, "message"=>$message) );
    }



    /**
     * client get printers by profile id
     */
    public function getPrinters ($profileid) {

        $printers = Profile::find($profileid)->printers;


        if ( count($printers) == 0 )  {
            $code = 1;
            $message = "No PRINTERS for profile #" . $profileid;
        }
        else {
            $code = 0;
            $message = json_encode($printers);
        }

        return json_encode( array ("code"=>$code, "message"=>$message) );
    }



    /**
     * client get an option value by a profile id and the parameter name
     */
    public function getParameter ($profileId, $parameterName) {


        $profile = Profile::find($profileId);
        $optionValue = $profile->parameters->where("name", $parameterName)->first()->pivot->parameterValue;


        if ( $optionValue ) {
            $code = 0;
            $message = $optionValue;
        }
        else {
            $code = 1;
            $optionDefault = Parameter::where("name", "=", $parameterName)->first()->defaultValue;
            $message = $optionDefault;
        }


        return json_encode( array ("code"=>$code, "message"=>$message) );

    }






    /**
     * client send shortcuts to database
     */
    public function saveShortcuts (Request $request) {


        // query
        $profileId = $request['profileId'];
        $desktop = $request['desktop'];

        $profile = Profile::find($profileId);
        $shortcuts = json_decode($desktop);


        // Delete all the shortcuts of the selected profile
        foreach ($profile->shortcuts as $shortcut) {
            $shortcut->delete();
        }

        // message stack
        $message_array = array();


        foreach ($shortcuts as $record) {
            
    
            if ( $record->name == "" ||  $record->target == "") {
                array_push($message_array, '{"code":"1", "message":"'.$record->name . ' : Empty"}');
            }
            else {

                // make a new shortcut
                $shortcut = new Shortcut;

                $shortcut->profile_id = $profileId;
                $shortcut->name = $record->name;
                $shortcut->target = $record->target;
                $shortcut->description = $record->description;
                $shortcut->arguments = $record->arguments;
                $shortcut->workingDir = $record->workingDir;
                $shortcut->location = $record->location;
                $shortcut->icon_position = $record->icon_position;

                $shortcut->save();

                array_push($message_array, '{"code":"0", "message":"'. $record->name . '"}');

            }        
            
        }

        // Create message dans send response
        $message = implode (",", $message_array);

        return json_encode( array ("message" => "[" . $message . "]") );
    }




    /**
     * client send shortcuts to database (release for Cli2Parx)
     */
    public function RESTsaveShortcuts (Request $request) {

        // query
        $profileId = $request['profileId'];
        $desktop = $request['desktop'];
        //$taskbar = $request['taskbar'];

        $shortcuts_desktop = json_decode($desktop, true);
        //$shortcuts_taskbar = json_decode("[" . $taskbar . "]");

        $profile = Profile::find($profileId);


        // Delete all the shortcuts
        foreach ($profile->shortcuts as $shortcut) {
            $shortcut->delete();
        }


        $message_array = array();

        // SHORTCUTS ON THE DESKTOP

        foreach ($shortcuts_desktop as $record) {

            if ( $record["name"] == "" ||  $record["target"] == "") {
                    array_push($message_array, '{"code":"1", "message":"'.$record["name"] . ' : Empty name or target"}');
            }
            else {

                // make a new shortcut
                $shortcut = new Shortcut;

                $shortcut->profile_id   = $profileId;
                $shortcut->name         = $record["name"];
                $shortcut->target       = $record["target"];
                $shortcut->description  = $record["description"];
                $shortcut->arguments    = $record["arguments"];
                $shortcut->workingDir   = $record["workingDir"];
                $shortcut->location     = $record["location"];

                $shortcut->save();

                array_push($message_array, '{"code":"0", "message":"'. $record["name"] . '"}');

            }

        }

        // Create message dans send response
        $message = implode (",", $message_array);

        return json_encode( array ("message" => "[" . $message . "]") );
    }






    /**
     * client get tasks by host id
     */
    public function getTasks ($hostid) {

        
        $host = Host::find($hostid);

        if ($host === NULL) {
            $code = 1;
            $message = "HOST does not exist";
        }
        else {
            $tasks = $host->tasks;

            if ( count($tasks) == 0 )  {
                $code = 1;
                $message = "NO TASK for this profile";
            }
            else {
                $code = 0;
                $message = json_encode($tasks);
            }
        }

        return json_encode( array ("code"=>$code, "message"=>$message) );

    }



    /**
     * client get tasks by hostname
     */
    public function getTasksByHostname ($hostname) {

        
        $host = Host::where("name", $hostname)->firstOrFail();

        if ($host === NULL) {
            $code = 1;
            $message = "HOST does not exist";
        }
        else {
            $tasks = $host->tasks;

            $code = 0;
            $message = json_encode($tasks);
        }

        return json_encode( array ("code"=>$code, "message"=>$message) );

    }




    /**
     * client delete a task
     */
    public function delTask (Request $request) {

        $active_task_id = $request['active_task_id'];

        // get host_id and task_id of the scheduled task (crappy)
        $active_task = DB::table('host_task')->where('id', $active_task_id)->first();
        $task_id = $active_task->task_id;
        $task = Task::find($task_id);

        // if task name = delete-schTask then delete the active task AND the task (so when we delete the task all pivot fields are deleted)
        if ($task->name == "delete-schTask") {
            $task->delete();
        }

        // else delete the active task ONLY
        else {
            DB::delete('delete from host_task where id = ?', array($active_task_id));
        }

        $code=0;
        $message=$task->name;

        return json_encode( array ("code"=>$code, "message"=>$message) );

    }




    /**
     * get all profile ids
     */
    public function getAllProfileIds () {
        $ids = Profile::all()->pluck("id");

        $code=0;

        return json_encode( array ("code"=>$code, "message"=>$ids) );

    }




    /**
     * get file association string for a specific profile
     */
    public function getFileAssoc ($profileId) {

        $profile = Profile::find($profileId);

        if ( $profile === NULL )  {
            $code = 1;
            $message = "NO profile found";
        }
        else {

            $file_assoc = $profile->fileAssoc;

            $code = 0;
            $message = json_encode($file_assoc);
        }

        return json_encode( array ("code"=>$code, "message"=>$message) );

    }



    


    public function testAuth() {
        return "wazzaaaaaa";
    }




}
