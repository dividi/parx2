<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;

use Illuminate\Support\Facades\Log;

use App\User;
use App\UserGroup;
use App\Profile;
use App\Config;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Resources\UserResource;
use App\Http\Resources\ProfileResource;
use App\Http\Resources\UsergroupResource;


use Illuminate\Pagination\Paginator;

Paginator::useBootstrap();

class UserController extends Controller
{


    // Rules to validate
    protected $rules = [
        'login' => 'required|between:3,30|unique:User,login',
        'firstName' => 'required|between:3,30',
        'lastName' => 'required|between:3,30'
    ];

    // custom messages
    protected $messages = [
        'required' => "Le champ :attribute est obligatoire.",
        'unique' => "L'utilisateur existe déjà.",
        'between' => "Le champ :attribute doit avoir entre :min et :max caractères.",
    ];





    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $total_count = User::count(); 
        $per_page = $request->get("perPage", 50);
        $users = User::with(["profiles", "usergroups"])->orderBy("login")->filter($request->all())->paginate($per_page)->withQueryString();
        return view('user.index', compact('users', 'request', 'total_count'));
    }





    /**
     * Create a new USER
     */
    public function create()
    {
        $profiles = Profile::whereKeyNot(9999)->pluck('name', 'id');
        $usergroups = UserGroup::pluck('name', 'id');

        return view('user.create', compact('profiles', 'usergroups'));
    }


    public function store(Request $request)
    {

        $this->validate($request, $this->rules, $this->messages);

        $user = new User;
        $user->login = $request['login'];
        $user->firstName = $request['firstName'];
        $user->lastName = $request['lastName'];
        $user->save();

        $usergroups = $request['usergroups'];
        if ($usergroups) {
            $user->usergroups()->sync($usergroups);
        }

        $profiles = $request['profiles'];
        if ($profiles) {
            $user->profiles()->sync($profiles);
        }

        Log::info(auth()->user()->name . " User " . $user->login . " created");

        return to_route("users.index")->with('message', "L'utilisateur <b>" . $user->firstName . ' ' . $user->lastName . '</b> a été créé.');
    }





    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('user.show', compact('user'));
    }





    /**
     * Edit the USER
     */
    public function edit($id)
    {
        $user = User::find($id);

        $profiles = Profile::whereKeyNot(9999)->pluck('name', 'id');
        $usergroups = UserGroup::pluck('name', 'id');

        return view('user.edit', compact('user', 'profiles', 'usergroups'));
    }


    public function update(Request $request, $id)
    {

        // except current $id for the validation rule (or it says the user already in the db)
        $this->rules['login'] .= "," . $id;
        $this->validate($request, $this->rules, $this->messages);

        $user = User::find($id);

        $user->login = $request['login'];
        $user->firstName = $request['firstName'];
        $user->lastName = $request['lastName'];

        $user->save();

        $usergroups = $request['usergroups'];
        if ($usergroups) {
            $user->usergroups()->sync($usergroups);
        } else {
            $user->usergroups()->detach();
        }


        $profiles = $request['profiles'];
        if ($profiles) {
            $user->profiles()->sync($profiles);
        } else {
            $user->profiles()->detach();
        }

        Log::info(auth()->user()->name . " User " . $user->login . " created");

        return to_route("users.index")->with('message', "L'utilisateur <b>" . $user->firstName . ' ' . $user->lastName . '</b> a été modifié.');
    }





    /**
     * Delete USER
     */
    public function destroyConfirmation($id)
    {

        $user = User::findOrFail($id);

        return view('user.delete', compact('user'));
    }


    public function destroy($id)
    {

        $user = User::findOrFail($id);
        $user->delete();

        Log::info(auth()->user()->name . " User " . $user->login . " deleted");

        return back()->with('message', "L'utilisateur <b>" . $user->firstName . ' ' . $user->lastName . '</b> a été supprimé.');
    }





    /**
     *  Delete selection
     */
    public function massDelete()
    {
        return view('user.massDelete');
    }


    public function massDestroy(Request $request)
    {

        $ids = explode(",", $request['ids']);

        foreach ($ids as $id) {
            $user = User::find($id);
            $user->delete();
        }

        Log::info(auth()->user()->name . " " . count($ids) . " users deleted");

        return back()->with('message', "Les utilisateurs sélectionnés ont été supprimés.");
    }





    /**
     * Import the groups / profiles of a user in another one.
     */
    public function showImport($id)
    {

        $user = User::find($id);
        $users = User::where("id", "<>", $id)->orderBy('login')->get();

        return view('user.showImport', compact('user', 'users'));
    }


    public function import(Request $request, $id)
    {

        if ($request['import_groups'] . $request['import_profiles'] == "") {
            return back()->with('message', "Il faut sélectionner au moins un type d'import");
        }

        // the source user
        $source_user = User::find($request['source_user']);

        // current user
        $current_user = User::find($id);


        if ($request['import_groups'] == 1) {
            $source_user_groups = $source_user->usergroups;
            $current_user->usergroups()->sync($source_user_groups);
        }


        if ($request['import_profiles'] == 1) {
            $source_user_profiles = $source_user->profiles;
            $current_user->profiles()->sync($source_user_profiles);
        }

        Log::info(auth()->user()->name . " Groups and profiles of user" . $source_user->login . "imported in user " . $current_user->login);

        return to_route("users.index")->with('message', "Import des profils/groupes de l'utilisateur <b>" . $source_user->firstName . " " . $source_user->lastName . "</b> dans l'utilisateur <b>" . $current_user->firstName . ' ' . $current_user->lastName . '</b>');
    }





    /**
     * Import the groups / profiles of a user in a selection.
     */
    public function showMassImport(Request $request)
    {
        $ids = $request->selection_ids;
        $count_ids = count(explode(",", $ids));
        $users = User::orderBy('login')->get();

        return view('user.showMassImport', compact('users', 'ids', 'count_ids'));
    }


    public function massImport(Request $request)
    {

        if ($request['import_groups'] . $request['import_profiles'] == "") {
            return to_route("users.index")->with('message', "Il faut sélectionner au moins un type d'import");
        }

        // the source user
        $source_user = User::find($request['source_user']);
        $source_user_groups = $source_user->usergroups;
        $source_user_profiles = $source_user->profiles;


        // selected ids
        $ids = explode(",", $request['ids']);


        // for each selected element
        foreach ($ids as $id) {

            // the source user should not be updated
            if ($id <> $source_user->id) {

                // current user
                $current_user = User::find($id);

                // import groups
                if ($request['import_groups'] == 1) {
                    $current_user->usergroups()->sync($source_user_groups);
                }

                // import profiles
                if ($request['import_profiles'] == 1) {
                    $current_user->profiles()->sync($source_user_profiles);
                }
            }
        }

        Log::info(auth()->user()->name . " Groups and profiles of user" . $source_user->login . "imported in " . count($ids) . " user(s)");

        return to_route("users.index")->with('message', "Import des profils/groupes de l'utilisateur <b>" . $source_user->firstName . " " . $source_user->lastName . "</b> vers la sélection");
    }





    /**
     * Edit the groups of the selected users
     */
    public function massEditGroups(Request $request)
    {
        $ids = $request->selection_ids;
        $count_ids = count(explode(",", $ids));
        $usergroups = UserGroup::pluck('name', 'id');

        return view('user.massEditGroups', compact('usergroups', 'ids', 'count_ids'));
    }


    public function massUpdateGroups(Request $request)
    {

        $ids = explode(",", $request['ids']);
        $usergroups = $request['usergroups'];

        foreach ($ids as $id) {
            // find the user
            $user = User::find($id);
            // drop all usergroups
            $user->usergroups()->detach();
            // assign the new ones
            $user->usergroups()->attach($usergroups);
        }

        Log::info(auth()->user()->name . " Groups of " . count($ids) . " user(s) updates");

        return to_route("users.index")->with('message', "Les groupes des utilisateurs sélectionnés ont été modifiés.");
    }





    /**
     * Edit the profiles of the selected users
     */
    public function massEditProfiles(Request $request)
    {
        $ids = $request->selection_ids;
        $count_ids = count(explode(",", $ids));
        $profiles = Profile::whereKeyNot(9999)->pluck('name', 'id');

        return view('user.massEditProfiles', compact('profiles', 'ids', 'count_ids'));
    }



    public function massUpdateProfiles(Request $request)
    {

        $ids = explode(",", $request['ids']);
        $profiles = $request['profiles'];

        foreach ($ids as $id) {
            // find the user
            $user = User::find($id);
            // drop all profiles
            $user->profiles()->detach();
            // assign the new ones
            $user->profiles()->attach($profiles);
        }

        Log::info(auth()->user()->name . " Profiles of " . count($ids) . " user(s) updates");

        return to_route("users.index")->with('message', "Les profils des hôtes sélectionnés ont été modifiés.");
    }





    /**
     * Import by CSV file
     */
    public function importCSVCreate()
    {
        return view('user.importCSV');
    }


    public function importCSVStore(Request $request)
    {
        // Rules to validate
        $rules = ['file' => 'required|max:20000|mimes:csv,txt'];

        // custom messages
        $messages = [
            'required' => "Le fichier est obligatoire.",
            'max'      => "Le fichier est trop gros.",
            'mimes'    => "Le fichier n'a pas le bon format.",
        ];

        $this->validate($request, $rules, $messages);

        // upload without verification because we are warriors
        $request['file']->move("files", "import.csv");

        // Get the group
        $groupName = $request['group'];

        // Check if group exists or create it
        $group = UserGroup::where('name', '=', $groupName)->first();

        if (!$group) {
            $group = new UserGroup;
            $group->name = $groupName;
            $group->save();
        }

        // count
        $ok_count = 0;
        $total_count = 0;

        // Open the file
        $handle = fopen('files/import.csv', "r");

        // Processing the file
        while (($row = fgetcsv($handle)) !== FALSE) {

            if (trim($row[0]) != "") {

                $total_count++;

                $cells = explode(";", $row[0]);

                $login        = trim($cells[0]);
                $firstname    = trim($cells[1]);
                $lastname     = trim($cells[2]);
                $subGroupName = trim($cells[3]);

                // No duplicates entries (unique login)
                $userExists = User::where('login', '=', $login)->first();

                if (!$userExists) {

                    $subGroup = UserGroup::where('name', '=', $subGroupName)->first();

                    if (!$subGroup) {
                        $subGroup = new UserGroup;
                        $subGroup->name = $subGroupName;
                        $subGroup->save();
                    }


                    $newUser = new User;

                    $newUser->login     = $login;
                    $newUser->firstName = $firstname;
                    $newUser->lastName  = $lastname;

                    $newUser->save();

                    $newUser->usergroups()->attach($group);
                    $newUser->usergroups()->attach($subGroup);

                    $ok_count++;
                } // if user not exists

            } // if line is empty

        } // each rows

        Log::info(auth()->user()->name . " " . $ok_count . " on " . $total_count . " user(s) imported by CSV file");

        return to_route("users.index")->with('message', "$ok_count sur $total_count utilisateurs importés.");
    }





    /**
     * Import by LDAP
     */
    public function importLDAPCreate()
    {

        // find keys or create them
        $ldap_server = Config::firstOrCreate(['key' => "ldap_server"]);
        $ldap_login = Config::firstOrCreate(['key' => "ldap_login"]);
        $ldap_user_dn = Config::firstOrCreate(['key' => "ldap_user_dn"]);
        $ldap_allowed_groups = Config::firstOrCreate(['key' => "ldap_allowed_groups"]);

        return view('user.importLDAP', compact("ldap_server", "ldap_user_dn", "ldap_login", "ldap_allowed_groups"));
    }


    private function explodeUAC($flag_to_find)
    {

        $bits = array(
            '27' => 1, '26' => 2, '25' => 4, '24' => 8, '23' => 16, '22' => 32,
            '21' => 64, '20' => 128, '19' => 256, '18' => 512, '17' => 1024,
            '16' => 2048, '15' => 4096, '14' => 8192, '13' => 16328,
            '12' => 32768, '11' => 65536, '10' => 131072, '9' => 262144,
            '8' => 524288, '7' => 1048576, '6' => 2097152, '5' => 4194304,
            '4' => 8388608, '3' => 16777216, '2' => 33554432, '1' => 67108864
        );

        $binary = str_pad(decbin($flag_to_find), count($bits), 0, STR_PAD_LEFT);

        $flags = array();

        for ($i = 0; $i < strlen($binary); $i++) {

            if ($binary[$i] == 1) {
                $flags[] = $bits[$i + 1];
            }
        }

        return $flags;
    }


    private function cleanUpEntry($entry)
    {
        $retEntry = array();
        for ($i = 0; $i < $entry['count']; $i++) {
            if (is_array($entry[$i])) {
                $subtree = $entry[$i];
                //This condition should be superfluous so just take the recursive call
                //adapted to your situation in order to increase perf.
                if (!empty($subtree['dn']) and !isset($retEntry[$subtree['dn']])) {
                    $retEntry[$subtree['dn']] = $this->cleanUpEntry($subtree);
                } else {
                    $retEntry[] = $this->cleanUpEntry($subtree);
                }
            } else {
                $attribute = $entry[$i];
                if ($entry[$attribute]['count'] == 1) {
                    $retEntry[$attribute] = $entry[$attribute][0];
                } else {
                    for ($j = 0; $j < $entry[$attribute]['count']; $j++) {
                        $retEntry[$attribute][] = $entry[$attribute][$j];
                    }
                }
            }
        }
        return $retEntry;
    }


    public function importLDAPStore(Request $request)
    {

        // Get LDAP info
        $AD_server = $request['ldap_server'];
        $ldap_dn = $request['ldap_dn_user'];
        $AD_Auth_User = $request['ldap_login'];
        $AD_Auth_PWD = $request['ldap_pwd'];
        $delete_all = $request['delete_all'];
        $AD_groups = $request['ldap_allowed_groups'] ? $request['ldap_allowed_groups'] : "";


        // update database
        Config::where('key', '=', "ldap_server")->update(['value' => $AD_server]);
        Config::where('key', '=', "ldap_user_dn")->update(['value' => $ldap_dn]);
        Config::where('key', '=', "ldap_login")->update(['value' => $AD_Auth_User]);
        Config::where('key', '=', "ldap_allowed_groups")->update(['value' => $AD_groups]);


        // get groups list (split 'n trim)
        $groups_white_list = array_map('trim', explode(';', $AD_groups));


        // LDAP connection
        @$ds = ldap_connect($AD_server);


        if ($ds) {

            ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3); // IMPORTANT

            // bind to LDAP server
            @$resource = ldap_bind($ds, $AD_Auth_User, $AD_Auth_PWD);

            if ($resource) {

                $search = "CN=*";

                @$sr = ldap_search($ds, $ldap_dn, $search);

                if ($sr) {


                    // Clean the list before importCreate
                    if ($delete_all == 1) {

                        // get all users
                        $users = User::all();

                        // delete all users
                        foreach ($users as $user) {
                            $user->delete();
                        }
                    }

                    $data = ldap_get_entries($ds, $sr);
                    $data = $this->cleanUpEntry($data);

                    // count
                    $ok_count = 0;
                    $upd_count = 0;
                    $total_count = 0;


                    //dd($data);
                    foreach ($data as $row) {

                        // get UAC parsed attributes
                        $explodeUAC = $this->explodeUAC($row["useraccountcontrol"]);


                        // filter to get only enabled users (!in_array(2, $explodeUAC) == true if enabled)
                        if ($row["objectclass"][3] == "user" && !in_array(2, $explodeUAC)) {

                            $total_count++;


                            // SAM
                            $login = $row["samaccountname"];

                            // first name
                            if (isset($row["givenname"])) {
                                $firstname = $row["givenname"];
                            } else {
                                $firstname = $login;
                            }


                            // last name
                            if (isset($row["sn"])) {
                                $lastname = $row["sn"];
                            } else {
                                $lastname = $login;
                            }





                            // No duplicates entries (unique login)
                            $userExists = User::where('login', '=', $login)->first();


                            // CREATE USER

                            if (!$userExists) {

                                $newUser = new User;

                                $newUser->login = $login;
                                $newUser->firstName = $firstname;
                                $newUser->lastName = $lastname;

                                $newUser->save();


                                // GROUPS
                                if (isset($row["memberof"])) {
                                    $groups = $row["memberof"];
                                }


                                // unset the count item
                                unset($groups["count"]);


                                foreach ($groups as $group_cn) {

                                    $split_group = explode(",", $group_cn);

                                    $groupName = str_replace("CN=", "", $split_group[0]);


                                    // if group is in white list (warning in_array is case sensitive, so... in_array(strtolower($groupName), array_map('strtolower', $groups_white_list)) from documentation)
                                    if (in_array(strtolower($groupName), array_map('strtolower', $groups_white_list)) || trim($AD_groups) == "") {

                                        // Check if group exists or create it
                                        $group = UserGroup::where('name', '=', $groupName)->first();

                                        if (!$group) {
                                            $group = new UserGroup;
                                            $group->name = $groupName;
                                            $group->save();
                                        }

                                        $newUser->usergroups()->attach($group);
                                    }
                                } // endfor groups

                                $ok_count++;
                            } // endif user exists



                            // UPDATE USER

                            else {

                                // Update usergroups


                                // clear usergroups ($userExists is the User object)
                                $userExists->usergroups()->detach();

                                // affect new groups

                                if (isset($row["memberof"])) {
                                    $groups = $row["memberof"];
                                }


                                // unset the count item
                                unset($groups["count"]);

                                foreach ($groups as $group_cn) {

                                    $split_group = explode(",", $group_cn);

                                    $groupName = str_replace("CN=", "", $split_group[0]);


                                    // if group is in white list (warning in_array is case sensitive, so... in_array(strtolower($groupName), array_map('strtolower', $groups_white_list)) from documentation)
                                    if (in_array(strtolower($groupName), array_map('strtolower', $groups_white_list)) || trim($AD_groups) == "") {

                                        // Check if group exists or create it
                                        $group = UserGroup::where('name', '=', $groupName)->first();

                                        if (!$group) {
                                            $group = new UserGroup;
                                            $group->name = $groupName;
                                            $group->save();
                                        }

                                        $userExists->usergroups()->attach($group);
                                    }
                                } // endfor groups

                                $upd_count++;
                            }
                        } // endif is a user
                    } // endfor rows of data


                } else {
                    // no search result. bad password ?
                    return back()->with('message', "Erreur Authentification LDAP");
                }
            } else {
                // no bind ?
                return back()->with('message', "Impossible de se connecter au LDAP.");
            }
        } else {
            // Error no connection (bad ip ?)
            return back()->with('message', "Erreur LDAP : " . ldap_error($ds));
        }

        // Ok !
        Log::info(auth()->user()->name . " " . $ok_count . " on " . $total_count . " user(s) imported by LDAP");

        return to_route("users.index")->with('message', "Sur $total_count utilisateurs, $ok_count création(s) et $upd_count mis à jours");
    }



    /**
     * Export to csv file
     */
    public function exportUsers() {

        $filename = 'parx_users.csv';
        $q = User::with(["profiles", "usergroups"])->get();
        $users = UserResource::collection($q);


        $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$filename",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );


        $firstrow = array('login', 'firstName', 'lastName', 'profiles', 'groups');


        $callback = function () use ($users, $firstrow) {

            $handle = fopen('php://output', 'w+');

            // first row
            fputcsv($handle, $firstrow);

            foreach ($users as $user) {

                $row['login']  = $user->login;
                $row['firstName']    = $user->firstName;
                $row['lastName']    = $user->lastName;

                // concat groups
                $group_list = [];
                foreach ($user->usergroups as $g) {
                    array_push($group_list, $g->name);
                }
                $row['groups']  = implode(";", $group_list);

                // concat profiles
                $profile_list = [];
                foreach ($user->profiles as $p) {
                    array_push($profile_list, $p->name);
                }
                $row['profiles']  = implode("|", $profile_list);

                fputcsv($handle, array($row['login'], $row['firstName'], $row['lastName'], $row['profiles'], $row['groups']));
            }

            fclose($handle);
        };

        return response()->stream($callback, 200, $headers);
    }



}
