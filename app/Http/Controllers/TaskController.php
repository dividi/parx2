<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use App\Host;
use App\HostGroup;
use App\Profile;
use App\Task;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class TaskController extends Controller {


    // Secure this controller
    public function __construct() {
        $this->middleware('auth');
    }


    // Rules to validate
    protected $rules = [
        'name' => 'required|between:3,20|unique:Task,name'
    ];

    // custom messages
    protected $messages = [
        'required' => "Le nom de la tâche est obligatoire.",
        'unique' => "La tâche existe déjà.",
        'between' => 'Le nom doit avoir entre :min et :max caractères.',
    ];






    /**
     * Display a listing of the resource.
     */
    public function index(Request $request) {
        $total_count = Task::count(); 
        $per_page = $request->get("perPage", 50);
        $tasks = Task::with("hosts")->orderBy("name")->filter($request->all())->paginate($per_page)->withQueryString();
        return view ('task.index', compact('tasks', 'request', 'total_count'));
    }



    /**
     * Show the form for creating a new resource.
     */
    public function create() {
        return view ('task.create');
    }



    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request) {

        $this->validate ($request, $this->rules, $this->messages);

        $task = new Task;

        $task->name = $request['name'];
        $task->exe = $request['exe'];
        $task->args = $request['args'];
        $task->asAdmin = $request['asAdmin'];
        $task->description = $request['description'];

        $task->save();

        Log::info(auth()->user()->name . " Task " . $task->name . " created");

        return to_route("tasks.index")->with('message', "Creation de la tâche <b>" . $task->name .'</b>.');
    }




    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id) {
        $task = Task::find($id);
        return view ('task.edit', compact('task'));
    }



    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id) {

        // except current $id for the validation rule (or it says the host already in the db) beurk
        $this->rules['name'] .= "," . $id;

        $this->validate ($request, $this->rules, $this->messages);

        $task = Task::find($id);

        $task->name = $request['name'];
        $task->exe = $request['exe'];
        $task->args = $request['args'];
        $task->asAdmin = $request['asAdmin'];
        $task->description = $request['description'];

        $task->save();

        Log::info(auth()->user()->name . " Task " . $task->name . " updated");

        return to_route("tasks.index")->with('message', "Modification de la tâche <b>" . $task->name .'</b>.');
    }



    /**
     * Display a confirmation page to delete a task
     */
    public function destroyConfirmation($id) {

        $task = Task::find($id);

        return view ('task.delete', compact('task'));
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id) {

        $task = Task::find($id);
    
        if ($task->exe == "package") {
            if ( file_exists("files/packages/".$task->name . ".zip") ) {
                unlink("files/packages/".$task->name . ".zip");
            }
        }


        if ($task->exe == "update") {
            if ( file_exists("files/UPD_".$task->args . ".zip") ) {
                unlink("files/UPD_".$task->args . ".zip");
            }
        }

        $task->delete();
        
        Log::info(auth()->user()->name . " Task " . $task->name . " deleted");
        
        return back()->with('message', "La tâche <b>" . $task->name . '</b> est supprimée.');
    }







    /**
     * Display a listing of the active tasks on a host
     */
    public function activeTasks(Request $request) {

        $total_count = Host::has("tasks")->count();
        $per_page = $request->get("perPage", 50);
        $hosts = Host::has("tasks")->with(["tasks"])->orderBy("name")->filterActiveTasks($request->all())->paginate($per_page)->withQueryString();
        $show = 0;
        return view ('task.activeTasks', compact('hosts', 'show','request', 'total_count'));
    }



    /**
     * assign hosts to a task
     */
    public function assignHosts() {
        $hosts = Host::pluck('name', 'id');
        $tasks = Task::where("name", "<>", "delete-schTask")->pluck('name', 'id');

        return view ('task.assignHosts', compact('hosts', 'tasks'));
    }


    /**
     * update assigned task to hosts
     */
    public function updateHosts(Request $request) {

        $request->validate([
            'hosts' => 'required'
        ]);


        $hosts_ids  = $request['hosts'];
        $task       = $request['task'];
        $recurtion  = $request['recurtion'];
        $schedule   = $request['schedule'];

        // remove schedule for update
        if ($task == "update") {
            $schedule = "";
        }

        foreach ($hosts_ids as $host_id) {
            Host::find($host_id)->tasks()->attach( $task, array('recurtion' => $recurtion, 'schedule'=>$schedule) );
        }

        Log::info(auth()->user()->name . " Task " . Task::find($task)->name . " assigned on " . count($hosts_ids) . " host(s)");

        return to_route('tasks.active')->with('message', 'La tâche est assignée.');

    }



    /**
     * assign hostgroup to a task
     */
    public function assignHostGroup() {
        $hostgroups = HostGroup::pluck('name', 'id')->toArray();
        $hostgroups[-1]="Tous les hôtes"; // add group all
        
        $tasks = Task::where("name", "<>", "delete-schTask")->pluck('name', 'id');

        return view ('task.assignHostGroup', compact('hostgroups', 'tasks'));
    }


    /**
     * update assigned task to hostgroup
     */
    public function updateHostGroup(Request $request) {

        $hostgroup  = $request['hostgroup'];
        $task       = $request['task'];
        $recurtion  = $request['recurtion'];
        $schedule   = $request['schedule'];

        // remove schedule for update
        if ($task == "update") {
            $schedule = "";
        }

        // all hosts selected
        if ($hostgroup == -1) {
            $hosts = Host::all();
        }
        // classic hostgroup selected
        else {
            $hosts = HostGroup::find($hostgroup)->hosts;
        }
        
        foreach ($hosts as $host) {
            $host->tasks()->attach( $task, array('recurtion' => $recurtion, 'schedule'=>$schedule) );

        }
        
        Log::info(auth()->user()->name . " Task " . Task::find($task)->name . " assigned on " . count($hosts) . " host(s)");
        return to_route('tasks.active')->with('message', 'La tâche est assignée à tous les hôtes du groupe.');

    }





    /**
     * unassign confirmation message
     */
    public function unassignHostConfirmation($task_id, $host_id, $couple_id) {

        $host = Host::find($host_id);
        $task = Task::find($task_id);

        return view ('task.unassignHost', compact('host', 'task', 'couple_id'));
    }


    /**
     * unassign a task from a host
     */
    public function unassignHost($couple_id) {


        // get host_id and task_id of the scheduled task
        $schTask = DB::table('host_task')->where('id', $couple_id)->first();

        $host_id = $schTask->host_id;
        $task_id = $schTask->task_id;
        $schedule = $schTask->schedule;

        if ( $schedule ) {

            // add a new task to delete the scheduled task
            $deleteTask = new Task;
            $deleteTask->name = 'delete-schTask';
            $deleteTask->exe = 'delete-schTask';
            $deleteTask->args = $couple_id;
            $deleteTask->asAdmin = 1;
            $deleteTask->description = 'delete scheduled Task ' .$couple_id;

            $deleteTask->save();

            // push the task in active tasks
            Host::find($host_id)->tasks()->attach( $deleteTask, array('recurtion' => 0, 'schedule'=>'') );

        }


        // Delete the scheduled task in active tasks
        DB::delete('delete from host_task where id = ?', array($couple_id));

        Log::info(auth()->user()->name . " Task " . $task_id . " unassigned");

        return to_route('tasks.active')->with('message', 'La tâche est supprimée.');
    }




    /**
     * Toggle hidden tasks
     * @TODO : merge this fn with the activeTasks one
     */
    public function toggleHiddenTasks(Request $request, $show) {
        $total_count = Host::has("tasks")->count();
        $per_page = $request->get("perPage", 50);
        $hosts = Host::has("tasks")->with(["tasks"])->orderBy("name")->filterActiveTasks($request->all())->paginate($per_page)->withQueryString();
        return view ('task.activeTasks', compact('hosts', 'show','request', 'total_count'));

    }



    /**
     * Show the form to delete the selected tasks
     */
    public function massDelete() {

        return view('task.massDelete');
    }


    /**
     * destroy the selected tasks
     */
    public function massDestroy(Request $request) {

        $ids = explode(",", $request['ids'] );

        foreach ($ids as $id) {

            // get host_id and task_id of the scheduled task
            $schTask = DB::table('host_task')->where('id', $id)->first();

            $host_id = $schTask->host_id;
            $task_id = $schTask->task_id;
            $schedule = $schTask->schedule;

            if ( $schedule ) {

                // add a new task to delete the scheduled task
                $deleteTask = new Task;
                $deleteTask->name = 'delete-schTask';
                $deleteTask->exe = 'delete-schTask';
                $deleteTask->args = $id;
                $deleteTask->asAdmin = 1;
                $deleteTask->description = 'delete scheduled Task ' .$id;

                $deleteTask->save();

                // push the task in active tasks
                Host::find($host_id)->tasks()->attach( $deleteTask, array('recurtion' => 0, 'schedule'=>'') );

            }

            // Delete the scheduled task in active tasks
            DB::delete('delete from host_task where id = ?', array($id));

        }

        Log::info(auth()->user()->name . " " . count($ids) . " active tasks deleted");

        return to_route('tasks.active')->with('message', "Les tâches actives sélectionnées ont été supprimées.");
    }






    /**
     * Display the page to upload the client
     */
    public function uploadClient() {

        return view ('task.uploadClient');
    }


    /**
     * store the new client file
     */
    public function storeClient(Request $request) {

        // Rules to validate
        $rules = ['client' => 'required|max:100000|mimes:zip'  ];

        // custom messages
        $messages = [
            'required' => "Le fichier client est obligatoire.",
            'max' => 'Le fichier est trop gros.',
            'mimes' => "Le fichier n'a pas le bon format.",
        ];

        $this->validate($request, $rules, $messages);

        $request['client']->move("files", "UPD_" . $request['version'] . ".zip");


        //Create a new task
        $task = new Task;

        $task->name = "Update " . $request['version'];
        $task->exe = "update";
        $task->args = $request['version'];
        $task->asAdmin = 1;
        $task->description = "MAJ du client version" . $request['version'];

        $task->save();


        Log::info(auth()->user()->name . " Client " . $task->name . " uploaded");

        return to_route("tasks.index")->with('message', "Le client a été uploadé. Pensez à le distribuer !");

    }



    /**
     * display upload package form
     */
    public function uploadPackage() {

        return view ('task.uploadPackage');
    }


    /**
     * store the new Package file
     */
    public function storePackage(Request $request) {

        // Rules to validate
        $rules = [
            'name' => 'required|between:3,20|unique:Task,name',
            'package' => 'required|max:100000|mimes:zip'
      ];

        // custom messages
        $messages = [
            'required' => "Le fichier package est obligatoire.",
            'max' => 'Le fichier est trop gros.',
            'mimes' => "Le fichier n'a pas le bon format.",
            'unique' => "La tâche existe déjà.",
            'between' => 'Le nom doit avoir entre :min et :max caractères.',
        ];

        $this->validate($request, $rules, $messages);

        // replace all spaces by _
        $name = str_replace(" ", "_", $request['name']);

        $request['package']->move("files/packages", $name . ".zip");


        //Create a new task
        $task = new Task;

        $task->name = $name;
        $task->exe = "package";
        $task->args = $request['args'];
        $task->asAdmin = 1;
        $task->description = "";

        $task->save();

        Log::info(auth()->user()->name . " Package " . $task->name . " uploaded");

        return to_route("tasks.index")->with('message', "Le package a été uploadé. Pensez à le distribuer !");

    }


}
