<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\User;
use App\UserGroup;
use App\Profile;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserGroupController extends Controller {

    // Secure this controller
    public function __construct() {
        $this->middleware('auth');
    }


    // Rules to validate
    protected $rules = [
        'name' => 'required|between:1,20|unique:UserGroup,name'
    ];

    // custom messages
    protected $messages = [
        'required' => "Le nom du groupe est obligatoire.",
        'unique' => "Le groupe existe déjà.",
        'between' => "Le nom du groupe doit avoir entre :min et :max caractères.",
    ];



    /**
     * Display a listing of the resource.
     */
    public function index(Request $request) {
        $total_count = UserGroup::count(); 
        $per_page = $request->get("perPage", 50);
        $usergroups = UserGroup::with(["users", "profiles"])->orderBy("name")->filter($request->all())->paginate($per_page)->withQueryString();
        return view ('usergroup.index', compact('usergroups', 'request', 'total_count'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create() {
        return view ('usergroup.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request) {

        $this->validate ($request, $this->rules, $this->messages);

        $usergroup = new UserGroup;

        $usergroup->name = $request['name'];

        $usergroup->save();

        Log::info(auth()->user()->name . " User group " . $usergroup->name . " created");

        return back()->with('message', "Creation du groupe <b>" . $usergroup->name .'</b>.');
    }

    /**
     * Display the specified resource.
     */
    public function show($id) {
        $usergroup = UserGroup::find($id);
        return view ('usergroup.show', compact('usergroup'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id) {
        $profiles = Profile::whereKeyNot(9999)->pluck('name', 'id');
        $usergroup = UserGroup::find ($id);
        return view ('usergroup.edit', compact('usergroup', 'profiles'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id) {

        // except current $id for the validation rule (or it says the host already in the db)
        $this->rules['name'] .= "," . $id;
        $this->validate($request, $this->rules, $this->messages);

        $usergroup = UserGroup::findOrFail($id);
        $usergroup->name = $request ['name'];

        $usergroup->save();

        $profiles = $request['profiles'];
        if ($profiles) {
            $usergroup->profiles()->sync($profiles);
        } else {
            $usergroup->profiles()->detach();
        }

        Log::info(auth()->user()->name . " User group " . $usergroup->name . " updated");

        return back()->with('message', "Modification du groupe d'hôtes <b>" . $usergroup->name .'</b>.');
    }



    /**
     * Display a confirmation page to delete a group
     */
    public function destroyConfirmation($id) {

        $usergroup = UserGroup::findOrFail($id);

        return view ('usergroup.delete', compact('usergroup'));
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id) {

        $usergroup = UserGroup::find($id);

        $usergroup->delete();

        Log::info(auth()->user()->name . " User group " . $usergroup->name . " deleted");

        return back()->with('message', "Le groupe <b>" . $usergroup->name . '</b> est supprimé.');
    }



    /**
     * Show the form to delete the selected usergroups
     */
    public function massDelete() {

        return view('usergroup.massDelete');
    }


    /**
     * remove the selected usergroups
     */
    public function massDestroy(Request $request) {

        $ids = explode(",", $request['ids'] );

        foreach ($ids as $id) {
            $usergroup = UserGroup::find($id);
            $usergroup->delete();
            
            Log::info(auth()->user()->name . " User group " . $usergroup->name . " mass deleted");
        }

        return back()->with('message', "Les groupes d'utilisateurs sélectionnés ont été supprimés.");
    }



    /**
     * Show the page to update profiles of the usergroup users members.
     */
    public function editProfiles($id) {

        $usergroup = UserGroup::find($id);
        $profiles = Profile::whereKeyNot(9999)->pluck('name', 'id');
        return view ('usergroup.editProfiles', compact('usergroup', 'profiles') );
    }

    /**
     * Update the profiles of the usergroup users members.
     */
    public function updateProfiles(Request $request, $id) {

        $clearProfiles = $request['clearProfiles'];
        $profiles = $request['profiles'];

        $usergroup = UserGroup::find($id);
        $users = $usergroup->users;

        // clear profiles
        if (isset($clearProfiles)) {

           foreach ($users as $user) {
                $user->profiles()->detach();
            }
        }

        // attach the selected profiles
        foreach ($users as $user) {
            $user->profiles()->attach($profiles);
        }

        Log::info(auth()->user()->name . " User group profiles " . $usergroup->name . " updated");

        return back()->with('message', "Modification des profils du groupe <b>" . $usergroup->name .'</b>.');
    }



}
