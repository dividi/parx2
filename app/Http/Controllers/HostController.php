<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Form;
use Illuminate\Support\Facades\File;

use App\Host;
use App\Profile;
use App\HostGroup;
use App\Config;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

use App\Http\Resources\HostResource;
//use App\Http\Resources\ProfileResource;
//use App\Http\Resources\HostgroupResource;

class HostController extends Controller {

    // Secure this controller
    public function __construct() {
        $this->middleware('auth');
    }


    // Rules to validate
    protected $rules = [
        'name' => 'required|between:3,20|unique:Host,name',
        'os' => 'required|between:3,50'
    ];

    // custom messages
    protected $messages = [
        'required' => "Le nom de l'hôte et le système sont obligatoires.",
        'unique' => "L'hôte existe déjà.",
        'between' => 'Le champ doit avoir entre :min et :max caractères.',
    ];




    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request) {

        $total_count = Host::count(); 
        $per_page = $request->get("perPage", 50);
        $hosts = Host::with(["profiles", "hostgroups"])->orderBy("name")->filter($request->all())->paginate($per_page)->withQueryString();
        return view('host.index', compact('hosts', 'request', 'total_count'));
    }



    /**
     * Show the form for creating a new resource.
     */
    public function create() {
        $profiles = Profile::whereKeyNot(9999)->pluck('name', 'id');
        $hostgroups = HostGroup::pluck('name', 'id');

        return view ( 'host.create', compact('profiles', 'hostgroups') );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request) {

        $this->validate ($request, $this->rules, $this->messages);

        $host = new Host;
        $host->name = $request ['name'];
        $host->os = $request ['os'];
        $host->save();


        $hostgroups = $request['hostgroups'];
        if ($hostgroups) {
            $host->hostgroups()->sync($hostgroups);
        }

        $profiles = $request['profiles'];
        if ($profiles) {
            $host->profiles()->sync($profiles);
        }

        Log::info(auth()->user()->name . " Host " . $host->name . " created");

        return to_route("hosts.index")->with('message', "Creation de l'hôte <b>" . $host->name .'</b>.');

    }

    /**
     * Display the specified resource.
     */
    public function show($id) {

        $host = Host::findOrFail($id);
        return view ('host.show', compact('host') );
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id) {

        $host = Host::findOrFail($id);

        $profiles = Profile::whereKeyNot(9999)->pluck('name', 'id');
        $hostgroups = HostGroup::pluck('name', 'id');

        return view('host.edit', compact('host', 'profiles', 'hostgroups'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id) {

        // except current $id for the validation rule (or it says the host already in the db)
        $this->rules['name'] .= "," . $id;

        $this->validate($request, $this->rules, $this->messages);

        
        $host = Host::findOrFail($id);

        $host->name = $request ['name'];
        $host->os = $request ['os'];

        $host->save();

        $hostgroups = $request['hostgroups'];
        if ($hostgroups) {
            $host->hostgroups()->sync($hostgroups);
        } else {
            $host->hostgroups()->detach();
        }


        $profiles = $request['profiles'];
        if ($profiles) {
            $host->profiles()->sync($profiles);
        } else {
            $host->profiles()->detach();
        }

        Log::info(auth()->user()->name . " Host " . $host->name . " updated");

        return to_route("hosts.index")->with('message', "Modification de l'hôte <b>" . $host->name .'</b>.');
    }







    /**
     * Display the confirmation message to delete the profile.
     */
    public function destroyConfirmation($id) {

        $host = Host::findOrFail($id);

        return view ('host.delete', compact('host'));
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id) {

        $host = Host::findOrFail($id);

        $host->delete();

        Log::info(auth()->user()->name . " Host " . $host->name . " deleted");

        return back()->with('message', "L'hôte <b>" . $host->name . '</b> a été supprimé.');
    }







    /**
     * Show the form for editing the groups of the selected hosts
     */
    public function massEditGroups(Request $request) {

        $ids = $request->selection_ids;
        $count_ids = count(explode(",", $ids));
        $hostgroups = HostGroup::pluck('name', 'id');

        return view('host.massEditGroups', compact('hostgroups', 'ids', 'count_ids'));
    }


    /**
     * Show the form for editing the groups of the selected hosts
     */
    public function massUpdateGroups(Request $request) {

        $ids = explode(",", $request['ids'] );
        $hostgroups = $request['hostgroups'];

        foreach ($ids as $id) {
            // find the host
            $host = Host::find($id);
            // drop all hostgroups
            $host->hostgroups()->detach();
            // assign the new ones
            $host->hostgroups()->attach($hostgroups);

            Log::info(auth()->user()->name . " Host Groups " . $host->name . " updated");

        }

        return to_route("hosts.index")->with('message', "Les groupes des hôtes sélectionnés ont été modifiés.");
    }









    /**
     * Show the form for editing the profiles of the selected hosts
     *
     */
    public function massEditProfiles(Request $request) {

        $ids = $request->selection_ids;
        $count_ids = count(explode(",", $ids));
        $profiles = Profile::whereKeyNot(9999)->pluck('name', 'id');

        return view('host.massEditProfiles', compact('profiles', 'ids', 'count_ids'));
    }


    /**
     * Show the form for editing the profiles of the selected hosts
     *
     * @param  Request
     * @return Response
     */
    public function massUpdateProfiles(Request $request) {

        $ids = explode(",", $request['ids'] );
        $profiles = $request['profiles'];

        foreach ($ids as $id) {
            // find the host
            $host = Host::find($id);
            // drop all profiles
            $host->profiles()->detach();
            // assign the new ones
            $host->profiles()->attach($profiles);

            Log::info(auth()->user()->name . " Host Profiles" . $host->name . " created");

        }

        return to_route("hosts.index")->with('message', "Les profils des hôtes sélectionnés ont été modifiés.");
    }




    /**
     * Show the form for editing the profiles of the selected users
     */
    public function massDelete() {

        return view('host.massDelete');
    }


    /**
     * Show the form for editing the profiles of the selected users
     */
    public function massDestroy(Request $request) {

        $ids = explode(",", $request['ids'] );

        foreach ($ids as $id) {
            $host = Host::find($id);
            $host->delete();

            Log::info(auth()->user()->name . " Host " . $host->name . " deleted");
        }

        return back()->with('message', "Les hôtes sélectionnés ont été supprimés.");
    }







    /**
     * Show the form to import hosts by file
     */
    public function importCreate() {

        return view('host.import');
    }


    /**
     * Post import file
     */
    public function importStore(Request $request) {

        // Rules to validate
        $rules = ['file' => 'required|max:20000|mimes:csv,txt'  ];

        // custom messages
        $messages = [
            'required' => "Le fichier est obligatoire.",
            'max' => 'Le fichier est trop gros.',
            'mimes' => "Le fichier n'a pas le bon format.",
        ];

        $this->validate($request, $rules, $messages);

        // upload without verification because we are warriors
        $request['file']->move("files", "import.csv");

        // count
        $ok_count = 0;
        $total_count = 0;

        // Open the file
        $handle = fopen('files/import.csv', "r");

        // Processing the file
        while(($row = fgetcsv($handle)) !== FALSE) {

            $total_count++;

            $cells = explode(";", $row[0]);

            $name = trim($cells[0]);
            $os = trim($cells[1]);
            $version = trim($cells[2]);
            $subGroupName = trim($cells[3]);

            // No duplicates entries (unique name)
            $hostExists = Host::where('name' , '=', $name)->first();

            if (!$hostExists) {

                $subGroup = HostGroup::where('name' , '=', $subGroupName)->first();

                // Create the subgroup if it not exists
                if (!$subGroup) {
                    $subGroup = New HostGroup;
                    $subGroup->name = $subGroupName;
                    $subGroup->save();
                }


                $newHost = New Host;

                $newHost->name = $name;
                $newHost->os = $os;
                $newHost->version = $version;

                $newHost->save();

                $newHost->hostgroups()->attach($subGroup);

                $ok_count++;

            }
        }

        Log::info(auth()->user()->name . " " . $total_count . " host(s) imported by CSV");

        return to_route("hosts.index")->with('message', "$ok_count sur $total_count hôtes importés.");

    }






        /**
         * Show the form with the LDAP creds 'n path
         */
        public function importLDAPCreate() {

            // find keys or create them
            $ldap_server = Config::firstOrCreate(['key' => "ldap_server"]);
            $ldap_login = Config::firstOrCreate(['key' => "ldap_login"]);
            $ldap_host_dn = Config::firstOrCreate(['key' => "ldap_host_dn"]);

            return view( 'host.importLDAP', compact("ldap_server", "ldap_host_dn", "ldap_login") );
        }


    private function cleanUpEntry($entry)
    {
        $retEntry = array();
        for ($i = 0; $i < $entry['count']; $i++) {
            if (is_array($entry[$i])) {
                $subtree = $entry[$i];
                //This condition should be superfluous so just take the recursive call
                //adapted to your situation in order to increase perf.
                if (!empty($subtree['dn']) and !isset($retEntry[$subtree['dn']])) {
                    $retEntry[$subtree['dn']] = $this->cleanUpEntry($subtree);
                } else {
                    $retEntry[] = $this->cleanUpEntry($subtree);
                }
            } else {
                $attribute = $entry[$i];
                if ($entry[$attribute]['count'] == 1) {
                    $retEntry[$attribute] = $entry[$attribute][0];
                } else {
                    for ($j = 0; $j < $entry[$attribute]['count']; $j++) {
                        $retEntry[$attribute][] = $entry[$attribute][$j];
                    }
                }
            }
        }
        return $retEntry;
    }


    // get the disgusting name, substract the ldap_host_dn, clean the host name and return an array with remaining ou
    private function get_all_groups(String $distinguished_name, String $ldap_host_dn) {

        $groups = str_ireplace(",".$ldap_host_dn, "", $distinguished_name);
        $groups = str_ireplace("OU=", "", $groups);
        $exp_groups = explode(",", $groups);
        return array_slice($exp_groups, 1); // remove the first element (CN=hostname)
    }



        /**
         * import hosts from ldap
         */
        public function importLDAPStore(Request $request) {

            // Get LDAP info
            $AD_server = $request['ldap_server'];
            $ldap_dn = $request['ldap_dn_host'];
            $AD_Auth_User = $request['ldap_login'];
            $AD_Auth_PWD = $request['ldap_pwd'];
            $delete_all = $request['delete_all'];


            // update database
            Config::where('key', '=', "ldap_server")->update(['value' => $AD_server]);
            Config::where('key', '=', "ldap_host_dn")->update(['value' => $ldap_dn]);
            Config::where('key', '=', "ldap_login")->update(['value' => $AD_Auth_User]);


            // LDAP connection
            @$ds = ldap_connect($AD_server);


            if ($ds) {

                ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3); // IMPORTANT

                // bind to LDAP server
                @$resource = ldap_bind($ds, $AD_Auth_User, $AD_Auth_PWD);

                if ($resource) {

                    $search = "CN=*";

                    @$sr = ldap_search($ds, $ldap_dn, $search);

                    if ($sr) {


                        // Clean the list before importCreate
                        if ( $delete_all == 1 ) {

                            // get all hosts
                            $hosts = Host::all();

                            // delete all hosts
                            foreach ($hosts as $host) {
                                $host->delete();
                            }
                        }

 
                        $data = ldap_get_entries($ds, $sr);
                        $data = $this->cleanUpEntry($data);

                        // count
                        $ok_count = 0;
                        $total_count = 0;


                        foreach ( $data as $row ) {

                            // is a computer
                            if ( in_array("computer", $row["objectclass"]) ) {


                                $total_count++;
                                
                                
                                $name = $row["cn"]; // the computer name
                                $groups = $this->get_all_groups($row["distinguishedname"], $ldap_dn) ; // curated groups
                                $os = isset($row["operatingsystem"]) ? $row["operatingsystem"] : "???" ;


                                // No duplicates entries (unique computer name)
                                $hostExists = Host::where('name' , '=', $name)->first();

                                // host do not exist
                                if (!$hostExists) {

                                    $newHost = new Host;

                                    $newHost->name = $name;
                                    $newHost->os = $os;
                                    $newHost->version = "N/A";

                                    $newHost->save();



                                    foreach ($groups as $groupName) {
                                        
                                        // Check if group exists or create it
                                        $group = HostGroup::where('name' , '=', $groupName)->first();
                                        
                                        if (!$group) {
                                            $group = New HostGroup;
                                            $group->name = $groupName;
                                            $group->save();
                                        }
                                        
                                        $newHost->hostgroups()->attach($group);
                                        
                                    }

                                    $ok_count++;


                                }   // end if !hostExists

                                else {

                                    // update host ?

                                } // end else hostexist



                            }   // end if is a computer
                            

                        } // endfor rows of data


                    }
                    else {
                        // no search result. bad password ?
                        return back()->with('message', "Erreur Authentification LDAP");
                    }


                } else {
                    // no bind ?
                    return back()->with('message', "Impossible de se connecter au LDAP.");
                }

            }
            else {
                // Error no connection (bad ip ?)
                return back()->with('message', "Erreur LDAP : " . ldap_error ($ds));
            }

            // Ok !

            Log::info(auth()->user()->name . " " . $ok_count ." on " . $total_count . " hosts imported from LDAP");

            return to_route("hosts.index")->with('message', "$ok_count / $total_count hôtes importés");

        } // end fn importLDAPStore




    /**
     * Export to csv file
     */
    public function exportHosts()
    {

        $filename = 'parx_hosts.csv';
        $q = Host::with(["profiles", "hostgroups"])->get();
        $hosts = HostResource::collection($q);


        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$filename",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );


        $firstrow = array('name', 'os', 'version', 'profiles', 'groups');


        $callback = function () use ($hosts, $firstrow) {

            $handle = fopen('php://output', 'w+');

            // first row
            fputcsv($handle, $firstrow);

            foreach ($hosts as $host) {

                $row['name']  = $host->name;
                $row['os']    = $host->os;
                $row['version']    = $host->version;

                // concat groups
                $group_list = [];
                foreach ($host->hostgroups as $g) {
                    array_push($group_list, $g->name);
                }
                $row['groups']  = implode(";", $group_list);

                // concat profiles
                $profile_list = [];
                foreach ($host->profiles as $p) {
                    array_push($profile_list, $p->name);
                }
                $row['profiles']  = implode("|", $profile_list);

                fputcsv($handle, array($row['name'], $row['os'], $row['version'], $row['profiles'], $row['groups']));
            }

            fclose($handle);
        };

        return response()->stream($callback, 200, $headers);
    }




    /**
     * Show the form for editing the config file of a selection
     */
    public function massConfig(Request $request) {
        $ids = $request->selection_ids;
        $selected_hosts = Host::find(explode(",", $ids))->pluck("name");
        return view('host.massConfig', compact('ids', 'selected_hosts'));
    }


    /**
     * Create a config file per selected host 
     */
    public function massCreateConfig(Request $request) {

        $ids = explode(",", $request['ids'] );

        foreach ($ids as $id) {
            // find the host
            $host = Host::find($id);
           
            $destinationFile = public_path()."/files/config/" . $host->name . '.config';    // choose the path
            File::put($destinationFile, $request->config);  // create the file

            Log::info(auth()->user()->name . " Host " . $host->name . " config file created");
        }

        return to_route("hosts.index")->with('message', "Les fichiers de config ont été créés.");
    }



}
