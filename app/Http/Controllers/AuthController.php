<?php

    /**
     * Manage web authentification + crud for user management
     */


namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Login;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;


class AuthController extends Controller
{

    // Rules to validate
    protected $rules = [
        'name' => 'required|between:3,100|unique:Login,name',
        'email' => 'required|email|between:5,255|unique:Login,email',
        'password' => 'required|string|between:8,255|confirmed'
    ];


    // custom messages
    protected $messages = [
        'required' => "Le champ :attribute est obligatoire.",
        'unique' => "Le compte ou le mail existe déjà.",
        'between' => "Le champ :attribute doit avoir entre :min et :max caractères.",
    ];



    /**
     * Display a listing of all logins
     *
     */
    public function index(Request $request)
    {
        $total_count = Login::count(); 
        $per_page = $request->get("perPage", 50);

        $logins = Login::where("name", "<>", "parxshell")->orderBy('name')->filter($request->all())->paginate($per_page)->withQueryString();
        return view('auth.index', compact('logins', 'request', 'total_count'));
    }



    /**
     * Create a new login after a valid registration.
     *
     * @param  array  $data
     */
    public function create()
    {
        return view('auth.create');
    }


/**
 * Create a new user
 * 
 * @param Request request The request object.
 * 
 * @return 
 */
    protected function store(Request $request)
    {

        $this->validate($request, $this->rules, $this->messages);

        Login::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);

        return to_route("auth.index")->with('message', "Le compte <b>" . $request['name'] . '</b> a été créé.');
    }

    /**
     * Remove the specified login from storage.
     */
    public function destroyConfirmation($id)
    {

        $login = Login::find($id);
        return view('auth.delete', compact('login'));
    }

    public function destroy($id)
    {

        $login = Login::find($id);

        $login->delete();

        return back()->with('message', 'Le login <b>' . $login->name . '</b> est supprimé.');
    }




    // resgister an account for the api
    public function registerAPI () {

        $name = 'parxshell';
        $email = 'parxshell@parx.org';
        $password = 'passwordforparxshell';
        $token_name = $name . "-token";


        $login = Login::where('name', $name)->first();

        // delete token and parxshell login
        if ($login) {
            $login->tokens()->delete();
            $login->delete();
        }

        // create a parxshell account
        $login = Login::create([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password)
        ]);

        // create a token
        $token = $login->createToken($token_name)->plainTextToken;
        
        // display the token
        $response = [
            'login' => $login,
            'token' => $token
        ];
        
        return response($response, 201);

    }






    /*   
    // Rules to validate
    protected $rules = [
        'username'    => 'required|min:3',
        'password' => 'required|min:6'
    ];

    // custom messages
    protected $messages = [
        'required' => "Le champ :attribute est obligatoire.",
        'min' => "Le champ :attribute doit avoir minimum :min caractères.",
    ];


    // show login page
    public function showLogin () {
        return view ('auth.login');
    }



    // login user
    public function doLogin(Request $request) {


        // run the validation rules on the inputs from the form
        $this->validate ($request, $this->rules, $this->messages);

        // create our user data for the authentication
        $userdata = array(
            'username'  => Input::get('username'),
            'password'  => Input::get('password')
        );

        // attempt to do the login
        if (Auth::attempt($userdata)) {

            // validation successful!
            // redirect them to the secure section or whatever
            return Redirect::to('/');

        } else {        

            // validation not successful, send back to form 
            return Redirect::to('login')->with('message', 'Login ou mot de passe invalide...');

        }

    }






    // show change password page
    public function showChangePassword () {
        return view ('auth.changePassword');
    }




    // do change password
    public function doChangePassword () {
        return view ('auth.changePassword');
    }


    // do change password
    public function updatePassword (Request $request) {
       
        // create our user data for the authentication
        $userdata = array(
            'username'  => "parx",
            'password'  => Input::get('oldPwd')
        );

        // attempt to do the login
        if (Auth::attempt($userdata)) {

            // validation successful!
            
            // update password
            $password = Input::get('newPwd');

            DB::table('Auth')
            ->where('username', "parx")
            ->update(array('password' => \Hash::make($password) ));

            // redirect
            return Redirect::to('/')->with('message', 'Mot de passe modifié');

        } else {        

            // validation not successful, send back to form 
            return Redirect::to('/')->with('message', 'Ancien mot de passe invalide...');

        }





    }
*/
}
