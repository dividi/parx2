<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;

use App\Profile;
use App\Parameter;
use App\Shortcut;
use App\Printer;
use App\Share;
use Illuminate\Support\Facades\Log;

use Illuminate\Support\Facades\File;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class ProfileController extends Controller {



    // Rules to validate
    protected $rules = [
        'name' => 'required|between:3,50|unique:Profile,name'
    ];

    // custom messages
    protected $messages = [
        'required' => 'Le nom du profil est obligatoire.',
        'unique' => 'Le profil existe déjà.',
        'between' => 'Le nom du profil doit avoir entre :min et :max caractères.',
    ];




    /**
     * Display a listing of all profiles
     *
     * @return
     */
    public function index(Request $request) {
        $total_count = Profile::whereKeyNot(9999)->count();         
        $per_page = $request->get("perPage", 50);
        $profiles = Profile::with(["hosts", "users"])->whereKeyNot(9999)->orderBy('name')->filter($request->all())->paginate($per_page)->withQueryString();
        return view ('profile.index', compact('profiles', 'request', 'total_count'));
    }



    /**
     * Create a new profile
     */
    public function create() {
        return view ('profile.create');
    }


    public function store(Request $request) {

        $this->validate ($request, $this->rules, $this->messages);

        $profile = new Profile;

        $profile->name = $request ['name'];
        $profile->matchType = $request ['matchType'];
        $profile->revision = now(); // update profile revision (timestamp)

        $profile->save();

        Log::info(auth()->user()->name . " creates profile " . $profile->name);

        return to_route("profiles.index")->with('message', 'Creation du profil <b>' . $profile->name .'</b>.');
    }




    /**
     * Display the specified resource.
     */
    public function show($id) {

        $profile = Profile::findOrFail($id);
        return view('profile.show', compact('profile'));
    }




    /**
     * Edit the specified resource in storage.
     */
    public function edit($id) {

        $profile = Profile::findOrFail($id);
        return view('profile.edit', compact('profile'));
    }


    public function update(Request $request, $id) {

        // except current $id for the validation rule (or it says the host already in the db)
        $this->rules['name'] .= "," . $id;

        $this->validate($request, $this->rules, $this->messages);

        $profile = Profile::findOrFail($id);
        $profile->name = $request ['name'];
        $profile->matchType = $request ['matchType'];

        $profile->save();

        Log::info(auth()->user()->name . " updates profile " . $profile->name);

        return to_route("profiles.index")->with('message', 'Modification du profil <b>' . $profile->name .'</b>.');
    }







    /**
     * Delete the profile.
     */
    public function destroyConfirmation($id) {

        $profile = Profile::find($id);

        return view ('profile.delete', compact('profile'));
    }


    public function destroy($id) {

        if ($id != 9999) {
            $profile = Profile::find($id);
        
            $profile->delete();
            
            Log::info(auth()->user()->name . " Profile " . $profile->name . " deleted");
    
            return back()->with('message', 'Le profil <b>' . $profile->name . '</b> est supprimé.');
        }
        else {
            return back()->with('message', 'Le profil <b>DEFAULT</b> ne doit pas être supprimé.');
        }
        
    }







    /**
     * import data from another profile
     */
    public function showImport($id) {

        $profile = Profile::find($id);
        $other_profiles = Profile::where("id", "<>", $id)->whereKeyNot(9999)->pluck('name', 'id');

        return view ('profile.import', compact('profile', 'other_profiles'));
    }


    public function import(Request $request, $id) {

        // current profile
        $current_profile = Profile::find($id);

        echo "current profile : " . $current_profile->name;

        echo "<br>";

        // profile to import from
        $source_profile = Profile::find( $request['other_profiles'] );

        echo "source profile : " . $source_profile->name;

        echo "<br>";



        // Import shortcuts
        if ( $request['import_shortcuts'] ) {

            // delete shortcuts from current profile
            foreach ($current_profile->shortcuts as $shortcut) {
                $shortcut->delete();
            }

            // attach shortcuts from source profile to current profile
            foreach ($source_profile->shortcuts as $shortcut) {

                $new_shortcut = new Shortcut;

                $new_shortcut->profile_id = $id;
                $new_shortcut->name = $shortcut->name;
                $new_shortcut->target = $shortcut->target;
                $new_shortcut->arguments = $shortcut->arguments;
                $new_shortcut->workingDir = $shortcut->workingDir;
                $new_shortcut->description = $shortcut->description;
                $new_shortcut->location = $shortcut->location;

                $new_shortcut->save();

            }

            Log::info(auth()->user()->name . " Shortcuts from profile " . $source_profile->name . " to profile " . $current_profile . " imported");

        }



        // Import printers
        if ( $request['import_printers'] ) {

            // unassign all printers from current profile
            $current_profile->printers()->detach();

            // assign printers from source profile to current profile
            foreach ($source_profile->printers as $printer) {
                $current_profile->printers()->attach($printer, array('default' => $printer->pivot->default));
            }

            Log::info(auth()->user()->name . " Printers from profile " . $source_profile->name . " to profile " . $current_profile . " imported");
        }


        // Import shares
        if ( $request['import_shares'] ) {

            // unassign all printers from current profile
            $current_profile->shares()->detach();

            // assign printers from source profile to current profile
            foreach ($source_profile->shares as $share) {
                $current_profile->shares()->attach($share, array('shareLetter' => $share->pivot->shareLetter));
            }

            Log::info(auth()->user()->name . " Shares from profile " . $source_profile->name . " to profile " . $current_profile . " imported");

        }


        // Import parameters
        if ( $request['import_options'] ) {

            // unassign all printers from current profile
            $current_profile->parameters()->detach();

            // assign printers from source profile to current profile
            foreach ($source_profile->parameters as $parameter) {
                $current_profile->parameters()->attach($parameter, array('parameterValue' => $parameter->pivot->parameterValue));
            }

            Log::info(auth()->user()->name . " Options from profile " . $source_profile->name . " to profile " . $current_profile . " imported");

        }


        // Import file association
        if ( $request['import_file_associations'] ) {

            $current_profile->fileAssoc = $source_profile->fileAssoc;
            $current_profile->save();

            Log::info(auth()->user()->name . " File associations from profile " . $source_profile->name . " to profile " . $current_profile . " imported");
        }

        return to_route("profiles.index")->with('message', 'Le profil <b>' . $source_profile->name . '</b> a été importé dans le profil ' . $current_profile->name);

    }



    /**
     * import data from another profile into selected profiles
     * @TODO : exclude selected profiles from other_profiles
     */
    public function showMassImport(Request $request) {
        
        $ids = $request->selection_ids;
        $selected_profiles = Profile::find(explode(",", $ids))->pluck("name");

        $other_profiles = Profile::orderBy('name')->whereKeyNot(9999)->pluck('name', 'id');
        return view ('profile.showMassImport', compact('other_profiles', 'selected_profiles', 'ids'));
    }


    public function massImport(Request $request) {


        // profile to import from
        $source_profile = Profile::find( $request['other_profiles'] );

        // selected ids
        $ids = explode(",", $request['ids'] );


        // for each selected element
        foreach ($ids as $id) {
            
            // the source profile should not be updated
            if ($id <> $source_profile->id) {

                // current profile
                $current_profile = Profile::find($id);


                // Import shortcuts
                if ( $request['import_shortcuts'] ) {

                    // delete shortcuts from current profile
                    foreach ($current_profile->shortcuts as $shortcut) {
                        $shortcut->delete();
                    }

                    // attach shortcuts from source profile to current profile
                    foreach ($source_profile->shortcuts as $shortcut) {

                        $new_shortcut = new Shortcut;

                        $new_shortcut->profile_id = $id;
                        $new_shortcut->name = $shortcut->name;
                        $new_shortcut->target = $shortcut->target;
                        $new_shortcut->arguments = $shortcut->arguments;
                        $new_shortcut->workingDir = $shortcut->workingDir;
                        $new_shortcut->description = $shortcut->description;
                        $new_shortcut->location = $shortcut->location;

                        $new_shortcut->save();
                    }

                    Log::info(auth()->user()->name . " Shortcuts from profile " . $source_profile->name . " to profile " . $current_profile . " imported");
                }


                // Import printers
                if ( $request['import_printers'] ) {

                    // unassign all printers from current profile
                    $current_profile->printers()->detach();

                    // assign printers from source profile to current profile
                    foreach ($source_profile->printers as $printer) {
                        $current_profile->printers()->attach($printer, array('default' => $printer->pivot->default));
                    }

                    Log::info(auth()->user()->name . " Printers from profile " . $source_profile->name . " to profile " . $current_profile . " imported");

                }


                // Import shares
                if ( $request['import_shares'] ) {

                    // unassign all printers from current profile
                    $current_profile->shares()->detach();

                    // assign printers from source profile to current profile
                    foreach ($source_profile->shares as $share) {
                        $current_profile->shares()->attach($share, array('shareLetter' => $share->pivot->shareLetter));
                    }

                    Log::info(auth()->user()->name . " Shares from profile " . $source_profile->name . " to profile " . $current_profile . " imported");
                }


                // Import parameters
                if ( $request['import_options'] ) {

                    // unassign all printers from current profile
                    $current_profile->parameters()->detach();

                    // assign printers from source profile to current profile
                    foreach ($source_profile->parameters as $parameter) {
                        $current_profile->parameters()->attach($parameter, array('parameterValue' => $parameter->pivot->parameterValue));
                    }

                    Log::info(auth()->user()->name . " Options from profile " . $source_profile->name . " to profile " . $current_profile . " imported");

                }


                // Import file association
                if ( $request['import_file_associations'] ) {

                    $current_profile->fileAssoc = $source_profile->fileAssoc;
                    $current_profile->save();

                    Log::info(auth()->user()->name . " File associations from profile " . $source_profile->name . " to profile " . $current_profile . " imported");

                }

            }

        }

        return back()->with('message', 'Les profil <b>' . $source_profile->name . '</b> a été importé dans les profils sélectionnés');
    }




    /**
     * Display the page to upload the wallpaper
     */
    public function uploadWallpaper($id) {

        $profile = Profile::find($id);
        return view ('profile.uploadWallpaper', compact("id", "profile"));
    }


    /**
     * store the new wallpaper file
     */
    public function storeWallpaper(Request $request, $id) {

        // Rules to validate
        $rules = ['wallpaper' => 'max:5000|mimes:jpg'  ];

        // custom messages
        $messages = [
            'max' => 'Le fichier est trop gros.',
            'mimes' => "Le fichier n'a pas le bon format (jpg).",
        ];

        $this->validate($request, $rules, $messages);

        if ( !is_null($request['wallpaper']) ) {
            $request['wallpaper']->move("wallpapers", $id . ".jpg");

            Log::info(auth()->user()->name . " Wallpaper " . $id . " uploaded");

            return to_route("profiles.index")->with('message', "Le fond d'écran a été uploadé.");
        }
        else {
            unlink("wallpapers/".$id . ".jpg");

            Log::info(auth()->user()->name . " Wallpaper " . $id . " removed");

            return to_route("profiles.index")->with('message', "Le fond d'écran a été supprimé.");
        }

    }






    /**
     * Display the page to upload the wallpaper on multiple profiles
     */
    public function massUploadWallpaper(Request $request) {

        $ids = $request->selection_ids;
        $selected_profiles = Profile::find(explode(",", $ids))->pluck("name");
        return view ('profile.massUploadWallpaper', compact('selected_profiles', 'ids'));



    }


    /**
     * store the new wallpaper file for multiple profiles
     */
    public function massStoreWallpaper(Request $request) {

        // Rules to validate
        $rules = ['wallpaper' => 'max:5000|mimes:jpg'  ];

        // custom messages
        $messages = [
            'max' => 'Le fichier est trop gros.',
            'mimes' => "Le fichier n'a pas le bon format (jpg).",
        ];

        $this->validate($request, $rules, $messages);

        $ids = explode(",", $request['ids'] );

        if ( !is_null($request['wallpaper']) ) {

            // copy wp from temp to wallpapers
            $f = $request->file('wallpaper')->move("wallpapers", "w.jpg");

            // duplicate wp
            foreach ($ids as $profile_id) {
                File::copy($f, "wallpapers/". $profile_id .".jpg");                
            }

            // delete file after upload
            unlink("wallpapers/w.jpg");

            Log::info(auth()->user()->name . " Wallpaper " . $profile_id . " uploaded");
        
            return to_route("profiles.index")->with('message', "Le fond d'écran a été uploadé sur les profils sélectionnés.");
        }
        else {

            foreach ($ids as $profile_id) {
                if ( file_exists("wallpapers/".$profile_id . ".jpg") ) {
                    unlink("wallpapers/".$profile_id . ".jpg");
                }

                Log::info(auth()->user()->name . " Wallpaper " . $profile_id . " removed");
        
            }

            return to_route("profiles.index")->with('message', "Le fond d'écran a été supprimé des profils sélectionnés.");
        }


    }







    /**
     * Display the page to choose file association
     */
    public function chooseFileAssociation($id) {

        $profile = Profile::findOrFail($id);

        $json = json_decode($profile['fileAssoc']);
  
        return view ('profile.chooseFileAssociation', compact('profile', 'id', 'json'));
    }


    /**
     * store the file association selection
     */
    public function storeFileAssociation(Request $request, $id) {
        
        $obj = [
            'browser'   => $request["browser"],
            'pdf'       => $request["pdf"],
            'video'     => $request["video"],
            'music'     => $request["music"],
            'office'    => $request["office"],
            'image'     => $request["image"]
        ];


        $profile = Profile::findOrFail($id);
        $profile->fileAssoc = json_encode($obj);
        
        $profile->save();

        Log::info(auth()->user()->name . " File associations for profile " . $profile->name . " changed");

        return back()->with('message', 'Modification des associations de fichiers pour <b>' . $profile->name .'</b>.');

    }






}
