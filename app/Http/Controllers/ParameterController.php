<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Illuminate\Support\Facades\Log;

use App\Profile;
use App\Parameter;

use App\Http\Requests;
use App\Http\Controllers\Controller;



class ParameterController extends Controller {

    // Secure this controller
    public function __construct() {
        $this->middleware('auth');
    }


    /**
     * Display the parameters of the profile.
     * Add parameters if the profile is not up to date
     */
    public function index($id) {

        $profile = Profile::findOrFail($id);
        $all_parameters = Parameter::pluck("id")->toArray();    # get all parameters in Parameter table (needed ? should be replaced with the default_parameters)
        $current_parameters = $profile->parameters->pluck("id")->toArray(); # get all parameter of the specific profile

        $default_parameters = 
        [
            1 => ["proxyUrlIE",""],
            2 => ["proxyUrlFF","",],
            3 => ["hiddenDrives",""],
            4 => ["hideControlPanel","0"],
            5 => ["hideRunMenu","0"],
            6 => ["disableRegistry","0"],
            7 => ["bindHomeFolder",""],
            8 => ["disallowRun",""],
            9 => ["changeHomePageIE",""],
            10 => ["changeHomePageFF",""],
            11 => ["lockTimer","50"],
            12 => ["monitorSleep","30"],
            13 => ["systemSleep","60"],
            16 => ["shutdownTime",""],
            17 => ["proxyBypassIE",""],
            18 => ["proxyBypassFF",""],
            19 => ["proxyTypeFF",""],
            22 => ["proxyScriptIE",""],
            23 => ["proxyScriptFF",""],
            24 => ["disableDesktop","0"],
            25 => ["disableTaskManager","0"],
            26 => ["autoEndTasks","0"],
            27 => ["disableCMD","0"],
        ];


        // append new parameters if not in the list
        foreach ( array_keys($default_parameters) as $parameter_id ) {

            # the parameter in default_parameters list is not in the Parameter table. Create it and attach it to the current profile
            if (!in_array($parameter_id, $all_parameters)) {
                $param = new Parameter;
                $param->id = $parameter_id;
                $param->name = $default_parameters[$parameter_id][0];
                $param->defaultValue = $default_parameters[$parameter_id][1];
                $param->save();
            }

            // the current parameter in the default parameters list is not attached to the current profile
            if (!in_array($parameter_id, $current_parameters)) {
                $profile->parameters()->attach( $parameter_id , array("parameterValue" => $default_parameters[$parameter_id][1]) ) ;
            }
        }

        // reload profile
        $profile = Profile::findOrFail($id);


        // recreate a fake profile to draw (big workaround to not use the Parameters in DB)
        foreach ($profile->parameters as $key => $value) {
            $k = $value->id;
            $v = $value->pivot->parameterValue;
            $parameters[$k] = $v;
        }

        $p = [
            "id" => $profile->id,
            "name" => $profile->name,
            "parameters" => $parameters,
        ];

        return view ('profile.parameters', compact("p") );
    }



    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, $id) {

        $profile = Profile::findOrFail($id);

        $parameters = $request->param;


        foreach ( $parameters as $key => $value) {
            $profile->parameters()->updateExistingPivot($key, array("parameterValue"=> $value) ) ;
        }


        // update profile revision (timestamp)
        $profile->revision = now();
        $profile->save();

        Log::info(auth()->user()->name . " Profile " . $profile->name . " options updated");

        return redirect()->route('profiles.index')->with('message', 'Paramètres du profil <b>' . $profile->name .' sauvegardés.</b>.');
    }





    /**
     * Display the parameters to update a selection of profiles.
     */
    public function showMassEdit(Request $request) {

        $ids = $request->selection_ids;

        return view ('profile.massParameters', compact("ids"));
    }



    public function updateProfilesParameters(Request $request) {

        // parameters values
        $param_values = $request->param;

        // checks on
        $checks = $request->check;

        // at least one parameter is checked 
        if ( $checks != null ) {

            $all_parameters = Parameter::pluck("id")->toArray();    # get all parameters in Parameter table (needed ? should be replaced with the default_parameters)

            $default_parameters = 
            [
                1 => ["proxyUrlIE",""],
                2 => ["proxyUrlFF","",],
                3 => ["hiddenDrives",""],
                4 => ["hideControlPanel","0"],
                5 => ["hideRunMenu","0"],
                6 => ["disableRegistry","0"],
                7 => ["bindHomeFolder",""],
                8 => ["disallowRun",""],
                9 => ["changeHomePageIE",""],
                10 => ["changeHomePageFF",""],
                11 => ["lockTimer","50"],
                12 => ["monitorSleep","30"],
                13 => ["systemSleep","60"],
                16 => ["shutdownTime",""],
                17 => ["proxyBypassIE",""],
                18 => ["proxyBypassFF",""],
                19 => ["proxyTypeFF",""],
                22 => ["proxyScriptIE",""],
                23 => ["proxyScriptFF",""],
                24 => ["disableDesktop","0"],
                25 => ["disableTaskManager","0"],
                26 => ["autoEndTasks","0"],
                27 => ["disableCMD","0"],
            ];


            $profiles = explode(",", $request->profile_ids);

            foreach ($profiles as $profile_id) {

                // load the profile
                $profile = Profile::findOrFail($profile_id);


                // first, we have to append all missing parameters (in cas the user wants to mass edit profiles without opening 1 time the parameters page once )
                $current_parameters = $profile->parameters->pluck("id")->toArray(); # get all parameter of the specific profile

                // append new parameters if not in the list
                foreach ( array_keys($default_parameters) as $parameter_id ) {

                    # the parameter in default_parameters list is not in the Parameter table. Create it and attach it to the current profile
                    if (!in_array($parameter_id, $all_parameters)) {
                        $param = new Parameter;
                        $param->id = $parameter_id;
                        $param->name = $default_parameters[$parameter_id][0];
                        $param->defaultValue = $default_parameters[$parameter_id][1];
                        $param->save();
                    }

                    // the current parameter in the default parameters list is not attached to the current profile
                    if (!in_array($parameter_id, $current_parameters)) {
                        $profile->parameters()->attach( $parameter_id , array("parameterValue" => $default_parameters[$parameter_id][1]) ) ;
                    }
                }

                // reload profile
                $profile = Profile::findOrFail($profile_id);
                

                // apply selected parameters
                foreach ($checks as $key => $param_id) {

                    // param value cannot be null
                    $param_value = $param_values[$key];
                    if ( !isset($param_value) ) {
                        $param_value = "";
                    }
                    
                    $profile->parameters()->updateExistingPivot($key, array("parameterValue"=> $param_value) ) ;
            
                    // update profile revision (timestamp)
                    $profile->revision = now();
                    $profile->save();

                }
            }


            Log::info(auth()->user()->name . count($checks) . " options mises à jour sur " . count($profiles) . " profils" );

            return redirect()->route('profiles.index')->with('message', count($checks) . " options mises à jour sur " . count($profiles) . " profils" );
        }
        else {

            return redirect()->route('profiles.index')->with('message', 'Aucune option sélectionnée');
        }


    }














}

