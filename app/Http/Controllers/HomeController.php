<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;
use App\Host;
use App\HostGroup;
use App\User;
use App\Profile;
use App\UserGroup;
use App\Printer;
use App\Share;


use Illuminate\Http\Request;

class HomeController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     */
    public function stats() {

        $hosts_count    = Host::get()->count();
        $profiles_count = Profile::whereKeyNot(9999)->count();
        $users_count    = User::get()->count();
        $usergroups_count = UserGroup::get()->count();
        $hostgroups_count = HostGroup::get()->count();
        $printers_count = Printer::get()->count();
        $shares_count = Share::get()->count();
        $active_tasks_count = DB::table('host_task')->count();

        $hosts_without_profile_count = Host::withCount('profiles')->having('profiles_count', '=', 0)->get()->count();
        $users_without_profile_count = User::withCount('profiles')->having('profiles_count', '=', 0)->get()->count();
        
        // Called the named view "host.index" with parameter "hosts" (aka the var)
        return view ('welcome', compact(
            'hosts_count', 
            'profiles_count', 
            'users_count', 
            'usergroups_count', 
            'hostgroups_count', 
            'printers_count', 
            'shares_count',
            'active_tasks_count',
            'hosts_without_profile_count',
            'users_without_profile_count',
        ));
    }


}
