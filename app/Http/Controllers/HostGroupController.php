<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;

use App\Host;
use App\HostGroup;
use App\Profile;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Log;

class HostGroupController extends Controller {


    // Rules to validate
    protected $rules = [
        'name' => 'required|between:1,100|unique:HostGroup,name'
    ];

    // custom messages
    protected $messages = [
        'required' => "Le nom du groupe est obligatoire.",
        'unique' => "Le groupe existe déjà.",
        'between' => "Le nom du groupe doit avoir entre :min et :max caractères.",
    ];




    /**
     * display the list of hostgroups
     */
    public function index(Request $request) {
        $total_count = HostGroup::count(); 
        $per_page = $request->get("perPage", 50);
        $hostgroups = HostGroup::with(["hosts", "profiles"])->orderBy("name")->filter($request->all())->paginate($per_page)->withQueryString();
        return view ('hostgroup.index', compact('hostgroups', 'request', 'total_count'));
    }



    /**
     * display info page of a hostgroup
     */
    public function show($id) {

        $hostgroup = HostGroup::findOrFail($id);
        return view('hostgroup.show', compact('hostgroup'));
    }


    /**
     * Create a hostgroup
     */
    public function create() {
        return view ('hostgroup.create');
    }


    public function store(Request $request) {

        $this->validate ($request, $this->rules, $this->messages);

        $hostgroup = new HostGroup;

        $hostgroup->name = $request ['name'];

        $hostgroup->save();

        Log::info(auth()->user()->name . " Hostgroup " . $hostgroup->name . " created");

        return to_route("hostgroups.index")->with('message', "Creation du groupe <b>" . $hostgroup->name .'</b>.');
    }




    
    /**
     * Edit the hostgroup
     */
    public function edit($id) {

        $profiles = Profile::whereKeyNot(9999)->pluck('name', 'id');
        $hostgroup = HostGroup::findOrFail($id);
        return view ('hostgroup.edit', compact('hostgroup', 'profiles'));
    }


    public function update(Request $request, $id) {

        // except current $id for the validation rule (or it says the host already in the db)
        $this->rules['name'] .= "," . $id;
        $this->validate($request, $this->rules, $this->messages);


        $hostgroup = HostGroup::findOrFail($id);
        $hostgroup->name = $request['name'];

        $hostgroup->save();


        $profiles = $request['profiles'];
        if ($profiles) {
            $hostgroup->profiles()->sync($profiles);
        } else {
            $hostgroup->profiles()->detach();
        }



        Log::info(auth()->user()->name . " Hostgroup " . $hostgroup->name . " updated");

        return to_route("hostgroups.index")->with('message', "Modification du groupe <b>" . $hostgroup->name .'</b>.');
    }





    /**
     * Delete a hostgroup
     */
    public function destroyConfirmation($id) {

        $hostgroup = HostGroup::findOrFail($id);

        return view ('hostgroup.delete', compact('hostgroup'));
    }


    public function destroy(Request $request, $id) {

        $deleteHosts = $request['deleteHosts'];
        $hostgroup = HostGroup::findOrFail($id);

        // delete attached hosts
        if (isset($deleteHosts)) {

           foreach ($hostgroup->hosts as $host) {
                $host->delete();
            }

            // delete the group
            $hostgroup->delete();

            Log::info(auth()->user()->name . " Hostgroup " . $hostgroup->name . " and all hosts inside deleted");

            return back()->with('message', "Le groupe <b>" . $hostgroup->name . '</b> est supprimé, ainsi que tous les hôtes rattachés.');

        }
        else {

            // delete the group
            $hostgroup->delete();

            Log::info(auth()->user()->name . " Hostgroup " . $hostgroup->name . " deleted");

            return back()->with('message', "Le groupe <b>" . $hostgroup->name . '</b> est supprimé.');
        }



    }





    /**
     * Update the profiles of the hostgroup hosts members.
     */
    public function editProfiles($id) {

        $hostgroup = HostGroup::find($id);
        $profiles = Profile::whereKeyNot(9999)->pluck('name', 'id');
        return view ('hostgroup.editProfiles', compact('hostgroup', 'profiles') );
    }


    public function updateProfiles(Request $request, $id) {

        $clearProfiles = $request['clearProfiles'];
        $profiles = $request['profiles'];

        $hostgroup = HostGroup::find($id);
        $hosts = $hostgroup->hosts;

        // clear profiles
        if (isset($clearProfiles)) {

           foreach ($hosts as $host) {
                $host->profiles()->detach();
            }
        }


        // attach the selected profiles
        foreach ($hosts as $host) {
            $host->profiles()->attach($profiles);
        }

        Log::info(auth()->user()->name . " Hostgroup " . $hostgroup->name . " profiles created");

        return to_route("hostgroups.index")->with('message', "Modification des profils du groupe <b>" . $hostgroup->name .'</b>.');
    }
}
