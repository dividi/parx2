<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use App\Shortcut;
use App\Profile;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ShortcutController extends Controller {

    // Secure this controller
    public function __construct() {
        $this->middleware('auth');
    }

    // Rules to validate
    protected $rules = [
        'name' => 'required|between:3,100',
        'target' => 'required|between:3,1000'
    ];

    // custom messages
    protected $messages = [
        'required' => 'Ce champ est obligatoire.',
        'unique' => 'Le raccourci existe déjà.',
        'between' => 'Le champ :attribute doit avoir entre :min et :max caractères.',
    ];




    
    /**
     * Display a listing of the shortcuts of a profile.
     */
    public function index($id, Request $request) {
        
        $total_count = Shortcut::count();
        $per_page = $request->get("perPage", 50);
        
        $profile = Profile::find($id);
        $shortcuts = Shortcut::with(["profile"])->where("profile_id", $id)->orderBy("name")->filter($request->all())->paginate($per_page)->withQueryString();

        return view ('shortcut.index', compact('shortcuts', 'profile', 'request', 'total_count'));
        
        //return view ('shortcut.index', compact('profile'));



    }





    /**
     * Create a shortcut
     */
    public function create($id) {
        $profile = Profile::find($id);

        return view ('shortcut.create', compact('profile'));
    }


    public function store(Request $request, $id) {

        $this->validate ($request, $this->rules, $this->messages);

        $shortcut = new Shortcut;

        $shortcut->profile_id = $id;
        $shortcut->name = $request ['name'];
        $shortcut->target = trim($request ['target']);
        $shortcut->arguments = trim($request ['arguments']);
        $shortcut->workingDir = trim($request ['workingDir']);
        $shortcut->description = $request ['description'];
        $shortcut->location = $request ['location'];
        $shortcut->startup = $request ['startup'];

        $shortcut->save();

        Log::info(auth()->user()->name . " Shortcut " . $shortcut->name . " created");

        return back()->with('message', 'Creation du raccourci <b>' . $shortcut->name .'</b>.');

    }





    /**
     * Edit Shortcut
     */
    public function edit($id, $shortcut_id ) {

        $profile = Profile::find($id);
        $shortcut = Shortcut::find($shortcut_id);

        return view('shortcut.edit', compact('profile', 'shortcut'));
    }


    public function update(Request $request, $id, $shortcut_id) {
        // except current $id for the validation rule (or it says the host already in the db)
        $this->rules['name'] .= "," . $shortcut_id;
        $this->validate($request, $this->rules, $this->messages);

        $shortcut = Shortcut::find($shortcut_id);

        $shortcut->name = $request ['name'];
        $shortcut->target = trim($request ['target']);
        $shortcut->arguments = trim($request ['arguments']);
        $shortcut->workingDir = trim($request ['workingDir']);
        $shortcut->description = $request ['description'];
        $shortcut->location = $request ['location'];
        $shortcut->startup = $request ['startup'];

        $shortcut->save();

        Log::info(auth()->user()->name . " Shortcut " . $shortcut->name . " updated");

        return back()->with('message', 'Modification du raccourci <b>' . $shortcut->name .'</b>.');
    }





    /**
     * Remove the specified resource from storage.
     */
    public function destroyConfirmation($id, $shortcut_id) {

        $profile = Profile::find($id);
        $shortcut = Shortcut::find($shortcut_id);

        return view('shortcut.delete', compact('profile', 'shortcut'));
    }


    public function destroy($id, $shortcut_id) {

        $shortcut = Shortcut::find($shortcut_id);

        $shortcut->delete();

        Log::info(auth()->user()->name . " Shortcut " . $shortcut->name . " deleted");

        return back()->with('message', 'Suppression du raccourci <b>' . $shortcut->name .'</b>.');
    }





    /**
     * assign shortcuts to a profile
     */
    public function assignShortcuts($id) {

        $profile = Profile::find($id);
        $shortcuts = Shortcut::groupBy('name')->orderBy('name')->pluck('name', 'id');
        return view('shortcut.assignShortcuts', compact('profile', 'shortcuts'));
    }


    public function updateShortcuts(Request $request, $id) {


        //$profile = Profile::find ($id);
        $shortcut_ids = $request["shortcuts"];
        $location = $request["location"];

        foreach ($shortcut_ids as $shortcut_id) {

            $shortcut = Shortcut::find($shortcut_id);

            $new_shortcut = new Shortcut;

            $new_shortcut->profile_id = $id;
            $new_shortcut->name = $shortcut->name;
            $new_shortcut->target = $shortcut->target;
            $new_shortcut->arguments = $shortcut->arguments;
            $new_shortcut->workingDir = $shortcut->workingDir;
            $new_shortcut->description = $shortcut->description;
            $new_shortcut->location = $location;

            $new_shortcut->save();

        }

        Log::info(auth()->user()->name . " " . count($shortcut_ids) . " assigned to profile " . Profile::find($id)->name);

        return back()->with('message', 'Association de ' .count($shortcut_ids). ' raccourci(s)');

    }





    /**
     * Display a listing of ALL the shortcuts of ALL profiles.
     */
    public function massIndex(Request $request) {

        $total_count = Shortcut::count();
        $per_page = $request->get("perPage", 50);

        $shortcuts = Shortcut::with(["profile"])->orderBy("name")->filter($request->all())->paginate($per_page)->withQueryString();

        return view ('shortcutsMgt.index', compact('shortcuts', 'request', 'total_count'));
    }





    /**
     * Store a newly created shortcut in db for selected profiles.
     */
    public function massCreate() {

        $all_profiles = Profile::whereKeyNot(9999)->orderBy('name')->pluck('name', 'id');

        return view ('shortcutsMgt.massCreate', compact('all_profiles'));

    }


    public function massStore(Request $request) {

        $this->validate ($request, $this->rules, $this->messages);

        $profiles = $request['all_profiles'];

        if (!filled($profiles)) {
            return back()->with('message', "Le raccourci doit être associé à au moins un profil");
        }

        foreach ($profiles as $profile) {

            $shortcut = new Shortcut;

            $shortcut->profile_id = $profile;
            $shortcut->name = $request ['name'];
            $shortcut->target = trim($request ['target']);
            $shortcut->arguments = trim($request ['arguments']);
            $shortcut->workingDir = trim($request ['workingDir']);
            $shortcut->description = $request ['description'];
            $shortcut->location = $request ['location'];

            $shortcut->save();
            
            Log::info(auth()->user()->name . " Shortcut " . $shortcut->name . " created for profile " . Profile::find($profile)->name);
        }

        return to_route('shortcutsMgt.index')->with('message', 'Creation du raccourci <b>' . $shortcut->name .'</b> pour les profils sélectionnés.');
    }





    /**
     * Duplicate a shortcut in db for selected profiles.
     */
    public function duplicate($shortcut_id) {

        $shortcut = Shortcut::find($shortcut_id);
        $other_profiles = Profile::where("id", "<>", $shortcut->profile_id)->whereKeyNot(9999)->orderBy('name')->pluck('name', 'id');

        return view ('shortcutsMgt.duplicate', compact('other_profiles', 'shortcut'));

    }


    public function duplicateStore(Request $request, $shortcut_id) {

        $sc = Shortcut::find($shortcut_id);
        $profiles = $request['other_profiles'];

        foreach ($profiles as $profile) {

            $shortcut = new Shortcut;

            $shortcut->profile_id = $profile;
            $shortcut->name = $sc->name;
            $shortcut->target = $sc->target;
            $shortcut->arguments = $sc->arguments;
            $shortcut->workingDir = $sc->workingDir;
            $shortcut->description = $sc->description;
            $shortcut->location = $sc->location;

            $shortcut->save();

            Log::info(auth()->user()->name . " Shortcut " . $shortcut->name . " duplicated on profile " . Profile::find($profile)->name);
        }


        return to_route('shortcutsMgt.index')->with('message', 'Clonage du raccourci <b>' . $shortcut->name .'</b> pour les profils sélectionnés.');

    }





    /**
     * mass delete the selected shortcuts
     */
    public function massDelete() {

        return view('shortcutsMgt.massDelete');
    }


    public function massDestroy(Request $request) {

        $ids = explode(",", $request['ids'] );

        foreach ($ids as $id) {
            $shortcut = Shortcut::find($id);
            $shortcut->delete();

            Log::info(auth()->user()->name . " Shortcut " . $shortcut->name . " deleted");
        }

        return to_route('shortcutsMgt.index')->with('message', "Les raccourcis sélectionnés ont été supprimés.");
    }







}
