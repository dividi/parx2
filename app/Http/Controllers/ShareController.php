<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Redirect;

use App\Share;
use App\Profile;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ShareController extends Controller {

    // Secure this controller
    public function __construct() {
        $this->middleware('auth');
    }

    // Rules to validate
    protected $rules = [
        'name' => 'required|between:3,20|unique:Share,name',
        'target' => 'required|between:3,1000'
    ];

    // custom messages
    protected $messages = [
        'required' => "Le champ :attribute est obligatoire.",
        'unique' => "Le partage existe déjà.",
        'between' => 'Le champ :attribute doit avoir entre :min et :max caractères.',
    ];






    /**
     * Display a listing of all shares
     */
    public function index(Request $request) {
        $total_count = Share::count(); 
        $per_page = $request->get("perPage", 50);
        $shares = Share::with(["profiles"])->orderBy("name")->filter($request->all())->paginate($per_page)->withQueryString();
        return view ('share.index', compact('shares', 'request', 'total_count'));
    }


    /**
     * Show the form for creating a new share.
     */
    public function create() {

        return view ('share.create');
    }

    /**
     * Store a newly created printer in storage.
     */
    public function store(Request $request) {

        $this->validate ($request, $this->rules, $this->messages);

        $share = new Share;
        $share->name = $request ['name'];
        $share->target = $request ['target'];
        $share->save();

        Log::info(auth()->user()->name . " Share " . $share->name . " created");

        return to_route("share.index")->with('message', "Creation du partage <b>" . $share->name .'</b>.');
    }



    /**
     * Show the form for editing share.
     */
    public function edit($id) {
        $share = Share::find($id);
        return view ( 'share.edit', compact('share') );
    }

    /**
     * Update the specified printer in storage.
     */
    public function update(Request $request, $id) {

        // except current $id for the validation rule (or it says the host already in the db)
        $this->rules['name'] .= "," . $id;
        $this->validate($request, $this->rules, $this->messages);

        $share = Share::find($id);

        $share->name = $request ['name'];
        $share->target = $request ['target'];

        $share->save();

        Log::info(auth()->user()->name . " Share " . $share->name . " updated");

        return to_route("share.index")->with('message', "Modification du partage <b>" . $share->name .'</b>.');
    }


    /**
     * Display the confirmation message to delete the share
     */
    public function destroyConfirmation($id) {

        $share = Share::findOrFail($id);

        return view ('share.delete', compact('share'));
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id) {
        $share = Share::find ($id);

        $share->delete();

        Log::info(auth()->user()->name . " Share " . $share->name . " deleted");

        return back()->with('message', "Suppression du partage <b>" . $share->name .'</b>.');
    }




    /*************************************
    *
    *        PROFILE ASSIGN
    *
    *************************************/







    /**
     * Display the shares of a specific profile.
     */
    public function assignedShares($id) {

        $profile = Profile::findOrFail($id);
        return view( 'share.profileShares', compact('profile') );
    }





    /**
     * Show the form for deleting the specified profile.
     */
    public function unassignShare($id, $share_id) {

        $profile = Profile::find($id);
        $share = Share::find($share_id);

        return view('share.unassignShare', compact('profile', 'share'));
    }


    /**
     * Show the form for deleting the specified profile.
     */
    public function destroyShare($id, $share_id) {

        $profile = Profile::find($id);
        $share = Share::find($share_id);

        $profile->shares()->detach($share);

        Log::info(auth()->user()->name . " Share " . $share->name . " unassigned from profile " . $profile->name);

        return back()->with('message', 'Suppression du partage <b>' . $share->name . '</b> du profil <b>' . $profile->name .'</b>.');

    }

    /**
     * Show the form for adding a share to the specified profile.
     */
    public function assignShare($id) {

        $profile = Profile::findOrFail($id);
        $shares = Share::pluck('name', 'id');

        return view('share.assignShare', compact('profile', 'shares'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function updateShare(Request $request, $id) {

        $share_id = $request['share'];
        $share = Share::find($share_id);
        $shareLetter = $request['shareLetter'];

        $profile = Profile::find($id);


        // Test if the letter is already assigned
        $letter_exists = count($profile->shares()->wherePivot('shareLetter', $shareLetter)->get());

        if ( $letter_exists == 0 ) {

            // Test if the share is already assigned
            $share_exists = count($profile->shares()->wherePivot('share_id', $share_id)->get());

            if ( $share_exists == 0 ) {
                $profile->shares()->attach( $share, array('shareLetter' => $shareLetter) );

                Log::info(auth()->user()->name . " Share " . $share->name . " affected to letter " . $shareLetter . " on profile " . $profile->name);

                return to_route("profiles.assignedShares", ["id"=>$id])->with('message', 'Ajout du partage ' . $share->name . ' sur <b>' . $shareLetter .'</b>.');
            }
            else {
                return back()->with('message', 'Le partage <b>' . $share->name .'</b> est déjà associé à ce profil.');
            }
        }
        else {
            return back()->with('message', 'La lettre <b>' . $shareLetter .'</b> est déjà affectée à ce profil.');
        }

    }




}
