<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Redirect;

use App\Printer;
use App\Profile;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PrinterController extends Controller {

    // Secure this controller
    public function __construct() {
        $this->middleware('auth');
    }


    // Rules to validate
    protected $rules = [
        'name' => 'required|between:3,20|unique:Printer,name',
        'target' => 'required|between:3,1000'
    ];

    // custom messages
    protected $messages = [
        'required' => "Le champ :attribute est obligatoire.",
        'unique' => "L'imprimante existe déjà.",
        'between' => 'Le champ :attribute doit avoir entre :min et :max caractères.',
    ];






    /**
     * Display a listing of all printers
     */
    public function index(Request $request) {
        
        $total_count = Printer::count(); 
        $per_page = $request->get("perPage", 50);
        $printers = Printer::with(["profiles"])->orderBy("name")->filter($request->all())->paginate($per_page)->withQueryString();
        
        return view ('printer.index', compact('printers', 'request', 'total_count'));
    }


    /**
     * Show the form for creating a new printer.
     */
    public function create() {

        return view ('printer.create');
    }

    /**
     * Store a newly created printer in storage.
     */
    public function store(Request $request) {

        $this->validate ($request, $this->rules, $this->messages);

        $printer = new Printer;
        $printer->name = $request ['name'];
        $printer->target = $request ['target'];
        $printer->save();

        Log::info(auth()->user()->name . " Printer " . $printer->name . " created");

        return to_route("printers.index")->with('message', "Creation de l'imprimante <b>" . $printer->name .'</b>.');
    }



    /**
     * Show the form for editing printer.
     */
    public function edit($id) {
        $printer = Printer::find($id);
        return view ( 'printer.edit', compact('printer') );
    }

    /**
     * Update the specified printer in storage.
     */
    public function update(Request $request, $id) {

        // except current $id for the validation rule (or it says the host already in the db)
        $this->rules['name'] .= "," . $id;
        $this->validate($request, $this->rules, $this->messages);


        $printer = Printer::find($id);

        $printer->name = $request ['name'];
        $printer->target = $request ['target'];

        $printer->save();

        Log::info(auth()->user()->name . " Printer " . $printer->name . " update");

        return to_route("printers.index")->with('message', "Modification de l'imprimante <b>" . $printer->name .'</b>.');
    }


    /**
     * Display the confirmation message to delete the printer
     */
    public function destroyConfirmation($id) {

        $printer = Printer::findOrFail($id);

        return view ('printer.delete', compact('printer'));
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id) {
        $printer = Printer::find ($id);

        $printer->delete();

        Log::info(auth()->user()->name . " Printer " . $printer->name . " deleted");

        return back()->with('message', "Suppression de l'imprimante <b>" . $printer->name .'</b>.');
    }




    /*************************************
    *
    *        PROFILE ASSIGN
    *
    *************************************/





    /**
     * Show the form to affect a printer to profiles.
     */
    public function affectPrinter($id) {

        $printer = Printer::find($id);
        $all_profiles = Profile::whereKeyNot(9999)->orderBy('name')->pluck('name', 'id');

        return view ('printer.affect', compact('printer', 'all_profiles'));

    }



    /**
     * Affect printer to selected profiles ($id = printer_id)
     */
    public function affectPrinterUpdate(Request $request, $id) {

        $printer = Printer::find($id);
        $profiles = $request['all_profiles'];
        $default = $request['default'] == 1 ? 1 : 0;
        
        if (!isset($profiles)) {
            return back()->with('message', "Pas de profil sélectionné.");
        }


        foreach ($profiles as $profile_id) {

            $current_profile = Profile::find($profile_id);

            // Test if the printer is already assigned
            $printer_exists = count($current_profile->printers()->wherePivot('printer_id', $id)->get());

            if ($printer_exists == 0) {

                if ($default == 1) {

                    // UNdefault all printers
                    $profile_printers = $current_profile->printers;

                    foreach ( $profile_printers as $prnt) {
                        $res = $prnt->profiles()->updateExistingPivot($profile_id, array("default" => 0) ) ;
                    }

                }

                // add the printer
                $current_profile->printers()->attach( $printer, array('default' => $default) );

                Log::info(auth()->user()->name . " Printer " . $printer->name . " affected to " . $current_profile->name);

            }

            // printer is already affected to the profile
            else {
                // maybe make it default ?
                if ($default == 1) {

                    // UNdefault all printers
                    $profile_printers = $current_profile->printers;

                    foreach ( $profile_printers as $prnt) {
                        $prnt->profiles()->updateExistingPivot($profile_id, array("default" => 0) ) ;
                    }

                    // default this one
                    $printer->profiles()->updateExistingPivot($profile_id, array("default"=> 1) ) ;
                }

            }

        }

        return to_route("printers.index")->with('message', "Association de l'imprimante <b>" . $printer->name ."</b> pour les profils sélectionnés.");

    }



    /**
     * Display the shares of a specific profile.
     */
    public function assignedPrinters($id) {

        $profile = Profile::findOrFail($id);
        return view( 'printer.profilePrinters', compact('profile') );
    }



    /**
     * Show the form for adding a printer to the specified profile.
     */
    public function assignPrinter($id) {

        $profile = Profile::findOrFail($id);
        $printers = Printer::pluck('name', 'id');

        return view( 'printer.assignPrinter', compact('profile', 'printers') );
    }



    /**
     * Update the specified resource in storage.
     */
    public function updatePrinter(Request $request, $id) {

        $printer_id = $request['printer'];
        $printer = Printer::find($printer_id);
        $default = $request['default'] == 1 ? 1 : 0;

        $profile = Profile::find($id);


        // Test if the printer is already assigned
        $printer_exists = count($profile->printers()->wherePivot('printer_id', $printer_id)->get());

        if ( $printer_exists == 0 ) {


            // Put all printers not as default if default is checked then put the current one to default (is that clear ? no. ok.)
            if ( $default == 1 ) {

                $profile_printers = $profile->printers;

                foreach ( $profile_printers as $prnt) {
                    $prnt->profiles()->updateExistingPivot($id, array("default"=> 0) ) ;
                }

            }

            $profile->printers()->attach( $printer, array('default' => $default) );

            Log::info(auth()->user()->name . " Printer " . $printer->name . " affected to " . $profile->name);

            return to_route("profiles.assignedPrinters", ["id"=>$id])->with('message', "Ajout de l'imprimante <b>" . $printer->name . '</b>.');
        }
        else {
            return to_route("profiles.assignedPrinters", ["id"=>$id])->with('message', "L'imprimante <b>" . $printer->name ."</b> est déjà associée à ce profil.");
        }

    }




    /**
     * Clicked printer is the default one.
     */
    public function defaultPrinter($id, $printer_id) {

        $printer = Printer::find($printer_id);
        $profile = Profile::find($id);

        $profile_printers = $profile->printers;

        // All printers are not default
        foreach ( $profile_printers as $prnt) {
            $prnt->profiles()->updateExistingPivot($id, array("default"=> 0) ) ;
        }


        // Current printer as default
        $printer->profiles()->updateExistingPivot($id, array("default"=> 1) ) ;

        Log::info(auth()->user()->name . " Printer " . $printer->name . " set to default for profile " . $profile->name);

        return back()->with('message', "L'imprimante <b>" . $printer->name . '</b> est par défaut.');
    }





    /**
     * Show the form for deleting the specified profile.
     */
    public function unassignPrinter($id, $printer_id) {

        $profile = Profile::find($id);
        $printer = Printer::find($printer_id);

        return view('printer.unassignPrinter', compact('profile', 'printer'));
    }


    /**
     * Show the form for deleting the specified profile.
     */
    public function destroyPrinter($id, $printer_id) {

        $profile = Profile::find($id);
        $printer = Printer::find($printer_id);

        $profile->printers()->detach($printer);

        Log::info(auth()->user()->name . " Printer " . $printer->name . " unassigned to profile " . $profile->name);

       return back()->with('message', "Suppression de l'imprimante <b>" . $printer->name . '</b> du profil <b>' . $profile->name .'</b>.');

    }




}
