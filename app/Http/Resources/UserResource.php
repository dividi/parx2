<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'login' => $this->login,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'profiles' => $this->whenLoaded('profiles', fn () => ProfileResource::collection($this->profiles)),
            'groups' => $this->whenLoaded('usergroups', fn () => UsergroupResource::collection($this->usergroups)),
        ];
    }
}
