<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'os' => $this->os,
            'version' => $this->version,
            'profiles' => $this->whenLoaded('profiles', fn () => ProfileResource::collection($this->profiles)),
            'groups' => $this->whenLoaded('hostgroups', fn () => HostgroupResource::collection($this->hostgroups)),
        ];
    }
}
