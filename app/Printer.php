<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Printer extends Model {

    protected $table = 'Printer';
    public $timestamps = false;



	// Pivot table with Hosts	
	public function profiles() {
		return $this->belongsToMany('App\Profile', 'profile_printer', 'printer_id', 'profile_id' )->withPivot('default');
	}




	// cascade functions
	public static function boot() {
        
        parent::boot();
        
        // cascade deleting parameters in pivot table
        Printer::deleting(function($printer) {
            $printer->profiles()->detach();
        });


    }




    // scopes

    public function scopeOfName ($query, $value) {
        return $query->where('name', 'like', "%".$value."%");
    }

    public function scopeOfTarget ($query, $value) {
        return $query->where('target', 'like', "%".$value."%");
    }

	public function scopeOfProfile ($query, $profile_name) {
		return $query->whereHas('profiles', fn($profiles) => $profiles->ofName($profile_name));
	}

    public function scopeFilter($query, array $filters) {
		return $query
            ->when(isset($filters['filter_name']) && filled($filters['filter_name']), fn($query) => $query->ofName($filters['filter_name']) )
            ->when(isset($filters['filter_target']) && filled($filters['filter_target']), fn($query) => $query->ofTarget($filters['filter_target']) )
            ->when(isset($filters['filter_profile']) && filled($filters['filter_profile']), fn($query) => $query->ofProfile($filters['filter_profile']) );
	}




}
