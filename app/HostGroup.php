<?php

namespace App;

use App\Host;
use Illuminate\Database\Eloquent\Model;

class HostGroup extends Model {
    
    protected $table = 'HostGroup';
    public $timestamps = false;


	// Pivot table with profiles	
	public function profiles() {
		return $this->belongsToMany('App\Profile', 'hostgroup_profile', 'hostgroup_id', 'profile_id');
	}

	// Pivot table with Hosts	
	public function hosts() {
		return $this->belongsToMany('App\Host', 'host_hostgroup', 'hostgroup_id', 'host_id');
	}





	// scopes
	public function scopeOfName($query, $name) {
		return $query->where('name', 'like', "%".$name."%");
	}


	public function scopeOfProfile ($query, $profile_name) {
		return $query->whereHas('profiles', fn($profiles) => $profiles->ofName($profile_name));
	}

	public function scopeOfHostCount($query, $expression) {
		
		if (str_contains($expression, ">")) {
			$count = str_replace(">", "", $expression);
			return $query->has('hosts', '>', $count);
		}

		if (str_contains($expression, "<")) {
			$count = str_replace("<", "", $expression);
			return $query->has('hosts', '<', $count);
		}

		if (str_contains($expression, "!")) {
			$count = str_replace("!", "", $expression);
			return $query->has('hosts', '!=', $count);
		}
		
		return $query->has('hosts', '=', $expression);

	}


	public function scopeFilter($query, array $filters) {

		return $query
			->when(isset($filters['filter_name']) && filled($filters['filter_name']), fn($query) => $query->ofName($filters['filter_name']) )
			->when(isset($filters['filter_host_count']) && filled($filters['filter_host_count']), fn($query) => $query->ofHostCount($filters['filter_host_count']) )
			->when(isset($filters['filter_profile']) && filled($filters['filter_profile']), fn($query) => $query->ofProfile($filters['filter_profile']) );
	}






}
