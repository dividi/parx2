<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shortcut extends Model {

    protected $table = 'Shortcut';
    public $timestamps = false;


    public function profile() {
        return $this->belongsTo(Profile::class);
    }




    // scopes
    public function scopeOfName ($query, $name) {
        return $query->where('name', 'like', "%".$name."%");
    }

    public function scopeOfTarget ($query, $value) {
        return $query->where('target', 'like', "%".$value."%");
    }

	public function scopeOfArguments ($query, $value) {
        return $query->where('arguments', 'like', "%".$value."%");
    }

    public function scopeOfWorkingDir ($query, $value) {
        return $query->where('workingDir', 'like', "%".$value."%");
    }

	public function scopeOfDescription ($query, $value) {
        return $query->where('description', 'like', "%".$value."%");
    }

	public function scopeOfLocation ($query, $location) {
		return $query->where('location', '=', $location);
	}


	public function scopeOfProfile ($query, $profile_name) {
		return $query->whereHas('profile', fn($profiles) => $profiles->ofName($profile_name));
	}



	public function scopeFilter($query, array $filters) {
		return $query
				->when(isset($filters['filter_name']) && filled($filters['filter_name']), fn($query) => $query->ofName($filters['filter_name']) )
				->when(isset($filters['filter_target']) && filled($filters['filter_target']), fn($query) => $query->ofTarget($filters['filter_target']) )
				->when(isset($filters['filter_arguments']) && filled($filters['filter_arguments']), fn($query) => $query->ofArguments($filters['filter_arguments']) )
				->when(isset($filters['filter_workingdir']) && filled($filters['filter_workingdir']), fn($query) => $query->ofWorkingDir($filters['filter_workingdir']) )
				->when(isset($filters['filter_description']) && filled($filters['filter_description']), fn($query) => $query->ofDescription($filters['filter_description']) )
				->when(isset($filters['filter_location']) && filled($filters['filter_location']), fn($query) => $query->ofLocation($filters['filter_location']) )
				->when(isset($filters['filter_profile']) && filled($filters['filter_profile']), fn($query) => $query->ofProfile($filters['filter_profile']) )
				;
	}



}
