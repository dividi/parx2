<?php

namespace App;

use App\HostGroup;
use App\Profile;
use Illuminate\Database\Eloquent\Model;

class Host extends Model {
    
    // MassAssignException guard
	protected $guarded = [];
    protected $table = 'Host';
    public $timestamps = false;



	// Pivot table with profiles	
	public function profiles() {
		return $this->belongsToMany('App\Profile', 'host_profile', 'host_id', 'profile_id');
	}

	
	// Pivot table with hostgroups	
	public function hostgroups() {
		return $this->belongsToMany('App\HostGroup', 'host_hostgroup', 'host_id', 'hostgroup_id');
	}


	// Pivot table with tasks	
	public function tasks() {
		return $this->belongsToMany('App\Task', 'host_task', 'host_id', 'task_id')->withPivot('id', 'recurtion', 'schedule');
	}



	// cascade functions
	public static function boot() {
        
        parent::boot();
        
        // cascade deleting parameters in pivot table
        Profile::deleting(function($host) {
            $host->hostgroups()->detach();
            $host->profiles()->detach();
            $host->tasks()->detach();
        });

    }




	// scopes

/* 

filter_profile_count

 */


	public function scopeOfName ($query, $name) {
		return $query->where('name', 'like', "%".$name."%");
	}

	public function scopeOfSystem ($query, $system) {
		return $query->where('os', 'like', "%".$system."%");
	}

	public function scopeOfVersion ($query, $version) {
		return $query->where('version', 'like', "%".$version."%");
	}

	public function scopeOfLastConnection ($query, $last_connection) {
		return $query->where('last_connection', 'like', "%".$last_connection."%");
	}

	public function scopeOfLastProfile ($query, $last_profile) {
		return $query->where('last_profile_name', 'like', "%".$last_profile."%");
	}


	public function scopeOfProfile ($query, $profile_name) {
		return $query->whereHas('profiles', fn($profiles) => $profiles->ofName($profile_name));
	}

	public function scopeOfGroup ($query, $group_name) {
		return $query->whereHas('hostgroups', fn($hostgroups) => $hostgroups->ofName($group_name));
	}

	public function scopeOfProfileCount($query, $expression) {
		
		if (str_contains($expression, ">")) {
			$count = str_replace(">", "", $expression);
			return $query->has('profiles', '>', $count);
		}

		if (str_contains($expression, "<")) {
			$count = str_replace("<", "", $expression);
			return $query->has('profiles', '<', $count);
		}

		if (str_contains($expression, "!")) {
			$count = str_replace("!", "", $expression);
			return $query->has('profiles', '!=', $count);
		}
		
		return $query->has('profiles', '=', $expression);

	}

	// for active tasks
	public function scopeOfTaskName ($query, $value) {
		return $query->whereHas('tasks', fn($tasks) => $tasks->ofName($value));
	}
	public function scopeOfTaskExe ($query, $value) {
		return $query->whereHas('tasks', fn($tasks) => $tasks->ofExe($value));
	}
	public function scopeOfTaskArgs ($query, $value) {
		return $query->whereHas('tasks', fn($tasks) => $tasks->ofArgs($value));
	}
	public function scopeOfTaskDescription ($query, $value) {
		return $query->whereHas('tasks', fn($tasks) => $tasks->ofDescription($value));
	}
	public function scopeOfTaskSchedule ($query, $value) {
		return $query->whereHas('tasks', fn($q) => $q->where('schedule', 'like', "%".$value."%")  );
	}
	public function scopeOfTaskId ($query, $value) {
		return $query->whereHas('tasks', fn($q) => $q->where('host_task.id', $value)  );
	}


	public function scopeFilter($query, array $filters) {
		return $query
			->when(isset($filters['filter_name']) && filled($filters['filter_name']), fn($query) => $query->ofName($filters['filter_name']) )
			->when(isset($filters['filter_system']) && filled($filters['filter_system']), fn($query) => $query->ofSystem($filters['filter_system']) )
			->when(isset($filters['filter_version']) && filled($filters['filter_version']), fn($query) => $query->ofVersion($filters['filter_version']) )
			->when(isset($filters['filter_last_connection']) && filled($filters['filter_last_connection']), fn($query) => $query->ofLastConnection($filters['filter_last_connection']) )
			->when(isset($filters['filter_last_profile']) && filled($filters['filter_last_profile']), fn($query) => $query->ofLastProfile($filters['filter_last_profile']) )
			->when(isset($filters['filter_groups']) && filled($filters['filter_groups']), fn($query) => $query->ofGroup($filters['filter_groups']) )
			->when(isset($filters['filter_profiles']) && filled($filters['filter_profiles']), fn($query) => $query->ofProfile($filters['filter_profiles']) )
			->when(isset($filters['filter_profile_count']) && filled($filters['filter_profile_count']), fn($query) => $query->ofProfileCount($filters['filter_profile_count']) );
	}


	// filters for active tasks
	public function scopeFilterActiveTasks($query, array $filters) {
		return $query
            ->when(isset($filters['filter_id']) && filled($filters['filter_id']), fn($query) => $query->ofTaskId($filters['filter_id']) )
            ->when(isset($filters['filter_host']) && filled($filters['filter_host']), fn($query) => $query->ofName($filters['filter_host']) )
            ->when(isset($filters['filter_task_name']) && filled($filters['filter_task_name']), fn($query) => $query->ofTaskName($filters['filter_task_name']) )
			->when(isset($filters['filter_exe']) && filled($filters['filter_exe']), fn($query) => $query->ofTaskExe($filters['filter_exe']) )
            ->when(isset($filters['filter_args']) && filled($filters['filter_args']), fn($query) => $query->ofTaskArgs($filters['filter_args']) )
            ->when(isset($filters['filter_description']) && filled($filters['filter_description']), fn($query) => $query->ofTaskDescription($filters['filter_description']) )
            ->when(isset($filters['filter_schedule']) && filled($filters['filter_schedule']), fn($query) => $query->ofTaskSchedule($filters['filter_schedule']) );
	}


}
