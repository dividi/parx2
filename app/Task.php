<?php

namespace App;

use App\Host;
use Illuminate\Database\Eloquent\Model;

class Task extends Model {

	// MassAssignException guard
	protected $guarded = [];
    protected $table = 'Task';
    public $timestamps = false;
    

    // Pivot table with hosts	
	public function hosts() {
		return $this->belongsToMany('App\Host', 'host_task', 'task_id', 'host_id')->withPivot('id', 'recurtion', 'schedule');
	}







	// cascade functions
	public static function boot() {
        
        parent::boot();
        
        // cascade deleting parameters in pivot table
        Profile::deleting(function($task) {
            $task->hosts()->detach();
        });

    }


    // scopes

    public function scopeOfName ($query, $value) {
        return $query->where('name', 'like', "%".$value."%");
    }

    public function scopeOfExe ($query, $value) {
        return $query->where('exe', 'like', "%".$value."%");
    }
    
    public function scopeOfArgs ($query, $value) {
        return $query->where('args', 'like', "%".$value."%");
    }

    public function scopeOfDescription ($query, $value) {
        return $query->where('description', 'like', "%".$value."%");
    }

	public function scopeOfHostCount($query, $expression) {
		
		if (str_contains($expression, ">")) {
			$count = str_replace(">", "", $expression);
			return $query->has('hosts', '>', $count);
		}

		if (str_contains($expression, "<")) {
			$count = str_replace("<", "", $expression);
			return $query->has('hosts', '<', $count);
		}

		if (str_contains($expression, "!")) {
			$count = str_replace("!", "", $expression);
			return $query->has('hosts', '!=', $count);
		}
		
		return $query->has('hosts', '=', $expression);

	}

    public function scopeFilter($query, array $filters) {
		return $query
            ->when(isset($filters['filter_name']) && filled($filters['filter_name']), fn($query) => $query->ofName($filters['filter_name']) )
            ->when(isset($filters['filter_exe']) && filled($filters['filter_exe']), fn($query) => $query->ofExe($filters['filter_exe']) )
            ->when(isset($filters['filter_args']) && filled($filters['filter_args']), fn($query) => $query->ofArgs($filters['filter_args']) )
            ->when(isset($filters['filter_host_count']) && filled($filters['filter_host_count']), fn($query) => $query->ofHostCount($filters['filter_host_count']) )
            ->when(isset($filters['filter_description']) && filled($filters['filter_description']), fn($query) => $query->ofDescription($filters['filter_description']) );
	}
}