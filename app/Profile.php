<?php

namespace App;

use App\Parameter;
use App\Host;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model {

	protected $table = 'Profile';
	public $timestamps = false;


	// Pivot table with Parameters
	public function parameters() {
		return $this->belongsToMany('App\Parameter', 'ProfileParameter', 'profile_id', 'parameter_id')->withPivot('parameterValue');
	}

	// Pivot table with Hosts
	public function hosts() {
		return $this->belongsToMany('App\Host', 'host_profile', 'profile_id', 'host_id');
	}

	// Pivot table with Printers
	public function printers() {
		return $this->belongsToMany('App\Printer', 'profile_printer', 'profile_id', 'printer_id')->withPivot('default');
	}

	// Pivot table with Parameters
	public function shares() {
		return $this->belongsToMany('App\Share', 'ProfileShare', 'profile_id', 'share_id')->withPivot('shareLetter');
	}

	// Pivot table with Shortcuts
	public function shortcuts() {
		return $this->hasMany('App\Shortcut', 'profile_id');
	}

	// Pivot table with Users
	public function users() {
		return $this->belongsToMany('App\User', 'user_profile', 'profile_id', 'user_id');
	}

	// Pivot table with Usergroups
	public function usergroups() {
		return $this->belongsToMany('App\UserGroup', 'usergroup_profile', 'profile_id', 'usergroup_id');
	}

	// Pivot table with Hostgroups
	public function hostgroups() {
		return $this->belongsToMany('App\HostGroup', 'hostgroup_profile', 'profile_id', 'hostgroup_id');
	}



	// cascade functions
	public static function boot() {

        parent::boot();

        // cascade deleting parameters in pivot table
        Profile::deleting(function($profile) {
           	$profile->parameters()->detach();
            $profile->hosts()->detach();
            $profile->printers()->detach();
            $profile->shares()->detach();
            $profile->users()->detach();
            $profile->usergroups()->detach();
            $profile->hostgroups()->detach();
        });


        // Create parameters list after profile creation
/*         Profile::created(function($profile) {
            $parameters = Parameter::All();

	        foreach ($parameters as $param) {
	            $profile->parameters()->attach( $param->id, array('parameterValue' => $param->defaultValue) );
	        }
        }); */

    }




	// scopes
	public function scopeOfName ($query, $name) {
		return $query->where('name', 'like', "%".$name."%");
	}

	public function scopeOfMatchType ($query, $match_type) {
		return $query->where('matchType', '=', $match_type);
	}

	public function scopeOfCount($query, $field, $expression) {
		
		if (str_contains($expression, ">")) {
			$count = str_replace(">", "", $expression);
			return $query->has($field, '>', $count);
		}

		if (str_contains($expression, "<")) {
			$count = str_replace("<", "", $expression);
			return $query->has($field, '<', $count);
		}

		if (str_contains($expression, "!")) {
			$count = str_replace("!", "", $expression);
			return $query->has($field, '!=', $count);
		}
		
		return $query->has($field, '=', $expression);

	}


	public function scopeFilter($query, array $filters) {
		return $query
				->when(isset($filters['filter_name']) && filled($filters['filter_name']), fn($query) => $query->ofName($filters['filter_name']) )
				->when(isset($filters['filter_match_type']) && filled($filters['filter_match_type']), fn($query) => $query->ofMatchType($filters['filter_match_type']) )
				->when(isset($filters['filter_host_count']) && filled($filters['filter_host_count']), fn($query) => $query->ofCount('hosts', $filters['filter_host_count']) )
				->when(isset($filters['filter_user_count']) && filled($filters['filter_user_count']), fn($query) => $query->ofCount('users', $filters['filter_user_count']) )
				->when(isset($filters['filter_hostgroup_count']) && filled($filters['filter_hostgroup_count']), fn($query) => $query->ofCount('hostgroups', $filters['filter_hostgroup_count']) )
				->when(isset($filters['filter_usergroup_count']) && filled($filters['filter_usergroup_count']), fn($query) => $query->ofCount('usergroups', $filters['filter_usergroup_count']) );
	}


}
