<?php

namespace App;

use App\UserGroup;
use App\Profile;
use Illuminate\Database\Eloquent\Model;

class User extends Model {
   	
   	// MassAssignException guard
	protected $guarded = [];
    protected $table = 'User';
    public $timestamps = false;



	// Pivot table with profiles	
	public function profiles() {
		return $this->belongsToMany('App\Profile', 'user_profile', 'user_id', 'profile_id');
	}


	// Pivot table with hostgroups	
	public function usergroups() {
		return $this->belongsToMany('App\UserGroup', 'user_usergroup', 'user_id', 'usergroup_id');
	}



	// cascade functions
	public static function boot() {
        
        parent::boot();
        
        // cascade deleting parameters in pivot table
        Profile::deleting(function($user) {
            $user->usergroups()->detach();
            $user->profiles()->detach();
        });

    }


	// scopes

	public function scopeOfLogin ($query, $login) {
		return $query->where('login', 'like', "%".$login."%");
	}

	public function scopeOfFirstname ($query, $firstname) {
		return $query->where('firstName', 'like', "%".$firstname."%");
	}

	public function scopeOfLastname ($query, $lastname) {
		return $query->where('lastName', 'like', "%".$lastname."%");
	}

	public function scopeOfProfile ($query, $profile_name) {
		return $query->whereHas('profiles', fn($profiles) => $profiles->ofName($profile_name));
	}

	public function scopeOfGroup ($query, $group_name) {
		return $query->whereHas('usergroups', fn($usergroups) => $usergroups->ofName($group_name));
	}


	public function scopeFilter($query, array $filters) {
		return $query
				->when(isset($filters['filter_login']) && filled($filters['filter_login']), fn($query) => $query->ofLogin($filters['filter_login']) )
				->when(isset($filters['filter_firstname']) && filled($filters['filter_firstname']), fn($query) => $query->ofFirstname($filters['filter_firstname']) )
				->when(isset($filters['filter_lastname']) && filled($filters['filter_lastname']), fn($query) => $query->ofLastname($filters['filter_lastname']) )
				->when(isset($filters['filter_groups']) && filled($filters['filter_groups']), fn($query) => $query->ofGroup($filters['filter_groups']) )
				->when(isset($filters['filter_profiles']) && filled($filters['filter_profiles']), fn($query) => $query->ofProfile($filters['filter_profiles']) );
	}

}
