<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parameter extends Model {
    
	protected $table = 'Parameter';
	public $timestamps = false;
	protected $guarded = [];
    


	public function profiles() {
		return $this->belongsToMany('App\Profile', 'ProfileParameter', 'parameter_id', 'profile_id')->withPivot('parameterValue');
	}

}
