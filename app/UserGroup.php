<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model {
    

    protected $table = 'UserGroup';
    public $timestamps = false;



	// Pivot table with profiles	
	public function profiles() {
		return $this->belongsToMany('App\Profile', 'usergroup_profile', 'usergroup_id', 'profile_id');
	}


	// Pivot table with Hosts	
	public function users() {
		return $this->belongsToMany('App\User', 'user_usergroup', 'usergroup_id', 'user_id');
	}




	// scopes
	public function scopeOfName($query, $name) {
		return $query->where('name', 'like', "%".$name."%");
	}


	public function scopeOfProfile ($query, $profile_name) {
		return $query->whereHas('profiles', fn($profiles) => $profiles->ofName($profile_name));
	}


	// want to get all lines where the count of users is greater than...
	public function scopeOfUserCount($query, $expression) {
		
		if (str_contains($expression, ">")) {
			$count = str_replace(">", "", $expression);
			return $query->has('users', '>', $count);
		}

		if (str_contains($expression, "<")) {
			$count = str_replace("<", "", $expression);
			return $query->has('users', '<', $count);
		}

		if (str_contains($expression, "!")) {
			$count = str_replace("!", "", $expression);
			return $query->has('users', '!=', $count);
		}
		
		return $query->has('users', '=', $expression);

	}


	public function scopeFilter($query, array $filters) {

		return $query
			->when(isset($filters['filter_name']) && filled($filters['filter_name']), fn($query) => $query->ofName($filters['filter_name']) )
			->when(isset($filters['filter_user_count']) && filled($filters['filter_user_count']), fn($query) => $query->ofUserCount($filters['filter_user_count']) )
			->when(isset($filters['filter_profile']) && filled($filters['filter_profile']), fn($query) => $query->ofProfile($filters['filter_profile']) );
	}


}
