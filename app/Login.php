<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;

class Login extends Authenticatable
{

    use Notifiable, HasApiTokens;

    protected $table = 'Login';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];



    // scopes

    public function scopeOfName ($query, $value) {
        return $query->where('name', 'like', "%".$value."%");
    }

    public function scopeOfEmail ($query, $value) {
        return $query->where('email', 'like', "%".$value."%");
    }

	public function scopeOfProfile ($query, $profile_name) {
		return $query->whereHas('profiles', fn($profiles) => $profiles->ofName($profile_name));
	}

    public function scopeFilter($query, array $filters) {
		return $query
            ->when(isset($filters['filter_name']) && filled($filters['filter_name']), fn($query) => $query->ofName($filters['filter_name']) )
            ->when(isset($filters['filter_email']) && filled($filters['filter_email']), fn($query) => $query->ofEmail($filters['filter_email']) );
	}



}